from qgis.PyQt.QtGui import *
from qgis.PyQt.QtWidgets import *
from qgis.PyQt.QtSql import *
from qgis.PyQt.QtCore import *
from qgis.PyQt.QtNetwork import QNetworkRequest
from qgis.utils import Qgis
from qgis.core import QgsProject, QgsDataSourceUri, QgsWkbTypes, QgsMapLayerType, QgsFeature, QgsLayerTreeGroup, QgsLayerTreeLayer, QgsFieldConstraints, QgsEditorWidgetSetup, QgsDefaultValue, QgsVectorLayer, QgsPoint, QgsGeometry, QgsCoordinateTransform, QgsCoordinateReferenceSystem, QgsNetworkAccessManager, QgsField, QgsFields, QgsLayerDefinition, QgsApplication, QgsSettings
# initialize Qt resources from file resources.py
# execute : pyrcc5 -o resources.py resources.qrc
from . import resources
from qgis import processing
import datetime
import uuid
import os, subprocess, platform

class MappingDialogBox(QDialog):
    def __init__(self, title, formlayout, parent=None):
        super(MappingDialogBox, self).__init__(parent=None)

        self.parent = parent
        self.formlayout = formlayout
        self.layout = QGridLayout()

        l = 0
        for label in self.formlayout :
            type, valuelist, other = self.formlayout[label]
            if type == 'ComboBox' :
                newrow = QComboBox()
                newrow.addItems(valuelist)
                newrow.currentTextChanged.connect(self.set_visible)
                self.layout.addWidget(QLabel(label), l, 0)
                self.layout.addWidget(newrow, l, 1)

            elif type == 'LineEdit' :
                self.layout.addWidget(QLabel(label), l, 0)
                self.layout.addWidget(QLineEdit(), l, 1)

            elif type == 'CheckBox':
                self.layout.addWidget(QLabel(label), l, 0)
                self.layout.addWidget(QCheckBox(), l, 1)
            l+=1

        self.set_visible()

        QBtn = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        buttonBox = QDialogButtonBox(QBtn, self)
        buttonBox.accepted.connect(self.accept_test)
        buttonBox.rejected.connect(self.reject)
        self.layout.addWidget(buttonBox,self.layout.rowCount(),1,-1,-1)

        self.setLayout(self.layout)
        self.setModal(True)
        self.setWindowTitle(title)

    def set_visible(self):
        i = 0
        while i < self.layout.rowCount() :
            wlabel=self.layout.itemAtPosition(i,0) #Label
            wfield=self.layout.itemAtPosition(i,1) #Field
            if isinstance(wlabel, QLayoutItem):
                wlabel=wlabel.widget()
                if isinstance(wlabel, QLabel):
                    if isinstance(wfield, QLayoutItem):
                        wfield=wfield.widget()
                        # print(i, wlabel.text())
                        if isinstance(wfield, QComboBox):
                            if wfield.currentText() == '<AUTRE>' and not self.layout.itemAtPosition(i,2) :
                                # print('add widget',wlabel.text())
                                if isinstance(self.formlayout[wlabel.text()][2], list) :
                                    newcol = QComboBox()
                                    newcol.addItems(self.formlayout[wlabel.text()][2])
                                    self.layout.addWidget(newcol, i, 2)
                                else:
                                    self.layout.addWidget(QLineEdit(str(self.formlayout[wlabel.text()][2] or '')), i, 2)
                                    # TODO : mettre des contraintes selon le type d'attribut
                                self.layout.setColumnMinimumWidth(2, 300)
                            elif wfield.currentText() != '<AUTRE>' and self.layout.itemAtPosition(i,2) :
                                # print('remove widget',wlabel.text())
                                wfield2=self.layout.itemAtPosition(i,2)
                                self.layout.removeWidget(wfield2.widget())
            i+=1

    def accept_test(self):
        accept=True
        for i in range(0,self.layout.rowCount()-1):
            wfield=self.return_text(self.layout.itemAtPosition(i,1))
            if wfield == '<AUTRE>' and self.layout.itemAtPosition(i,2) :
                wfield2=self.return_text(self.layout.itemAtPosition(i,2))
                if not wfield2 :
                    wlabel=self.return_text(self.layout.itemAtPosition(i,0))
                    self.parent.iface.messageBar().pushMessage("Formulaire incomplet", "L'attribut  `{0}`  n'est pas défini".format(wlabel), Qgis.Warning, duration=3)
                    accept=False
                    break
            else :
                pass
        if accept :
            self.accept()

    def get_output(self):
        map_dict = {}
        for i in range(0,self.layout.rowCount()-1):
            wlabel=self.return_text(self.layout.itemAtPosition(i,0)) #Label
            wfield=self.return_text(self.layout.itemAtPosition(i,1)) #Field1
            if wfield == '<AUTRE>' and self.layout.itemAtPosition(i,2) :
                wfield2=self.return_text(self.layout.itemAtPosition(i,2).widget())
                map_dict[wlabel]="'%s'" % wfield2
            else :
                map_dict[wlabel]=wfield
        return map_dict

    def return_text(self, witem) :
        if isinstance(witem, QLayoutItem):
            wtext = self.return_text(witem.widget())
        elif isinstance(witem, QLabel):
            wtext=witem.text()
        elif isinstance(witem, QLineEdit):
            wtext=witem.text()
        elif isinstance(witem, QComboBox):
            wtext=witem.currentText()
        elif isinstance(witem, QCheckBox):
            wtext=str(witem.isChecked())
        return wtext


class LinefromPLORDialog(QDialog):
    def __init__(self, plor_lyrnames, line_lyrnames, parent=None):
        super(LinefromPLORDialog, self).__init__(parent=None)

        self.parent = parent
        self.line_lyrnames = line_lyrnames

        self.layout = QFormLayout()
        self.layout.setLabelAlignment(Qt.AlignRight)

        self.plorlyr = QComboBox()
        self.plorlyr.addItems(plor_lyrnames)
        self.plorlyr.currentTextChanged.connect(self.updte_linelyrs)
        self.layout.addRow("Couche d'origine",self.plorlyr)

        self.ordertyp = QComboBox()
        self.ordertyp.addItems(['Ordonner par proximité', 'Ordonner par "Numero"'])
        self.layout.addRow("Type de tracé",self.ordertyp)

        self.linelyr = QComboBox()
        self.linelyr.addItems(self.linelyrs())
        self.layout.addRow("Couche de destination",self.linelyr)

        QBtn = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        self.buttonBox = QDialogButtonBox(QBtn, self)
        self.buttonBox.accepted.connect(self.accept_test)
        self.buttonBox.rejected.connect(self.reject)
        self.layout.addRow(self.buttonBox)

        self.setLayout(self.layout)
        self.setModal(True)
        self.setWindowTitle("Création d'un ouvrage linéaire a partir de points levés")

    def accept_test(self):
        if (self.plorlyr.currentText() and self.linelyr.currentText() and self.ordertyp.currentText() ) :
            print("ok")
            self.accept()

    def linelyrs(self):
        return [l for l in self.line_lyrnames if l[:3]==self.plorlyr.currentText()[:3]]

    def updte_linelyrs(self):
        self.linelyr.clear()
        self.linelyr.addItems(self.linelyrs())

    def get_output(self):
        return [self.plorlyr.currentText(),self.linelyr.currentText(),self.ordertyp.currentText()]

class NewProjDialog(QDialog):
    def __init__(self, items, parent=None):
        super(NewProjDialog, self).__init__(parent=None)

        self.setMinimumSize(500,200)
        form = QFormLayout(self)
        form.addRow(QLabel("Définir le répertoire de création du projet :"))
        self.selectPath = QPushButton("...")
        self.selectPath.clicked.connect(self.setPath)
        self.outPath = QLineEdit()
        self.outPath.setReadOnly(True)
        form.addRow(self.selectPath,self.outPath)
        form.addRow(QLabel("Entrer un nom pour le nouveau projet :"))
        self.projName = QLineEdit()
        form.addRow(self.projName)
        form.addRow(QLabel('Sélectionnner les métiers concernés :'))
        self.listView = QListView(self)
        form.addRow(self.listView)
        model = QStandardItemModel(self.listView)
        self.setWindowTitle('Configuration du projet')
        for item in items:
            # create an item with a caption
            standardItem = QStandardItem(item)
            standardItem.setCheckable(True)
            model.appendRow(standardItem)
        self.listView.setModel(model)

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal, self)
        form.addRow(buttonBox)
        buttonBox.accepted.connect(self.accept_test)
        buttonBox.rejected.connect(self.reject)

    def setPath(self):
        dir = QFileDialog.getExistingDirectory(QFileDialog(), "Sélectionnner  le répertoire", r"~/")
        self.outPath.setText(dir)
        return

    def accept_test(self):
        if (self.outPath.text() and self.projName.text() and len(self.itemsSelected()) > 0 ) :
            # print(self.outPath.text(), self.projName.text(), self.itemsSelected())
            self.accept()

    def itemsSelected(self):
        selected = []
        model = self.listView.model()
        i = 0
        while model.item(i):
            if model.item(i).checkState():
                selected.append(model.item(i).text())
            i += 1
        return selected

class RecoStarTools:

    def __init__(self, iface):
        # save reference to the QGIS interface
        self.iface = iface
        self.root = iface.layerTreeView().layerTreeModel().rootGroup()

    def initGui(self):
        self.toolbar = self.iface.addToolBar("RecoStarTools")
        self.toolbar.setObjectName("Outils OpenRecoStar")

        self.loadproject = QAction(QIcon(":/qgs_plugins/OpenRecoStarPlugin/icons/NewProjet.png"),
                                    "Créer un nouveau projet",
                                    self.iface.mainWindow())
        self.loadproject.setObjectName("LoadProject")
        self.loadproject.setWhatsThis("Créer un nouveau projet")
        self.loadproject.setStatusTip("Création projet")
        self.loadproject.triggered.connect(self.loadProject)
        self.toolbar.addAction(self.loadproject)

        self.importplor = QAction(QIcon(":/qgs_plugins/OpenRecoStarPlugin/icons/ImportPointLeve.png"),
                                    "Importer les Points Levés",
                                    self.iface.mainWindow())
        self.importplor.setObjectName("ImportPLOR")
        self.importplor.setWhatsThis("Importer les Points Levés")
        self.importplor.setStatusTip("Import des PLOR")
        self.importplor.triggered.connect(self.importPLOR)
        self.toolbar.addAction(self.importplor)

        self.linefromplor = QAction(QIcon(":/qgs_plugins/OpenRecoStarPlugin/icons/TracePointLeve.png"),
                                    "Tracer les lignes à partir des Points Levés",
                                    self.iface.mainWindow())
        self.linefromplor.setObjectName("LinefromPLOR")
        self.linefromplor.setWhatsThis("Tracer les lignes à partir des Points Levés")
        self.linefromplor.setStatusTip("Trace les ligne")
        self.linefromplor.triggered.connect(self.linefromPLOR)
        self.toolbar.addAction(self.linefromplor)

        self.gpkg2gml = QAction(QIcon(":/qgs_plugins/OpenRecoStarPlugin/icons/ExportGML.png"),
                                    "Exporter le geopackage en GML",
                                    self.iface.mainWindow())
        self.gpkg2gml.setObjectName("GPKG2GML")
        self.gpkg2gml.setWhatsThis("Exporter le geopackage en GML")
        self.gpkg2gml.setStatusTip("Export GML")
        self.gpkg2gml.triggered.connect(self.GPKG2GML)
        self.toolbar.addAction(self.gpkg2gml)

        self.toolbar.addSeparator()

        self.majgpkg = QAction([t for t in self.iface.digitizeToolBar().actions() if t.objectName()=='mActionToggleEditing'][0].icon(),
                                    "Mettre à jour le GPKG",
                                    self.iface.mainWindow())
        self.majgpkg.setObjectName("MAJGPKG")
        self.majgpkg.setWhatsThis("Mettre à jour le GPKG")
        self.majgpkg.setStatusTip("Ouvre / Ferme session édition")
        self.majgpkg.setCheckable(True)
        self.majgpkg.triggered.connect(self.editSession)
        self.toolbar.addAction(self.majgpkg)

        self.savegpkg = QAction([t for t in self.iface.digitizeToolBar().actions() if t.objectName()=='mActionSaveLayerEdits'][0].icon(),
                                    "Enregistrer les mises à jour dans le GPKG",
                                    self.iface.mainWindow())
        self.savegpkg.setObjectName("SAVEGPKG")
        self.savegpkg.setWhatsThis("Enregistrer les mises à jour dans le GPKG")
        self.savegpkg.setStatusTip("Sauvegarde les mises à jour")
        self.savegpkg.triggered.connect(self.saveEdits)
        self.savegpkg.setEnabled(False)
        self.toolbar.addAction(self.savegpkg)

        self.toolbar.addAction([t for t in self.iface.editMenu().actions() if t.objectName()=='mActionRotatePointSymbols'][0])

    def unload(self):
        del self.toolbar

    def getLayerFromTable(self, tablename, gpkg=None) :
        for layer in [l.layer() for l in self.root.findLayers() if l.layer().type() == QgsMapLayerType.VectorLayer] :
            if len(list(layer.dataProvider().uri().parameterKeys())) > 0 :
                if tablename == layer.dataProvider().uri().params(list(layer.dataProvider().uri().parameterKeys())[0])[0] :
                    if not gpkg or gpkg == layer.dataProvider().dataSourceUri().split('|')[0].strip() :
                        return layer

    def getLayersFromTable(self, tablename, gpkg=None) :
        layers = []
        for layer in [l.layer() for l in self.root.findLayers() if l.layer().type() == QgsMapLayerType.VectorLayer] :
            if len(list(layer.dataProvider().uri().parameterKeys())) > 0 :
                if tablename in layer.dataProvider().uri().params(list(layer.dataProvider().uri().parameterKeys())[0])[0] :
                    if not gpkg or gpkg == layer.dataProvider().dataSourceUri().split('|')[0].strip() :
                        layers.append(layer)
        return layers

    def messageBox(self, level, titre, message=None, detail=None, button=None, defbutton=None):
        msgBox = QMessageBox()
        if level == 'Info' :
            messlevel = Qgis.Info
            msgBox.setIcon(QMessageBox.Information)
        elif level == 'Question' :
            messlevel = Qgis.Info
            msgBox.setIcon(QMessageBox.Question)
        elif level == 'Success' :
            messlevel = Qgis.Success
            msgBox.setIcon(QMessageBox.Information)
        elif level == 'Warning' :
            messlevel = Qgis.Warning
            msgBox.setIcon(QMessageBox.Warning)
        elif level == 'Critical' :
            messlevel = Qgis.Critical
            msgBox.setIcon(QMessageBox.Critical)
        msgBox.setText(titre)
        if message :
            msgBox.setInformativeText(message)
        if detail :
            msgBox.setDetailedText(detail)
        if button :
            msgBox.setStandardButtons(button);
        if defbutton :
            msgBox.setDefaultButton(defbutton)
        rep = msgBox.exec()
        if not button :
            self.iface.messageBar().pushMessage(titre, message, detail, messlevel, duration=3)
        print(titre, message, detail, rep)
        return rep

    def loadGDALlib(self):
        envval = os.getenv('PATH')
        # We need to give some extra hints to get things picked up on OS X
        isDarwin = False
        try:
            isDarwin = platform.system() == 'Darwin'
        except OSError:  # https://travis-ci.org/m-kuhn/QGIS#L1493-L1526
            pass

        if isDarwin and os.path.isfile(os.path.join(QgsApplication.prefixPath(), "bin", "gdalinfo")):
            # Looks like there's a bundled gdal. Let's use it.
            if os.path.join(QgsApplication.prefixPath(), "bin") not in envval.split(os.pathsep) :
                os.environ['PATH'] = "{}{}{}".format(os.path.join(QgsApplication.prefixPath(), "bin"), os.pathsep, envval)
            os.environ['DYLD_LIBRARY_PATH'] = os.path.join(QgsApplication.prefixPath(), "lib")
        else:
            # Other platforms should use default gdal finder codepath
            settings = QgsSettings()
            path = settings.value('/GdalTools/gdalPath', '')
            if not path.lower() in envval.lower().split(os.pathsep):
                envval += f'{os.pathsep}{path}'
                os.putenv('PATH', envval)

    def createTempLyr(self, geomtype, geom): #Point, Linestring, Polygon
        tmplyr = QgsVectorLayer(geomtype+"?crs=epsg:2154", "tmp_"+geomtype, "memory")
        tmpfields = QgsFields()
        tmpfields.append(QgsField('id', QVariant.Int))
        tmpdata = tmplyr.dataProvider()
        tmpdata.addAttributes(tmpfields)
        tmplyr.updateFields()
        tmplyrenderer = tmplyr.renderer()
        tmplyrsymbol = tmplyrenderer.symbol()
        tmplyrsymbol.setColor(QColor.fromRgb(255, 0, 0))
        if geomtype.lower() == 'point' :
            tmplyrsymbol.setSize(3)
        elif geomtype.lower() == 'linestring' :
            tmplyrsymbol.setWidth(1.5)
        tmplyr.triggerRepaint()
        tmpfeat = QgsFeature(tmplyr.fields())
        tmpfeat.setAttribute('id', 1)
        tmpfeat.setGeometry(geom)
        tmplyr.startEditing()
        tmplyr.addFeature(tmpfeat)
        tmplyr.commitChanges(True)
        QgsProject.instance().addMapLayer(tmplyr)
        QgsProject.instance().reloadAllLayers()
        return tmplyr, tmpfeat

    def editSession(self) :
        project = QgsProject.instance()
        if len(self.iface.editableLayers()) > 0:
            # TODO: confirmer enregistrement
            if len(self.iface.editableLayers(True)) > 0 :
                # print(iface.editableLayers(True)[0].name())
                self.iface.editableLayers(True)[0].commitChanges(True)
            else :
                self.iface.editableLayers()[0].commitChanges(True)
            self.majgpkg.setChecked(False)
            self.savegpkg.setEnabled(False)
        else:
            if self.iface.activeLayer() :
                # print(iface.activeLayer().name())
                self.iface.activeLayer().startEditing()
                for t in self.iface.digitizeToolBar().actions():
                    #print(t.objectName(), t.iconText())
                    if t.objectName() in ['mActionAllEdits','mActionToggleEditing','mActionSaveLayerEdits'] :
                        t.setVisible(False)
                self.majgpkg.setChecked(True)
                self.savegpkg.setEnabled(True)
        project.reloadAllLayers ()
        return

    def saveEdits(self) :
        project = QgsProject.instance()
        if len(self.iface.editableLayers()) > 0:
            if len(self.iface.editableLayers(True)) > 0 :
                # print(iface.editableLayers(True)[0].name())
                layer=self.iface.editableLayers(True)[0]
                layer.commitChanges(True) #True = Stop Editing
                project.reloadAllLayers()
                layer.startEditing()
                return

    def loadProject(self) :
        print("loadProject: run called!")
        metiers={'RPD':'RPD - Réseau Public de Distribution', 'EP' : 'EP - Eclairage Public'}
        dial = NewProjDialog(list(metiers.values()))
        if dial.exec_() == QDialog.Accepted:
            projname=dial.projName.text().strip().replace(' ', '_')
            newpath=dial.outPath.text()+r'/'+projname
            print(newpath)
            schms=[m for m in metiers if metiers[m] in dial.itemsSelected()]
            print(dial.itemsSelected(), schms)
            if 'EP' in schms: #message en attente de finalisation de l'EP
                self.messageBox('Info', "Info Eclairage Public", "Le modèle de données Eclairage Public est en phase de développement. La fonctionnalité d'export GML n'est pas encore disponible pour l'EP.")
            if not os.path.exists(newpath):
                networkAccessManager = QgsNetworkAccessManager.instance()
                networkAccessManager.useSystemProxy()
                os.makedirs(newpath+r"/gpkg")
                for schm in schms :
                    url="https://gitlab.com/StaR-Elec/OpenRecoStar/-/raw/master/gpkg/Reco-Star-Elec-{0}.gpkg".format(schm)
                    req = QNetworkRequest(QUrl(url))
                    reply = networkAccessManager.blockingGet(req)
                    with open(newpath+r"/gpkg/"+os.path.basename(url), "wb") as f :
                       f.write(reply.content().data())
                qgsname='Reco-Star-Elec_'+projname+'.qgs'
                # url="https://gitlab.com/StaR-Elec/OpenRecoStar/-/raw/master/qgs/Reco-Star-Elec.qgs"
                # req = QNetworkRequest(QUrl(url))
                # reply = networkAccessManager.blockingGet(req)
                # with open(newpath+r"/"+qgsname, "wb") as f :
                #    f.write(reply.content().data())
                newProject=QgsProject()
                newProject.writePath(newpath+r"/"+qgsname)
                for schm in schms + ['FDP']:
                    url="https://gitlab.com/StaR-Elec/OpenRecoStar/-/raw/master/qgs/Reco-Star-Elec_{0}.qlr".format(schm)
                    req = QNetworkRequest(QUrl(url))
                    reply = networkAccessManager.blockingGet(req)
                    with open(newpath+r"/"+os.path.basename(url), "wb") as f :
                        f.write(reply.content().data())
                    QgsLayerDefinition.loadLayerDefinition(newpath+r"/"+os.path.basename(url), newProject, newProject.layerTreeRoot())
                    os.remove(newpath+r"/"+os.path.basename(url))
                # print(newProject.transactionMode(), newProject.evaluateDefaultValues(), newProject.crs())
                newProject.setCrs(QgsCoordinateReferenceSystem("EPSG:2154"))
                newProject.setTransactionMode(Qgis.TransactionMode.AutomaticGroups)
                newProject.setEvaluateDefaultValues(True)
                newProject.write(newpath+r"/"+qgsname)
                self.iface.messageBar().pushMessage("Création projet terminé", newpath, Qgis.Success)
                rep = self.messageBox('Question', "Souhaitez vous ouvrir le projet ?", qgsname, "Le fichier : "+newpath+r"/"+qgsname+", sera ouvert dans QGIS", QMessageBox.Ignore | QMessageBox.Open, QMessageBox.Open)
                if rep == QMessageBox.Open :
                    project = QgsProject.instance()
                    project.read(newpath+r"/"+qgsname)
                    self.iface.messageBar().pushMessage("Projet chargé", qgsname, Qgis.Success)
            else :
                self.iface.messageBar().pushMessage("Création projet abandonnée", 'Un projet nommé "{0}" existe déja dans le répertoire'.format(projname), Qgis.Warning, duration=5)
                return
        else :
            self.iface.messageBar().pushMessage("Création projet abandonnée", "Action annulée par l'utilisateur", Qgis.Warning, duration=5)
            return

    def importPLOR(self) :
        print("importPLOR: run called!")
        canvas=self.iface.mapCanvas()
        file = QFileDialog.getOpenFileName(QFileDialog(), "Importer le fichier", filter="CSV / SHP (*.csv *.shp)")
        print(file)
        if file[0] :
            vlayer = QgsVectorLayer(file[0], "importPLOR_lyr", "ogr")
            file_type = file[0][-3:].lower()
            if not vlayer.isValid():
                print("Layer failed to load!")
                self.iface.messageBar().pushMessage("Impossible de charger la couche", Qgis.Critical)
                return
            else:
                # Qgsproject=QgsProject.instance()
                # Qgsproject.instance().addMapLayer(vlayer)
                vlyr_attrib =  vlayer.fields().names()
                plor_lyrs = self.getLayersFromTable('PointLeveOuvrageReseau')
                plor_lyrnames = [lyr.name() for lyr in plor_lyrs]
                plor_lyrname, ctrl = QInputDialog.getItem(QInputDialog(), "Couche", "Choisir la couche dans laquelle importer les données", plor_lyrnames, 0)
                if ctrl :
                    plor_lyr = plor_lyrs[plor_lyrnames.index(plor_lyrname)]
                    if not plor_lyr.dataProvider().transaction():
                        plor_lyr.startEditing()
                    if plor_lyr.dataProvider().transaction():
                        #TODO: enregistrer ce qui est dans le buffer
                        self.iface.messageBar().clearWidgets()
                        formlayout = {}
                        for field in [f for f in plor_lyr.fields() if f.name() not in ['fid', 'ogr_pkid', 'pkid', 'id', 'Leve_uom', 'Z']] :
                            attrib_stp = plor_lyr.editorWidgetSetup(plor_lyr.fields().indexFromName(field.name()))
                            # print(field.name(), attrib_stp.config())
                            if not attrib_stp.isNull() :
                                if attrib_stp.type() == 'ValueRelation':
                                    value_lyr = self.root.findLayer(attrib_stp.config()['Layer']).layer()
                                    value_list = [f['valeurs'] for f in value_lyr.getFeatures()]
                                    sort_attrib = self.sortsimilarity(field.name(), vlyr_attrib)
                                    if sort_attrib[1] > 0.5 :
                                        formlayout[field.name()] = ('ComboBox',sort_attrib[0]+['<AUTRE>'],value_list)
                                    else :
                                        formlayout[field.name()] = ('ComboBox',['<AUTRE>']+sort_attrib[0],value_list)
                                else :
                                    defvalue_lyr=plor_lyr.defaultValue(plor_lyr.fields().indexFromName(field.name()))
                                    sort_attrib = self.sortsimilarity(field.name(), vlyr_attrib)
                                    if sort_attrib[1] > 0.5 :
                                        formlayout[field.name()] = ('ComboBox',sort_attrib[0]+['<AUTRE>'],defvalue_lyr)
                                    else :
                                        formlayout[field.name()] = ('ComboBox',['<AUTRE>']+sort_attrib[0],defvalue_lyr)
                                    # TODO ajouter des infos sur le type d'attribut
                        srslyr = self.getLayerFromTable('SRSValue')
                        dictSRS = {}
                        for f in srslyr.getFeatures():
                            dictSRS[f.attribute('alias')]=f.attribute('valeurs')
                        if file_type == 'csv' :
                            formlayout['GeomX'] = ('ComboBox',self.sortsimilarity('X', vlyr_attrib)[0],None)
                            formlayout['GeomY'] = ('ComboBox',self.sortsimilarity('Y', vlyr_attrib)[0],None)
                            formlayout['GeomZ'] = ('ComboBox',self.sortsimilarity('Z', vlyr_attrib)[0],None)
                            formlayout['Z TN ?'] = ('CheckBox',None,None)
                            formlayout['GeomEPSG'] = ('ComboBox', list(dictSRS.keys())+['<AUTRE>'],None) #TODO : utiliser la liste de valeur metadata
                        dial = MappingDialogBox("Import fichier PLOR", formlayout, self)
                        if dial.exec() == QDialog.Accepted:
                            mapping=dial.get_output()
                            print(mapping)
                            for vfeat in vlayer.getFeatures() :
                                pfeat=QgsFeature(plor_lyr.fields())
                                pfeat.setAttribute('id', 'id'+str(uuid.uuid4()))
                                #pfeat.setAttribute('Leve_uom', plor_lyr.defaultValue(plor_lyr.fields().indexFromName('Leve_uom')))
                                if file_type == 'csv' :
                                    # print(vfeat.attribute(mapping['GeomX']), vfeat.attribute(mapping['GeomY']), vfeat.attribute(mapping['GeomZ']))
                                    Z = float(vfeat.attribute(mapping['GeomZ']))
                                    if not (mapping['Leve'][0] == "'" and mapping['Leve'][-1] == "'")\
                                    and mapping['TypeLeve'] == "'ChargeGeneratrice'" and mapping['Z TN ?'] == 'True' :
                                        if not vfeat.attribute(mapping['Leve']) == '' and vfeat.attribute(mapping['Leve']) :
                                            Z = float(vfeat.attribute(mapping['GeomZ']))-float(vfeat.attribute(mapping['Leve']))
                                    geom_point=QgsGeometry(QgsPoint(float(vfeat.attribute(mapping['GeomX'])), float(vfeat.attribute(mapping['GeomY'])), Z))
                                    transform = QgsCoordinateTransform()
                                    geom_point.transform(QgsCoordinateTransform(QgsCoordinateReferenceSystem(dictSRS[mapping['GeomEPSG']]), QgsCoordinateReferenceSystem("EPSG:2154"), QgsProject.instance()))
                                    pfeat.setGeometry(geom_point)
                                elif file_type == 'shp' :
                                    geom_shp=vfeat.geometry()
                                    geom_shp.transform(QgsCoordinateTransform(vlayer.dataProvider().sourceCrs(), QgsCoordinateReferenceSystem("EPSG:2154"), QgsProject.instance()))
                                    pfeat.setGeometry(geom_shp)
                                for m in mapping :
                                    fset=False
                                    if (file_type == 'csv' and m[:4] != 'Geom' and m != 'Z TN ?') or file_type != 'csv' :
                                        # print(m, mapping[m])
                                        if m == 'Leve' :
                                            if mapping['Leve'] == "'$z'" :
                                                # print ('getZ', pfeat.geometry().get().z())
                                                pfeat.setAttribute(m, pfeat.geometry().get().z())
                                                fset=True
                                            elif not (mapping['Leve'][0] == "'" and mapping['Leve'][-1] == "'") :
                                                if vfeat.attribute(mapping['Leve']) == '' or not vfeat.attribute(mapping['Leve']) :
                                                    # print ('null getZ', pfeat.geometry().get().z())
                                                    pfeat.setAttribute(m, pfeat.geometry().get().z())
                                                    fset=True
                                        elif m == 'TypeLeve' :
                                            if mapping['Leve'] == "'$z'" :
                                                pfeat.setAttribute(m, "AltitudeGeneratrice")
                                                fset=True
                                            elif not (mapping['Leve'][0] == "'" and mapping['Leve'][-1] == "'") :
                                                if vfeat.attribute(mapping['Leve']) == '' or not vfeat.attribute(mapping['Leve']) :
                                                    pfeat.setAttribute(m, "AltitudeGeneratrice")
                                                    fset=True
                                        if mapping[m][0] == "'" and mapping[m][-1] == "'" and not fset:
                                            pfeat.setAttribute(m, mapping[m].strip("'"))
                                        elif not fset :
                                            pfeat.setAttribute(m, vfeat.attribute(mapping[m]))
                                result=plor_lyr.addFeature(pfeat)
                            canvas.zoomToFeatureExtent(pfeat.geometry().boundingBox().buffered(50))

                            def valid_accept():
                                self.iface.messageBar().clearWidgets()
                                commit=plor_lyr.commitChanges()
                                if commit == False :
                                    plor_lyr.rollBack()
                                    self.messageBox('Critical', "Import échoué", "Enregistrement des mises à jour impossibles")
                                    return
                                else :
                                    self.iface.messageBar().clearWidgets()
                                    self.iface.messageBar().pushMessage("Import réussi", Qgis.Success)
                                    QgsProject.instance().reloadAllLayers()
                                    return

                            def valid_reject():
                                self.iface.messageBar().clearWidgets()
                                plor_lyr.rollBack()
                                self.iface.messageBar().pushMessage("Import abandonné", "Action annulée par l'utilisateur", Qgis.Warning, duration=5)
                                return

                            ValidMessageBar = self.iface.messageBar().createMessage("Validation", "Valider l'import de ces données?")
                            QBtn = QDialogButtonBox.No | QDialogButtonBox.Yes
                            buttonBox = QDialogButtonBox(QBtn)
                            ValidMessageBar.layout().addWidget(buttonBox)
                            buttonBox.accepted.connect(valid_accept)
                            buttonBox.rejected.connect(valid_reject)
                            self.iface.messageBar().pushWidget(ValidMessageBar,Qgis.Warning,duration=0)
                        else:
                            self.iface.messageBar().pushMessage("Import abandonné", "Action annulée par l'utilisateur", Qgis.Warning, duration=5)
                            return
                    else:
                        self.messageBox('Critical', "Import abandonné", "L'import ne peut aboutir", "Aucune session de mise à jour n'est ouverte")
                        return
        else :
            self.iface.messageBar().pushMessage("Import abandonné", "Action annulée par l'utilisateur", Qgis.Warning, duration=5)
            return

    def similarity(self, a, b) :
        def match(a, b) :
            score, cnt =  0, 0
            for i in range(0, max(len(a)-2 , 1)):
                if a[i:i+3].lower() in b.lower() :
                    score += 1
                cnt+=1
            return score/cnt
        score1 = match(a, b)
        score2 = match(b, a)
        return (score1+score2)/2

    def sortsimilarity(self, a, l) :
        newlist = []
        for i in l :
            score=self.similarity(a,i)
            newlist.append((i,score))
        newlist.sort(reverse=True, key=lambda index: index[1])
        return [n for n, s in newlist], max([s for n, s in newlist])

    def allPointLayerSelected(self) :
        lyrs = []
        for layer in [l.layer() for l in self.root.findLayers() if l.layer().type() == QgsMapLayerType.VectorLayer and l.layer().geometryType() == QgsWkbTypes.PointGeometry] : ## 0 = VectorLayer
            if layer.selectedFeatureIds():
                lyrs.append(layer)
        return lyrs

    def allLineLayers(self) :
        layers = []
        for layer in [l.layer() for l in self.root.findLayers() if l.layer().type() == QgsMapLayerType.VectorLayer and l.layer().geometryType() == QgsWkbTypes.LineGeometry] :
            layers.append(layer)
        return layers

    def linefromPLOR(self) :
        print("linefromPLOR: run called!")
        canvas=self.iface.mapCanvas()
        plor_lyrs = self.allPointLayerSelected()
        plor_lyr = None
        if plor_lyrs :
            plor_lyrnames = [lyr.name() for lyr in plor_lyrs]
        else :
            plor_lyrs = self.getLayersFromTable('PointLeveOuvrageReseau')
            plor_lyrnames = [lyr.name() for lyr in plor_lyrs]
        line_lyrs=[lyr for lyr in self.allLineLayers() if lyr.name()[:4] != 'ctrl']
        line_lyrnames = [lyr.name() for lyr in line_lyrs]
        dial = LinefromPLORDialog(plor_lyrnames, line_lyrnames, self)
        if dial.exec() == QDialog.Accepted:
            plor_lyrname,line_lyrname,order_type=dial.get_output()
            plor_lyr = plor_lyrs[plor_lyrnames.index(plor_lyrname)]
            if len(plor_lyr.selectedFeatures()) == 0:
                codouvgs = []
                for f in plor_lyr.getFeatures():
                    if str(f['CodeOuvrage']) not in codouvgs and not str(f['CodeOuvrage']) == 'NULL':
                        codouvgs.append(str(f['CodeOuvrage']))
                codouvgs.sort()
                codouvg, ctrl = QInputDialog.getItem(QInputDialog(), "CodeOuvrage PLOR", "Choisir le Code Ouvrage à tracer", codouvgs, 0)
                if ctrl :
                    # TODO: limiter sélection 1KM autour du zoom carte
                    plor_lyr.selectByExpression("CodeOuvrage='{0}'".format(codouvg))
            if len(plor_lyr.selectedFeatures()) > 0:
                if order_type == 'Ordonner par "Numero"' :
                    # ORDONNE LES POINTS PAR NUMERO
                    plf=plor_lyr.selectedFeatures()
                    plf.sort(key=lambda element: element['NumeroPoint'].zfill(10))
                    orderedid=[f.id() for f in plf]
                else :
                    # ORDONNE LES POINTS PAR PROXIMITE
                    maxdist, extremid, orderedid = 0, None, []
                    for f in plor_lyr.selectedFeatures():
                        for t in plor_lyr.selectedFeatures():
                            if t.id() != f.id() :
                                distance=t.geometry().distance(f.geometry())
                                # print(t.id(),f.id(),distance)
                                if distance > maxdist :
                                    maxdist = distance
                                    extremid = f.id()
                    # print(extremid)
                    orderedid.append(extremid)
                    while len(orderedid) < plor_lyr.selectedFeatureCount() :
                        mindist=99
                        for f in plor_lyr.selectedFeatures():
                            if f.id() not in orderedid :
                                distance=plor_lyr.getFeature(orderedid[-1]).geometry().distance(f.geometry())
                                # print(orderedid[-1],f.id(),distance)
                                if distance < mindist :
                                    mindist = distance
                                    extremid = f.id()
                        orderedid.append(extremid)
                        # print(extremid,orderedid)
                if len(orderedid) > 0 :
                    geom_line=QgsGeometry()
                    geom_line.addPoints([plor_lyr.getFeature(i).geometry().vertexAt(0) for i in orderedid], QgsWkbTypes.LineGeometry)
                    # print(geom_line)
                    canvas.zoomToFeatureExtent(geom_line.boundingBox().buffered(10))
                    tmplyr, tmpfeat=self.createTempLyr('LineString', geom_line)
                    def valid_accept():
                        self.iface.messageBar().clearWidgets()
                        QgsProject.instance().removeMapLayer(tmplyr)
                        line_lyr = line_lyrs[line_lyrnames.index(line_lyrname)]
                        if not line_lyr.dataProvider().transaction():
                            line_lyr.startEditing()
                        if line_lyr.dataProvider().transaction():
                            #TODO: enregistrer ce qui est dans le buffer
                            feat=QgsFeature(line_lyr.fields())
                            feat.setAttribute('id', 'id'+str(uuid.uuid4()))
                            for field in [f for f in line_lyr.fields() if f.name() not in ['fid', 'ogr_pkid', 'pkid', 'id']] :
                                defvalue_lyr=line_lyr.defaultValue(line_lyr.fields().indexFromName(field.name()))
                                if defvalue_lyr and not defvalue_lyr == '':
                                    feat.setAttribute(field.name(), defvalue_lyr)
                            valid=self.iface.openFeatureForm(line_lyr, feat)
                            if valid :
                                feat.setGeometry(geom_line)
                                result=line_lyr.addFeature(feat)
                                if result == True :
                                    commit=line_lyr.commitChanges()
                                    if commit == False :
                                        line_lyr.rollBack()
                                        self.messageBox('Critical', "Création échouée", "Enregistrement des mises à jour impossibles")
                                        return
                                    else :
                                        self.iface.messageBar().clearWidgets()
                                        self.iface.messageBar().pushMessage("Création réussie", Qgis.Success)
                                        QgsProject.instance().reloadAllLayers()
                                        plor_lyr.removeSelection()
                                        return
                                else:
                                    self.messageBox('Critical', "Création échouée", "La création de la ligne ne peut aboutir")
                                    return
                            else:
                                self.iface.messageBar().pushMessage("Création abandonnée", "Action annulée par l'utilisateur", Qgis.Warning, duration=5)
                                return
                        else:
                            self.messageBox('Critical', "Création abandonnée", "La création de la ligne ne peut aboutir", "Aucune session de mise à jour n'est ouverte")
                            return

                    def valid_reject():
                        self.iface.messageBar().clearWidgets()
                        QgsProject.instance().removeMapLayer(tmplyr)
                        plor_lyr.removeSelection()
                        self.iface.messageBar().pushMessage("Création abandonnée", "Action annulée par l'utilisateur", Qgis.Warning, duration=5)
                        return

                    ValidMessageBar = self.iface.messageBar().createMessage("Validation", "Valider la création de cette ligne?")
                    QBtn = QDialogButtonBox.No | QDialogButtonBox.Yes
                    buttonBox = QDialogButtonBox(QBtn)
                    ValidMessageBar.layout().addWidget(buttonBox)
                    buttonBox.accepted.connect(valid_accept)
                    buttonBox.rejected.connect(valid_reject)
                    self.iface.messageBar().pushWidget(ValidMessageBar,Qgis.Warning,duration=0)
        else :
            self.iface.messageBar().pushMessage("Création abandonnée", "Action annulée par l'utilisateur", Qgis.Warning, duration=5)
            return

    def findGMLattrib(self, gml, id, classname, attribname):
        if os.path.isfile(gml):
            fline, tline = None, None
            lines=open(gml).readlines()
            for line in lines:
                #print(line, lines.index(line))
                if classname+' gml:id="'+id+'"' in line :
                    fline=lines.index(line)
                    #print(line, lines.index(line))
                    for line2 in lines[fline:] :
                        if '</RecoStaR:'+classname+'>' in line2:
                            tline=fline+lines[fline:].index(line2)
                            break
                    #print(fline,tline)
                    break
            if fline and tline :
                for attrib in lines[fline:tline+1] :
                    if '<RecoStaR:'+attribname+'>' in attrib :
                        aline=fline+lines[fline:tline+1].index(attrib)
                        print(aline, attrib)
                        return aline, attrib
                        break

    def getDatasourceGPKG(self) :
        gpkgs = []
        for layer in [l.layer() for l in self.root.findLayers() if l.layer().type() == QgsMapLayerType.VectorLayer] :
            if '.gpkg' in layer.dataProvider().dataSourceUri():
                gpkg = layer.dataProvider().dataSourceUri().split('|')[0].strip()
                if gpkg not in gpkgs :
                    gpkgs.append(gpkg)
        return gpkgs

    def GPKG2GML(self) :
        print("GPKG2GML: run called!")
        gpkgs = self.getDatasourceGPKG()
        gpkg, ctrl = QInputDialog.getItem(QInputDialog(), "Geopackage vers GML", "Choisir le geopackage à exporter", gpkgs, 0)
        print(gpkg)
        if gpkg[-7:] == 'EP.gpkg': #message en attente de finalisation de l'EP
            self.messageBox('Info', "Info Eclairage Public GML", "Le modèle de données Eclairage Public est en phase de développement. La fonctionnalité d'export GML n'est pas encore disponible pour l'EP.")
            return
        if ctrl :
            message = []
            for ctrltable in ('ctrl_Cheminement_Cable', 'ctrl_Noeud_Cable', 'ctrl_Noeud_Conteneur', 'ctrl_PLOR_Ouvrage'):
                ctrllyr = self.getLayerFromTable(ctrltable,gpkg)
                if ctrllyr.featureCount() > 0 :
                    message.append(ctrltable+' : '+str(ctrllyr.featureCount())+' erreurs')
            if len(message) > 0 :
                rep = self.messageBox('Warning', "Contrôle de cohérence topologique", "Il semble qu'il reste quelques erreurs topologiques. Voulez-vous quand même exporter?", '\r\n'.join(message)+'\r\n \r\nSi vous exportez maintenant, le fichier GML produit ne sera probablement pas valide.', QMessageBox.No | QMessageBox.Yes, QMessageBox.No)
                if rep == QMessageBox.No :
                    self.iface.messageBar().pushMessage("Export abandonné", "Corrections topologiques nécessaires", Qgis.Warning, duration=5)
                    return
            outputGML = QFileDialog.getSaveFileName(QFileDialog(), "Définir le fichier de destination", filter="GML (*.gml)")
            print(outputGML)
            if outputGML[0] :
                reseaulyr = self.getLayerFromTable('ReseauUtilite',gpkg)
                if not reseaulyr.dataProvider().transaction():
                    reseaulyr.startEditing()
                if reseaulyr.dataProvider().transaction():
                    reseaufeat = reseaulyr.getFeature(1)
                    valid=self.iface.openFeatureForm(reseaulyr, reseaufeat)
                    if valid :
                        result1=reseaulyr.updateFeature(reseaufeat)
                        if result1 == True :
                            commit=reseaulyr.commitChanges()
                            if commit == False :
                                reseaulyr.rollBack()
                                self.messageBox('Critical', "Export échoué", "Enregistrement des mises à jour ReseauUtilite impossibles")
                                return
                        else :
                            reseaulyr.rollBack()
                            self.messageBox('Critical', "Export échoué", "Impossible de mettre à jour ReseauUtilite")
                            return
                    else :
                        reseaulyr.rollBack()
                        self.iface.messageBar().pushMessage("Export abandonné", "Action annulée par l'utilisateur", Qgis.Warning, duration=5)
                        return
                else:
                    self.messageBox('Critical', "Export abandonné", "L'export ne peut aboutir", "Aucune session de mise à jour n'est ouverte pour ReseauUtilite")
                    return

                metadatalyr = self.getLayerFromTable('Metadata',gpkg)
                if not metadatalyr.dataProvider().transaction():
                    metadatalyr.startEditing()
                if metadatalyr.dataProvider().transaction():
                    if metadatalyr.featureCount() > 0 :
                        metadatafeat = metadatalyr.getFeature(1)
                        valid=self.iface.openFeatureForm(metadatalyr, metadatafeat)
                        if valid :
                            result2=metadatalyr.updateFeature(metadatafeat)
                        else :
                            metadatalyr.rollBack()
                            self.iface.messageBar().pushMessage("Export abandonné", "Action annulée par l'utilisateur", Qgis.Warning, duration=5)
                            return
                    else :
                        metadatafeat=QgsFeature(metadatalyr.fields())
                        metadatafeat.setAttribute('id', 'id'+str(uuid.uuid4()))
                        for field in [f for f in metadatalyr.fields() if f.name() not in ['fid', 'ogr_pkid', 'pkid', 'id']] :
                            defvalue_lyr=metadatalyr.defaultValue(metadatalyr.fields().indexFromName(field.name()))
                            if defvalue_lyr and not defvalue_lyr == '':
                                metadatafeat.setAttribute(field.name(), defvalue_lyr)
                        valid=self.iface.openFeatureForm(metadatalyr, metadatafeat)
                        if valid :
                            print
                            result2=metadatalyr.addFeature(metadatafeat)
                        else :
                            metadatalyr.rollBack()
                            self.iface.messageBar().pushMessage("Export abandonné", "Action annulée par l'utilisateur", Qgis.Warning, duration=5)
                            return
                    if result2 == True :
                        commit=metadatalyr.commitChanges()
                        if commit == False :
                            metadatalyr.rollBack()
                            self.messageBox('Critical', "Export échoué", "Enregistrement des mises à jour Metadata impossibles")
                            return
                    else :
                        metadatalyr.rollBack()
                        self.messageBox('Critical', "Export échoué", "Impossible de mettre à jour Metadata")
                        return
                else:
                    self.messageBox('Critical', "Export abandonné", "L'export ne peut aboutir", "Aucune session de mise à jour n'est ouverte pour Metadata")
                    return

                networkAccessManager = QgsNetworkAccessManager.instance()
                networkAccessManager.useSystemProxy()
                url="https://gitlab.com/StaR-Elec/OpenRecoStar/-/raw/master/sql/create_gpkg_gml_href_rpd.sql"
                req = QNetworkRequest(QUrl(url))
                reply = networkAccessManager.blockingGet(req)
                qlines = reply.content().data().decode('utf-8')
                queries = " ".join([l for l in qlines.splitlines() if not l[:2]=='--'])
                if not reseaulyr.dataProvider().transaction():
                    reseaulyr.startEditing()
                if reseaulyr.dataProvider().transaction():
                    for query in queries.split(';'):
                        # print(query.strip()+';')
                        if query not in ('', ' ') :
                            result3=reseaulyr.dataProvider().transaction().executeSql(query.strip()+';',False)
                            if result3[0] == False :
                                reseaulyr.rollBack()
                                self.messageBox('Critical', "Export échoué", "Echec de la requête d'export GML", result3[1])
                                return
                    commit=reseaulyr.commitChanges()
                    if commit == False :
                        reseaulyr.rollBack()
                        self.messageBox('Critical', "Export échoué", "Enregistrement des mises à jour GML impossibles")
                        return
                else:
                    self.messageBox('Critical', "Export abandonné", "L'export ne peut aboutir", "Aucune session de mise à jour n'est ouverte")
                    return

                #TODO: calculer le chemin vers la XSD en fonction du métier (RPD / EP / ... )
                xsd='https://gitlab.com/StaR-Elec/StaR-Elec/-/raw/main/RecoStaR/SchemaStarElecRecoStar.xsd'
                version='v0.7' # QUESTION : comment récupérer la version de la xsd
                self.loadGDALlib()
                tmpgml=outputGML[0][:-4]+'_tmp.gml'
                command='''ogr2ogr -f GMLAS "{4}" "{5}" -a_srs "EPSG:2154" -dsco INPUT_XSD="{0}" -dsco SRSNAME_FORMAT=SHORT -dsco WRAPPING=GMLAS_FEATURECOLLECTION -dsco GENERATE_XSD=NO -dsco LINEFORMAT=LF -dsco INDENT_SIZE=2 -dsco COMMENT="GML au format OpenRecoStar {1} via GDAL / QGIS.
DATE: {2}
GPKG: {3}"'''.format(xsd, version, datetime.datetime.now().isoformat(), os.path.basename(gpkg), tmpgml, gpkg)
                print(command)
                result=subprocess.run(command, shell=True, capture_output=True, encoding='utf-8')
                print(result)
                if result.returncode > 1 :
                    self.messageBox('Critical', "Echec export GML", "L'export GML (1) ne peut aboutir : Erreur {0}".format(result.returncode), result.stderr)
                    return

                tmpgmlgeom0=outputGML[0][:-4]+'_geomtmp0.gml'
                command='ogr2ogr -f GML "{0}" "{1}" -sql "select ogr_pkid, id, \\"Ligne2.5D\\" from RPD_GeometrieSupplementaire_Reco" -nln RPD_GeometrieSupplementaire_Reco -a_srs "EPSG:2154" -dsco SRSNAME_FORMAT=SHORT -dsco WRITE_FEATURE_BOUNDED_BY=NO'.format(tmpgmlgeom0, gpkg)
                print(command)
                result=subprocess.run(command, shell=True, capture_output=True, encoding='utf-8')
                print(result)
                if result.returncode > 1 :
                    self.messageBox('Critical', "Echec export GML", "L'export GML (2) ne peut aboutir : Erreur {0}".format(result.returncode), result.stderr)
                    return

                tmpgmlgeom=outputGML[0][:-4]+'_geomtmp.gml'
                command='ogr2ogr -f GMLAS "{1}" "{2}" -dsco INPUT_XSD="{0}" -a_srs "EPSG:2154" -dsco SRSNAME_FORMAT=SHORT -dsco WRAPPING=GMLAS_FEATURECOLLECTION -dsco GENERATE_XSD=NO -dsco LINEFORMAT=LF -dsco INDENT_SIZE=2'.format(xsd, tmpgmlgeom, tmpgmlgeom0)
                print(command)
                result=subprocess.run(command, shell=True, capture_output=True, encoding='utf-8')
                print(result)
                if result.returncode > 1 :
                    self.messageBox('Critical', "Echec export GML", "L'export GML (3) ne peut aboutir : Erreur {0}".format(result.returncode), result.stderr)
                    return

                if os.path.isfile(tmpgmlgeom0):
                    os.remove(tmpgmlgeom0)
                    os.remove(tmpgmlgeom0[:-4]+'.xsd')

                if os.path.isfile(tmpgml) and os.path.isfile(tmpgmlgeom):
                    lines=open(tmpgml).readlines()
                    for line in lines:
                        if 'GeometrieSupplementaire_Reco gml:id=' in line :
                            iline=lines.index(line)
                            classname=line.split(':')[1][:-4]
                            id=line.split('gml:id=')[1].rstrip().strip('>').strip('"')
                            # print(iline, classname, id)
                            rline, rattrib = self.findGMLattrib(tmpgml, id, classname, 'Ligne2.5D')
                            wline, wattrib = self.findGMLattrib(tmpgmlgeom, id, classname, 'Ligne2.5D')
                            lines[rline]=wattrib
                    with open(outputGML[0], 'w') as outfile:
                        for line in lines:
                            if not 'xsi:nil="true"></' in line :
                                if 'ogr_gmlas:featureMember' in line :
                                    line = line.replace('ogr_gmlas:featureMember', 'gml:featureMember')
                                if 'ogr_gmlas:FeatureCollection' in line :
                                    line = line.replace('ogr_gmlas:FeatureCollection', 'gml:FeatureCollection')
                                outfile.write(line)
                    os.remove(tmpgml)
                    os.remove(tmpgmlgeom)
                    # networkAccessManager = QgsNetworkAccessManager.instance()
                    # networkAccessManager.useSystemProxy()
                    url="https://gitlab.com/StaR-Elec/OpenRecoStar/-/raw/master/qgs/Reco-Star-Elec_RPD_GML.qgs"
                    req = QNetworkRequest(QUrl(url))
                    reply = networkAccessManager.blockingGet(req)
                    with open(os.path.dirname(outputGML[0])+'//'+os.path.basename(outputGML[0])[:-4]+'.qgs', "wb") as f :
                       f.write(reply.content().data().decode('utf-8').replace('./Reco-Star-Elec_RPD.gml', './'+os.path.basename(outputGML[0])).encode())
                    self.iface.messageBar().pushMessage("Export terminé", outputGML[0], Qgis.Success)
                    return
                else:
                    self.messageBox('Critical', "Echec export GML", "Le traitement n'a pas retourné de GML en sortie")
                    return
            else :
                self.iface.messageBar().pushMessage("Export abandonné", "Action annulée par l'utilisateur", Qgis.Warning, duration=5)
                return
        else :
            self.iface.messageBar().pushMessage("Export abandonné", "Action annulée par l'utilisateur", Qgis.Warning, duration=5)
            return
