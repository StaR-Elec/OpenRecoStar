from osgeo import ogr
from qgis.utils import iface
from qgis.core import *
from qgis.gui import *
import os

path=QgsProject.instance().readPath("./")
root=iface.layerTreeView().layerTreeModel().rootGroup()

def getLayerFromTable(tablename) :
    for layer in [l.layer() for l in root.findLayers() if l.layer().type() == QgsMapLayerType.VectorLayer] :
        if tablename in layer.dataProvider().uri().uri() :
            if layer.dataProvider().uri().uri().split('|')[0][-4:] == '.gml' :
                if layer.isValid() :
                    return layer

def openProject():
    for layerCable in ['RPD_CableElectrique_Reco', 'RPD_CableTerre_Reco'] :
        #print(layerCable)
        if os.path.isfile(path+f"/{layerCable}.geojson"):
            os.remove(path+f"/{layerCable}.geojson")
        if lyrCable := getLayerFromTable(layerCable):
            #print(lyrCable)
            unionsql=[]
            if lyrCC := getLayerFromTable("Cheminement_Cables"):
                for layerChem in ["RPD_Aerien_Reco", "RPD_PleineTerre_Reco", "RPD_Fourreau_Reco", "RPD_Galerie_Reco", "RPD_ProtectionMecanique_Reco"]:
                    if lyrChem := getLayerFromTable(layerChem) :
                        if 'Aerien' in layerChem :
                            unionsql.append(f"select gml_id, 'Aérien' TypePose, geometry from {lyrChem.name()}")
                        else :
                            unionsql.append(f"select gml_id, 'Enterre' TypePose, geometry from {lyrChem.name()}")

                sql_query="with cheminements as ("\
                +" UNION ALL ".join(unionsql)\
                +f""")
                select p.gml_id uid, c.*, p.TypePose, p.geometry from {lyrCable.name()} c
                join {lyrCC.name()} j on j.cables_href=c.gml_id
                join cheminements p on p.gml_id=j.cheminement_href;"""
                #print(sql_query)

                virtual_layer = QgsVectorLayer(
                    f"?query={sql_query}",  # Requête SQL
                    f"Temp {layerCable}",       # Nom de la couche
                    "virtual"                # Type
                )
                #QgsProject.instance().addMapLayer(virtual_layer)

                options = QgsVectorFileWriter.SaveVectorOptions()
                options.driverName = "GeoJSON"  # Définir le format
                options.fileEncoding = "UTF-8"  # Encodage

                error = QgsVectorFileWriter.writeAsVectorFormatV3(
                    virtual_layer,
                    path+f"/{layerCable}.geojson",
                    QgsProject.instance().transformContext(),
                    options
                )

    for layerNoeud in ['RPD_PosteElectrique_Reco'] :
        #print(layerNoeud)
        if os.path.isfile(path+f"/{layerNoeud}.geojson"):
            os.remove(path+f"/{layerNoeud}.geojson")
        if lyrNoeud := getLayerFromTable(layerNoeud):
            #print(lyrNoeud)
            unionsql=[]
            for layerCont in ["RPD_BatimentTechnique_Reco", "RPD_EnceinteCloturee_Reco"]:
                if lyrCont := getLayerFromTable(layerCont) :
                    unionsql.append(f"select gml_id, geometry from {lyrCont.name()}")

            sql_query="with conteneurs as ("\
            +" UNION ALL ".join(unionsql)\
            +f""")
            select n.*, c.geometry from {lyrNoeud.name()} n
            join conteneurs c on c.gml_id=n.conteneur_href;"""
            #print(sql_query)

            virtual_layer = QgsVectorLayer(
                f"?query={sql_query}",  # Requête SQL
                f"Temp {layerNoeud}",       # Nom de la couche
                "virtual"                # Type
            )
            #QgsProject.instance().addMapLayer(virtual_layer)

            options = QgsVectorFileWriter.SaveVectorOptions()
            options.driverName = "GeoJSON"  # Définir le format
            options.fileEncoding = "UTF-8"  # Encodage

            error = QgsVectorFileWriter.writeAsVectorFormatV3(
                virtual_layer,
                path+f"/{layerNoeud}.geojson",
                QgsProject.instance().transformContext(),
                options
            )

    QgsProject.instance().read()

def saveProject():
    pass

def closeProject():
    pass


openProject()
