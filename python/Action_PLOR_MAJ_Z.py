layer=form.layer()
feature = form.currentFormFeature()
if isinstance(feature.attribute('Leve'),float) :
    new_z = feature.attribute('Leve')
elif isinstance(feature.attribute('Leve'),str) :
    new_z = float(feature.attribute('Leve').replace(',','.'))
geom = feature.geometry()
if feature.attribute('TypeLeve') == 'AltitudeGeneratrice'\
and new_z!= geom.get().z() :
    updated_geom = QgsGeometry(QgsPoint(geom.asPoint().x(), geom.asPoint().y(), new_z))
    if feature.id() > 0:
        form.save()
        layer.changeGeometry(feature.id(), updated_geom)
        layer.commitChanges(False)
        ##RELOAD FEATURE
        form.setFeature(layer.getFeature([%$id%]))
        print("MAJ Z", new_z)
    else:
        form.changeGeometry(updated_geom)
        form.changeAttribute("Z", new_z)
        form.refreshFeature()
        print("MAJ Z", new_z)
