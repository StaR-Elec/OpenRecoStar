# OpenRecoStar

## 📚 [Accéder à la documentation](../../wikis/home) 📚

## 🛟  besoin d'aide ? une question ?

* 📖  [Echangez avec la communauté](https://gitlab.com/StaR-Elec/OpenRecoStar/-/issues)

* 📩  [Ou écrivez-nous](mailto:contact-project+star-elec-openrecostar-support@incoming.gitlab.com)
