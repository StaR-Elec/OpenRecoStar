select load_extension("/usr/local/lib/mod_spatialite.dylib");
SELECT EnableGpkgMode(); --GPKG
SELECT gpkgCreateBaseTables(); --GPKG
SELECT gpkgInsertEpsgSRID(2154); --GPKG

select DropTable(NULL, 'generate_series', True);
CREATE TABLE generate_series(
  value
);

WITH RECURSIVE
  cnt(x) AS (VALUES(1) UNION ALL SELECT x+1 FROM cnt WHERE x<1000)
INSERT INTO generate_series SELECT x FROM cnt;

SELECT value FROM generate_series;

--XXX ReseauUtilite

select DropTable(NULL, 'ReseauUtilite', True);
CREATE TABLE ReseauUtilite (
    fid INTEGER PRIMARY KEY AUTOINCREMENT
  , ogr_pkid TEXT DEFAULT ('ReseauUtilite_0')
  , id TEXT NOT NULL UNIQUE
  , Mention TEXT
  , Nom TEXT
  , Responsable TEXT
  , Theme TEXT
);

INSERT INTO ReseauUtilite (id, Mention, Nom, Responsable, Theme)
  VALUES ('Reseau','Test export GML OpenRecoStar','Réseau public de distribution','Enedis','ELECTRD')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ReseauUtilite','attributes','ReseauUtilite'); --GPKG

select DropTable(NULL, 'Reseau', True);
CREATE VIEW Reseau as
select cast(ROW_NUMBER () OVER () as int) fid
  , cast('Reseau_'||ROW_NUMBER () OVER () as text) ogr_pkid
  , cast(id as text) href, cast(null as text) reseauutilite_pkid from ReseauUtilite;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('Reseau','attributes','Reseau'); --GPKG

--XXX Metadata

DROP TABLE IF EXISTS SRSValue ;
CREATE TABLE SRSValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO SRSValue VALUES
  ('EPSG:3942', 'CC42 - EPSG:3942')
, ('EPSG:3943', 'CC43 - EPSG:3943')
, ('EPSG:3944', 'CC44 - EPSG:3944')
, ('EPSG:3945', 'CC45 - EPSG:3945')
, ('EPSG:3946', 'CC46 - EPSG:3946')
, ('EPSG:3947', 'CC47 - EPSG:3947')
, ('EPSG:3948', 'CC48 - EPSG:3948')
, ('EPSG:3949', 'CC49 - EPSG:3949')
, ('EPSG:3950', 'CC50 - EPSG:3950')
, ('EPSG:2154', 'RGF93LAMB93 - EPSG:2154')
, ('EPSG:5490', 'RGAF09UTM20 - EPSG:5490')
, ('EPSG:2972', 'RGFG95UTM22 - EPSG:2972')
, ('EPSG:2975', 'RGR92UTM40S - EPSG:2975')
, ('EPSG:4471', 'RGM04UTM38S - EPSG:4471')
, ('EPSG:4467', 'RGSPM06U21 - EPSG:4467')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('SRSValue','attributes','SRSValue'); --GPKG

select DropTable(NULL, 'Metadata', True);
CREATE TABLE Metadata (
    fid INTEGER PRIMARY KEY AUTOINCREMENT
  , ogr_pkid TEXT DEFAULT ('Metadata_0')
  , id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
  , Datecreation DATE NOT NULL
  , Logiciel TEXT NOT NULL
  , Producteur TEXT NOT NULL
  , Responsable TEXT NOT NULL
  , SRS TEXT NOT NULL REFERENCES SRSValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('Metadata','attributes','Metadata'); --GPKG


--XXX > Cheminements

select DropTable(NULL, 'ProtectionMaterialTypeValueReco', True);
CREATE TABLE ProtectionMaterialTypeValueReco (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ProtectionMaterialTypeValueReco VALUES
  ('CastIron','Fonte')
, ('Concrete','Béton')
, ('Masonry','Maçonnerie')
, ('Other','Autre')
, ('PE','Polyéthylène (PE)')
, ('PEX','Polyéthylène réticulé à haute densité (PEX)')
, ('PVC','PVC')
, ('Steel','Acier')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ProtectionMaterialTypeValueReco','attributes','ProtectionMaterialTypeValueReco'); --GPKG

select DropTable(NULL, 'ClassePrecisionReseauValue', True);
CREATE TABLE ClassePrecisionReseauValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ClassePrecisionReseauValue VALUES
  ('A','Classe A')
, ('B','Classe B')
, ('C','Classe C')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ClassePrecisionReseauValue','attributes','ClassePrecisionReseauValue'); --GPKG

select DropTable(NULL, 'EtatCoupeTypeValueReco', True);
CREATE TABLE EtatCoupeTypeValueReco (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO EtatCoupeTypeValueReco VALUES
  ('Provisoire','Provisoire')
, ('Definitive','Définitive')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EtatCoupeTypeValueReco','attributes','EtatCoupeTypeValueReco'); --GPKG

--XXX RPD_Fourreau_Reco

select DropTable(NULL, 'RPD_Fourreau_Reco', True);
CREATE TABLE RPD_Fourreau_Reco(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('RPD_Fourreau_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, Materiau TEXT NOT NULL REFERENCES ProtectionMaterialTypeValueReco (valeurs)
, DiametreDuFourreau NUMERIC NOT NULL
, DiametreDuFourreau_uom TEXT DEFAULT 'mm'
, CoupeType TEXT
, EtatCoupeType TEXT REFERENCES EtatCoupeTypeValueReco (valeurs)
, Geometrie LINESTRINGZ NOT NULL UNIQUE
, ProfondeurMinNonReg DOUBLE
, ProfondeurMinNonReg_uom TEXT DEFAULT 'm' -- REFERENCES ProfondeurMinNonReg_uomValue (valeurs)
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('RPD_Fourreau_Reco','features','RPD_Fourreau_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('RPD_Fourreau_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('RPD_Fourreau_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('RPD_Fourreau_Reco', 'Geometrie');

select DropTable(NULL, 'RPD_Fourreau_Reco_reseau_reseau', True);
CREATE VIEW RPD_Fourreau_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from RPD_Fourreau_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('RPD_Fourreau_Reco_reseau_reseau','attributes','RPD_Fourreau_Reco_reseau_reseau'); --GPKG

--XXX RPD_Galerie_Reco

select DropTable(NULL, 'RPD_Galerie_Reco', True);
CREATE TABLE RPD_Galerie_Reco(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('RPD_Galerie_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, Hauteur NUMERIC NOT NULL
, Hauteur_uom TEXT DEFAULT 'm'
, Largeur NUMERIC NOT NULL
, Largeur_uom TEXT DEFAULT 'm'
, Geometrie LINESTRINGZ NOT NULL UNIQUE
, ProfondeurMinNonReg DOUBLE
, ProfondeurMinNonReg_uom TEXT DEFAULT 'm' -- REFERENCES ProfondeurMinNonReg_uomValue (valeurs)
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('RPD_Galerie_Reco','features','RPD_Galerie_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('RPD_Galerie_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('RPD_Galerie_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('RPD_Galerie_Reco', 'Geometrie');

select DropTable(NULL, 'RPD_Galerie_Reco_reseau_reseau', True);
CREATE VIEW RPD_Galerie_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from RPD_Galerie_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('RPD_Galerie_Reco_reseau_reseau','attributes','RPD_Galerie_Reco_reseau_reseau'); --GPKG

--XXX RPD_PleineTerre_Reco

select DropTable(NULL, 'RPD_PleineTerre_Reco_line', True);
CREATE TABLE RPD_PleineTerre_Reco_line(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('RPD_PleineTerre_Reco_line_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, CoupeType TEXT
, EtatCoupeType TEXT REFERENCES EtatCoupeTypeValueReco (valeurs)
, Geometrie LINESTRINGZ NOT NULL UNIQUE
, ProfondeurMinNonReg DOUBLE
, ProfondeurMinNonReg_uom TEXT DEFAULT 'm' -- REFERENCES ProfondeurMinNonReg_uomValue (valeurs)
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('RPD_PleineTerre_Reco_line','features','RPD_PleineTerre_Reco_line',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('RPD_PleineTerre_Reco_line', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('RPD_PleineTerre_Reco_line', 'Geometrie' );
select gpkgAddGeometryTriggers('RPD_PleineTerre_Reco_line', 'Geometrie');

--XXX RPD_ProtectionMecanique_Reco

select DropTable(NULL, 'RPD_ProtectionMecanique_Reco', True);
CREATE TABLE RPD_ProtectionMecanique_Reco(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('RPD_ProtectionMecanique_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, Materiau TEXT NOT NULL REFERENCES ProtectionMaterialTypeValueReco (valeurs)
, CoupeType TEXT
, EtatCoupeType TEXT REFERENCES EtatCoupeTypeValueReco (valeurs)
, Geometrie LINESTRINGZ NOT NULL UNIQUE
, ProfondeurMinNonReg DOUBLE
, ProfondeurMinNonReg_uom TEXT DEFAULT 'm' -- REFERENCES ProfondeurMinNonReg_uomValue (valeurs)
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('RPD_ProtectionMecanique_Reco','features','RPD_ProtectionMecanique_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('RPD_ProtectionMecanique_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('RPD_ProtectionMecanique_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('RPD_ProtectionMecanique_Reco', 'Geometrie');

select DropTable(NULL, 'RPD_ProtectionMecanique_Reco_reseau_reseau', True);
CREATE VIEW RPD_ProtectionMecanique_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from RPD_ProtectionMecanique_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('RPD_ProtectionMecanique_Reco_reseau_reseau','attributes','RPD_ProtectionMecanique_Reco_reseau_reseau'); --GPKG

--XXX RPD_Aerien_Reco

select DropTable(NULL, 'ModePoseValue', True);
CREATE TABLE ModePoseValue  (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ModePoseValue VALUES
  ('EnFacade','En façade')
, ('Supporte','Supporté')
, ('SurLeSol','Sur le sol')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ModePoseValue','attributes','ModePoseValue'); --GPKG

select DropTable(NULL, 'RPD_Aerien_Reco_line', True);
CREATE TABLE RPD_Aerien_Reco_line(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('RPD_Aerien_Reco_line_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, ModePose TEXT NOT NULL REFERENCES ModePoseValue (valeurs)
, Geometrie LINESTRINGZ NOT NULL UNIQUE
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('RPD_Aerien_Reco_line','features','RPD_Aerien_Reco_line',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('RPD_Aerien_Reco_line', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('RPD_Aerien_Reco_line', 'Geometrie' );
select gpkgAddGeometryTriggers('RPD_Aerien_Reco_line', 'Geometrie');

-- XXX Vue Cheminement
-- vue technique hors recostar

select DropTable(NULL, 'Cheminement', True);
CREATE VIEW Cheminement as
with all_conso as (
  SELECT ogr_pkid, id, cast('Fourreau' as text) type_cheminement, ProfondeurMinNonReg, PrecisionXY, PrecisionZ, Geometrie FROM RPD_Fourreau_Reco
  UNION ALL
  SELECT ogr_pkid, id, cast('Galerie' as text) type_cheminement, ProfondeurMinNonReg, PrecisionXY, PrecisionZ, Geometrie FROM RPD_Galerie_Reco
  UNION ALL
  SELECT ogr_pkid, id, cast('PleineTerre' as text) type_cheminement, ProfondeurMinNonReg, PrecisionXY, PrecisionZ, Geometrie FROM RPD_PleineTerre_Reco_line
  UNION ALL
  SELECT ogr_pkid, id, cast('ProtectionMecanique' as text) type_cheminement, ProfondeurMinNonReg, PrecisionXY, PrecisionZ, Geometrie FROM RPD_ProtectionMecanique_Reco
  UNION ALL
  SELECT ogr_pkid, id, cast('Aerien' as text) type_cheminement, cast(Null as double) ProfondeurMinNonReg, PrecisionXY, PrecisionZ, Geometrie FROM RPD_Aerien_Reco_line
) select cast(ROW_NUMBER () OVER () as int) fid, * from all_conso;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('Cheminement','features','Cheminement',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('Cheminement', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

--XXX > Cables

-- XXX RPD_CableElectrique_Reco

select DropTable(NULL, 'DomaineTensionValue', True);
CREATE TABLE DomaineTensionValue  (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO DomaineTensionValue VALUES
  ('BT','Basse Tension')
, ('HTA','Haute Tension A')
, ('HTB','Haute Tension B')
, ('Inconnu','Tension inconnue (ouvrages désaffectés par exemple)')
, ('TBT','Très Basse Tension')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('DomaineTensionValue','attributes','DomaineTensionValue'); --GPKG


select DropTable(NULL, 'FonctionCableElectriqueValue', True);
CREATE TABLE	FonctionCableElectriqueValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO FonctionCableElectriqueValue VALUES
  ('Autre','Autre')
, ('Communication','Communication')
, ('DistributionEnergie','Distribution d''énergie')
, ('MiseTerre','Mise à la terre')
, ('Equipotentialité','Equipotentialité')
, ('MaltEquipot','Mise à la terre & équipotentialité')
, ('ProtectionCathodique','Protection cathodique')
, ('TransportEnergie','Transport de l''énergie')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('FonctionCableElectriqueValue','attributes','FonctionCableElectriqueValue'); --GPKG

select DropTable(NULL, 'IsolantValueReco', True);
CREATE TABLE	IsolantValueReco (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO IsolantValueReco VALUES
  ('Thermodurcissable', 'Isolation thermodurcissable')
, ('Reticulee', 'Isolation réticulée')
, ('Nu', 'Câble sans isolant')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('IsolantValueReco','attributes','IsolantValueReco'); --GPKG

select DropTable(NULL, 'CableMaterialTypeValue', True);
CREATE TABLE	CableMaterialTypeValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO CableMaterialTypeValue VALUES
  ('Alu', 'Aluminium')
, ('Cuivre', 'Cuivre')
, ('Alm','Almélec (câble uniforme d''alliage d''aluminium AAAC)') --QUESTION : pas dans la xsd?
, ('AluAcier','Alu-Acier (câble bimétallique ACSR)')
, ('AlmAcier','Alm-Acier (câble bimétallique AACSR)')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('CableMaterialTypeValue','attributes','CableMaterialTypeValue'); --GPKG

select DropTable(NULL, 'HierarchieBTValue', True);
CREATE TABLE	HierarchieBTValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO HierarchieBTValue VALUES
  ('Reseau', 'Réseau')
, ('LiaisonReseau', 'Liaison Réseau du branchement')
, ('DerivationIndividuelle', 'Dérivation Individuelle du branchement')
, ('TronconCommun', 'Tronçon Commun du collectif')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('HierarchieBTValue','attributes','HierarchieBTValue'); --GPKG

select DropTable(NULL, 'ConditionOfFacilityValueReco', True);
CREATE TABLE	ConditionOfFacilityValueReco (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ConditionOfFacilityValueReco VALUES
  ('Decommissioned', 'Hors service')
, ('Dismantled', 'Déposé')
, ('Functional', 'En service')
, ('UnderCommissionning', 'En attente de mise en service')
, ('Projected', 'Projet')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ConditionOfFacilityValueReco','attributes','ConditionOfFacilityValueReco'); --GPKG

select DropTable(NULL, 'TypePoseValue', True); --NOTE : hors reco star
CREATE TABLE TypePoseValue  (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO TypePoseValue VALUES
  ('EnFacade','En façade')
, ('Supporte','Supporté')
, ('SurLeSol','Sur le sol')
, ('Enterre','Enterré')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('TypePoseValue','attributes','TypePoseValue'); --GPKG

select DropTable(NULL, 'CablesNormalises', True); --NOTE : hors reco star
CREATE TABLE CablesNormalises  (
  id int NOT NULL UNIQUE PRIMARY KEY,
  TypePose text,
  DomaineTension text,
  HierarchieBT text,
  NombreConducteurs text,
  Section text,
  SectionNeutre text,
  Isolant text,
  Materiau text,
  Designation text
);

INSERT INTO CablesNormalises VALUES
  (1,'Aerien','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','4','35','35','Reticulee','Alu','T 4 35 AL')
, (2,'Aerien','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','4','25','25','Reticulee','Alu','T 25 AL')
, (3,'Aerien','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','4','70','54','Reticulee','Alu','T 70 AL')
, (4,'Aerien','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','4','50','54','Reticulee','Alu','T 50 AL')
, (5,'Aerien','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','4','35','54','Reticulee','Alu','T 35 AL')
, (6,'Enterre','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','4','35','35','Reticulee','Alu','4 x 35 AL')
, (7,'Enterre','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','4','95','70','Reticulee','Alu','3 x 95AL + 70AL 33S210')
, (8,'Enterre','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','4','150','95','Reticulee','Alu','3 x 150AL + 95AL 33S210')
, (9,'Enterre','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','4','240','120','Reticulee','Alu','3 x 240AL + 120AL 33S210')
, (10,'Enterre','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','4','95','50','Reticulee','Alu','3 x 95 AL + 50 AL &Pb')
, (11,'Enterre','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','4','150','70','Reticulee','Alu','3 x 150 AL + 70 AL &Pb')
, (12,'Enterre','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','4','240','95','Reticulee','Alu','3 x 240 AL + 95 AL &Pb')
, (13,'Enterre','BT','[LiaisonReseau,DerivationIndividuelle,TronconCommun]','2','35','35','Reticulee','Alu','2 x 35 AL')
, (14,'Aerien','BT','Reseau','4','70','54','Reticulee','Alu','T 70 AL')
, (15,'Aerien','BT','Reseau','4','150','70','Reticulee','Alu','T 150 AL')
, (16,'Aerien','HTA','Reseau','3','34',NULL,'Nu','Alm','34 AM')
, (17,'Aerien','HTA','Reseau','3','54',NULL,'Nu','Alm','54 AM')
, (18,'Aerien','HTA','Reseau','3','75',NULL,'Nu','Alm','75 AM')
, (19,'Aerien','HTA','Reseau','3','117',NULL,'Nu','Alm','117 AM')
, (20,'Aerien','HTA','Reseau','3','148',NULL,'Nu','Alm','148 AM')
, (21,'Aerien','HTA','Reseau','3','50',NULL,'Reticulee','Alu','T 50 AL')
, (22,'Aerien','HTA','Reseau','3','95',NULL,'Reticulee','Alu','T 95 AL')
, (23,'Aerien','HTA','Reseau','3','150',NULL,'Reticulee','Alu','T 150 AL')
, (24,'Aerien','HTA','Reseau','3','240',NULL,'Reticulee','Alu','T 240 AL')
, (25,'Aerien','HTA','Reseau','3','34',NULL,'Nu','AluAcier','34 AA')
, (26,'Aerien','HTA','Reseau','3','37',NULL,'Nu','AluAcier','37 AA')
, (27,'Aerien','HTA','Reseau','3','54',NULL,'Nu','AluAcier','54 AA')
, (28,'Aerien','HTA','Reseau','3','59',NULL,'Nu','AluAcier','59 AA')
, (29,'Aerien','HTA','Reseau','3','116',NULL,'Nu','AluAcier','116 AA')
, (30,'Aerien','HTA','Reseau','3','147',NULL,'Nu','AluAcier','147 AA')
, (31,'Aerien','HTA','Reseau','3','59',NULL,'Nu','AlmAcier','59 LA')
, (32,'Enterre','BT','Reseau','4','95','70','Reticulee','Alu','3 x 95AL + 70AL 33S210')
, (33,'Enterre','BT','Reseau','4','150','95','Reticulee','Alu','3 x 150AL + 95AL 33S210')
, (34,'Enterre','BT','Reseau','4','240','120','Reticulee','Alu','3 x 240AL + 120AL 33S210')
, (35,'Enterre','BT','Reseau','4','95','50','Reticulee','Alu','3 x 95AL + 50AL &Pb')
, (36,'Enterre','BT','Reseau','4','150','70','Reticulee','Alu','3 x 150AL + 70AL &Pb')
, (37,'Enterre','BT','Reseau','4','240','95','Reticulee','Alu','3 x 240AL + 95AL &Pb')
, (38,'Enterre','HTA','Reseau','3','50',NULL,'Reticulee','Alu','50 AL S6')
, (39,'Enterre','HTA','Reseau','3','95',NULL,'Reticulee','Alu','95 AL S6')
, (40,'Enterre','HTA','Reseau','3','150',NULL,'Reticulee','Alu','150 AL S6')
, (41,'Enterre','HTA','Reseau','3','240',NULL,'Reticulee','Alu','240 AL S6')
, (42,'Enterre','HTA','Reseau','3','400',NULL,'Reticulee','Alu','400 AL S6')
, (43,'Enterre','HTA','Reseau','3','150',NULL,'Reticulee','Alu','150 AL S6 (Galerie)')
, (44,'Enterre','HTA','Reseau','3','240',NULL,'Reticulee','Alu','240 AL S6 (Galerie)')
, (45,'Enterre','HTA','Reseau','3','240',NULL,'Reticulee','Cuivre','240 CU S6')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('CablesNormalises','attributes','CablesNormalises'); --GPKG

select DropTable(NULL, 'RPD_CableElectrique_Reco', True);
CREATE TABLE RPD_CableElectrique_Reco(
    fid INTEGER PRIMARY KEY AUTOINCREMENT
  , ogr_pkid TEXT DEFAULT ('RPD_CableElectrique_Reco_0')
  , id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
  , DomaineTension TEXT NOT NULL REFERENCES DomaineTensionValue (valeurs)
  , FonctionCable_href TEXT NOT NULL REFERENCES FonctionCableElectriqueValue (valeurs)
  , CableNormalise INTEGER REFERENCES CablesNormalises (id)
  , NombreConducteurs INTEGER NOT NULL
  , Section NUMERIC NOT NULL
  , Section_uom TEXT DEFAULT 'mm-2'
  , SectionNeutre NUMERIC
  , SectionNeutre_uom TEXT DEFAULT 'mm-2'
  , Isolant TEXT NOT NULL REFERENCES IsolantValueReco (valeurs)
  , Materiau TEXT NOT NULL REFERENCES CableMaterialTypeValue (valeurs)
  , HierarchieBT TEXT REFERENCES HierarchieBTValue (valeurs)  -- NOTE : NOT NULL si DomaineTension = BT
  , Commentaire TEXT
  , Statut TEXT NOT NULL REFERENCES ConditionOfFacilityValueReco (valeurs)
  -- Cheminement
  , TypePose TEXT NOT NULL REFERENCES TypePoseValue (valeurs) --NOTE : hors reco star => TypePose pour Cheminement Aérien ou PleineTerre (Enterre)
  , Geometrie LINESTRINGZ NOT NULL UNIQUE
  , PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
  , PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
  CHECK ((DomaineTension='BT' AND HierarchieBT IS NOT NULL) OR DomaineTension <> 'BT')
);
-- QGIS "HierarchieBT" constraint : ("DomaineTension" = 'BT' and  "HierarchieBT" is not null) or "DomaineTension" <> 'BT'

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('RPD_CableElectrique_Reco','features','RPD_CableElectrique_Reco'); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('RPD_CableElectrique_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('RPD_CableElectrique_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('RPD_CableElectrique_Reco', 'Geometrie');

select DropTable(NULL, 'RPD_CableElectrique_Reco_reseau_reseau', True);
CREATE VIEW RPD_CableElectrique_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from RPD_CableElectrique_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('RPD_CableElectrique_Reco_reseau_reseau','attributes','RPD_CableElectrique_Reco_reseau_reseau'); --GPKG

--XXX RPD_CableTerre_Reco

select DropTable(NULL, 'ConducteurProtectionValue', True);
CREATE TABLE	ConducteurProtectionValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ConducteurProtectionValue VALUES
  ('CuivreNu','Cuivre nu')
, ('CuivreIsol','Cuivre isolé')
, ('Sans','Sans')
, ('VertJaune','Vert-jaune')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ConducteurProtectionValue','attributes','ConducteurProtectionValue'); --GPKG

select DropTable(NULL, 'RPD_CableTerre_Reco', True);
CREATE TABLE RPD_CableTerre_Reco(
    fid INTEGER PRIMARY KEY AUTOINCREMENT
  , ogr_pkid TEXT DEFAULT ('RPD_CableTerre_Reco_0')
  , id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
  , FonctionCable_href TEXT NOT NULL REFERENCES FonctionCableElectriqueValue (valeurs)
  , NatureCableTerre_href TEXT NOT NULL REFERENCES ConducteurProtectionValue (valeurs)
  , Section NUMERIC NOT NULL
  , Section_uom TEXT DEFAULT 'mm-2'
  , Materiau TEXT NOT NULL REFERENCES CableMaterialTypeValue (valeurs)
  , Commentaire TEXT
  , Statut TEXT NOT NULL REFERENCES ConditionOfFacilityValueReco (valeurs)
  -- Cheminement
  , TypePose TEXT NOT NULL REFERENCES TypePoseValue (valeurs) --NOTE : hors reco star => TypePose pour aérien ou PleineTerre (Enterre)
  , Geometrie LINESTRINGZ NOT NULL UNIQUE
  , PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
  , PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
  , noeudreseau_href TEXT --QUESTION : UUID noeud arrivée terre?
);

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('RPD_CableTerre_Reco','features','RPD_CableTerre_Reco'); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('RPD_CableTerre_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('RPD_CableTerre_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('RPD_CableTerre_Reco', 'Geometrie');

select DropTable(NULL, 'RPD_CableTerre_Reco_reseau_reseau', True);
CREATE VIEW RPD_CableTerre_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from RPD_CableTerre_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('RPD_CableTerre_Reco_reseau_reseau','attributes','RPD_CableTerre_Reco_reseau_reseau'); --GPKG

-- XXX Vue Cable
-- vue technique hors recostar

select DropTable(NULL, 'Cable', True);
CREATE VIEW Cable as
with all_conso as (
  SELECT ogr_pkid, id, cast('CableElectrique' as text) type_cable, TypePose, FonctionCable_href FonctionCable, Section, Commentaire, Statut, PrecisionXY, PrecisionZ, Geometrie FROM RPD_CableElectrique_Reco
  UNION ALL
  SELECT ogr_pkid, id, cast('CableTerre' as text) type_cable, TypePose, FonctionCable_href FonctionCable, Section, Commentaire, Statut, PrecisionXY, PrecisionZ, Geometrie FROM RPD_CableTerre_Reco
) select cast(ROW_NUMBER () OVER () as int) fid, * from all_conso;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('Cable','features','Cable',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('Cable', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

--XXX Cheminements par defaut

select DropTable(NULL, 'RPD_PleineTerre_Reco_virt', True);
CREATE VIEW RPD_PleineTerre_Reco_virt as
with difference as (
SELECT c.id, coalesce(ST_Difference(c."Geometrie", ST_Union(h."Geometrie")), c."Geometrie") Geometrie, c.PrecisionXY,c.PrecisionZ
FROM Cable c
LEFT JOIN Cheminement h ON ST_Within(h."Geometrie", c."Geometrie")
WHERE TypePose = 'Enterre'
group by c.id, c.PrecisionXY, c.PrecisionZ, c."Geometrie"
)
select cast(ROW_NUMBER () OVER () as int) fid, cast('RPD_PleineTerre_Reco_virt_'||ROW_NUMBER () OVER () as text) ogr_pkid, cast(('id'||CreateUUID()) as text) id
, cast(Null as text) CoupeType, cast(Null as text) EtatCoupeType, ST_GeometryN(c.Geometrie, s.value) Geometrie
, cast(Null as double) ProfondeurMinNonReg, cast('mm-2' as text) ProfondeurMinNonReg_uom, c.PrecisionXY, c.PrecisionZ
from difference c
, generate_series s ON s.value <= ST_NumGeometries(c.Geometrie)
;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('RPD_PleineTerre_Reco_virt','features','RPD_PleineTerre_Reco_virt',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('RPD_PleineTerre_Reco_virt', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

select DropTable(NULL, 'RPD_PleineTerre_Reco', True);
CREATE TABLE RPD_PleineTerre_Reco as --NOTE: VM à rafraichir avant chaque export
with uniiion as (
select * from RPD_PleineTerre_Reco_line
union all
select * from RPD_PleineTerre_Reco_virt
)
select cast(ROW_NUMBER () OVER () as int) fid, ogr_pkid, id, CoupeType, EtatCoupeType, Geometrie, ProfondeurMinNonReg, ProfondeurMinNonReg_uom, PrecisionXY, PrecisionZ
from uniiion
;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('RPD_PleineTerre_Reco','features','RPD_PleineTerre_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('RPD_PleineTerre_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

select DropTable(NULL, 'RPD_PleineTerre_Reco_reseau_reseau', True);
CREATE VIEW RPD_PleineTerre_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from RPD_PleineTerre_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('RPD_PleineTerre_Reco_reseau_reseau','attributes','RPD_PleineTerre_Reco_reseau_reseau'); --GPKG

select DropTable(NULL, 'RPD_Aerien_Reco_virt', True);
CREATE VIEW RPD_Aerien_Reco_virt as
with difference as (
SELECT c.id, coalesce(ST_Difference(c."Geometrie", ST_Union(h."Geometrie")), c."Geometrie") Geometrie, c.TypePose, c.PrecisionXY,c.PrecisionZ
FROM Cable c
LEFT JOIN Cheminement h ON ST_Within(h."Geometrie", c."Geometrie")
WHERE NOT TypePose = 'Enterre'
group by c.id, c.TypePose, c.PrecisionXY, c.PrecisionZ, c."Geometrie"
)
select cast(ROW_NUMBER () OVER () as int) fid, cast('RPD_Aerien_Reco_virt'||ROW_NUMBER () OVER () as text) ogr_pkid, cast(('id'||CreateUUID()) as text) id
, c.TypePose ModePose, ST_GeometryN(c.Geometrie, s.value) Geometrie
, c.PrecisionXY, c.PrecisionZ
from difference c
, generate_series s ON s.value <= ST_NumGeometries(c.Geometrie)
;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('RPD_Aerien_Reco_virt','features','RPD_Aerien_Reco_virt',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('RPD_Aerien_Reco_virt', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

select DropTable(NULL, 'RPD_Aerien_Reco', True);
CREATE TABLE RPD_Aerien_Reco as --NOTE: VM à rafraichir avant chaque export
with uniiion as (
select * from RPD_Aerien_Reco_line
union all
select * from RPD_Aerien_Reco_virt
)
select cast(ROW_NUMBER () OVER () as int) fid, ogr_pkid, id, ModePose, Geometrie, PrecisionXY, PrecisionZ
from uniiion
;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('RPD_Aerien_Reco','features','RPD_Aerien_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('RPD_Aerien_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

select DropTable(NULL, 'RPD_Aerien_Reco_reseau_reseau', True);
CREATE VIEW RPD_Aerien_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from RPD_Aerien_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('RPD_Aerien_Reco_reseau_reseau','attributes','RPD_Aerien_Reco_reseau_reseau'); --GPKG

--XXX Relation Cheminement_Cables
select DropTable(NULL, 'Cheminement_Cables', True);
CREATE VIEW Cheminement_Cables AS
with uniiion as (
  select cast(c.id as text) cables_href
  , cast(h.id as text) cheminement_href
  FROM Cable c
  JOIN RPD_Fourreau_Reco h ON ST_Within(h."Geometrie", c."Geometrie")
  union all
  select cast(c.id as text) cables_href
  , cast(h.id as text) cheminement_href
  FROM Cable c
  JOIN RPD_ProtectionMecanique_Reco h ON ST_Within(h."Geometrie", c."Geometrie")
  union all
  select cast(c.id as text) cables_href
  , cast(h.id as text) cheminement_href
  FROM Cable c
  JOIN RPD_Galerie_Reco h ON ST_Within(h."Geometrie", c."Geometrie")
  union all
  select cast(c.id as text) cables_href
  , cast(h.id as text) cheminement_href
  FROM Cable c
  JOIN RPD_PleineTerre_Reco h ON ST_Within(h."Geometrie", c."Geometrie")
  union all
  select cast(c.id as text) cables_href
  , cast(h.id as text) cheminement_href
  FROM Cable c
  JOIN RPD_Aerien_Reco h ON ST_Within(h."Geometrie", c."Geometrie")
)
select cast(ROW_NUMBER () OVER () as int) fid
, cast('Cheminement_Cables_'||ROW_NUMBER () OVER () as text) ogr_pkid
, ('id'||CreateUUID()) id
, *
, cast(null as text) cables_cables_rpd_cableelectrique_reco_pkid
, cast(null as text) cables_cables_rpd_cableterre_reco_pkid
, cast(null as text) cheminement_cheminement_rpd_aerien_reco_pkid
, cast(null as text) cheminement_cheminement_rpd_fourreau_reco_pkid
, cast(null as text) cheminement_cheminement_rpd_galerie_reco_pkid
, cast(null as text) cheminement_cheminement_rpd_pleineterre_reco_pkid
, cast(null as text) cheminement_cheminement_rpd_protectionmecanique_reco_pkid
from uniiion
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('Cheminement_Cables','attributes','Cheminement_Cables'); --GPKG
-- TODO : plusieurs cables peuvent emprunter le même cheminement
