select load_extension("/usr/local/lib/mod_spatialite.dylib");
SELECT EnableGpkgMode(); --GPKG
SELECT gpkgCreateBaseTables(); --GPKG
SELECT gpkgInsertEpsgSRID(2154); --GPKG

select DropTable(NULL, 'generate_series', TRUE);
CREATE TABLE generate_series(
  value
);

WITH RECURSIVE
  cnt(x) AS (VALUES(1) UNION ALL SELECT x+1 FROM cnt WHERE x<1000)
INSERT INTO generate_series SELECT x FROM cnt;

SELECT value FROM generate_series;

--XXX ReseauUtilite

select DropTable(NULL, 'ReseauUtilite', TRUE);
CREATE TABLE ReseauUtilite (
    fid INTEGER PRIMARY KEY AUTOINCREMENT
  , ogr_pkid TEXT DEFAULT ('ReseauUtilite_0')
  , id TEXT NOT NULL UNIQUE
  , Mention TEXT
  , Nom TEXT
  , Responsable TEXT
  , Theme TEXT
);

INSERT INTO ReseauUtilite (id, Mention, Nom, Responsable, Theme)
  VALUES ('Reseau','Test export GML OpenRecoStar','Réseau Eclairage Public','xxx','XXX')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ReseauUtilite','attributes','ReseauUtilite'); --GPKG

select DropTable(NULL, 'Reseau', TRUE);
CREATE VIEW Reseau as
select cast(ROW_NUMBER () OVER () as int) fid
  , cast('Reseau_'||ROW_NUMBER () OVER () as text) ogr_pkid
  , cast(id as text) href, cast(null as text) reseauutilite_pkid from ReseauUtilite;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('Reseau','attributes','Reseau'); --GPKG

--XXX Metadata

DROP TABLE IF EXISTS SRSValue ;
CREATE TABLE SRSValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO SRSValue VALUES
  ('EPSG:3942', 'CC42')
, ('EPSG:3943', 'CC43')
, ('EPSG:3944', 'CC44')
, ('EPSG:3945', 'CC45')
, ('EPSG:3946', 'CC46')
, ('EPSG:3947', 'CC47')
, ('EPSG:3948', 'CC48')
, ('EPSG:3949', 'CC49')
, ('EPSG:3950', 'CC50')
, ('EPSG:2154', 'RGF93LAMB93')
, ('EPSG:5490', 'RGAF09UTM20')
, ('EPSG:2972', 'RGFG95UTM22')
, ('EPSG:2975', 'RGR92UTM40S')
, ('EPSG:4471', 'RGM04UTM38S')
, ('EPSG:4467', 'RGSPM06U21')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('SRSValue','attributes','SRSValue'); --GPKG

select DropTable(NULL, 'Metadata', TRUE);
CREATE TABLE Metadata (
    fid INTEGER PRIMARY KEY AUTOINCREMENT
  , ogr_pkid TEXT DEFAULT ('Metadata_0')
  , id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
  , Datecreation DATE NOT NULL
  , Logiciel TEXT NOT NULL
  , Producteur TEXT NOT NULL
  , Responsable TEXT NOT NULL
  , SRS TEXT NOT NULL REFERENCES SRSValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('Metadata','attributes','Metadata'); --GPKG

--XXX > Cheminements

select DropTable(NULL, 'PipeMaterialTypeValue', TRUE);
CREATE TABLE PipeMaterialTypeValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO PipeMaterialTypeValue VALUES
  ('ABS','ABS')
, ('Asbestos','Amiante')
, ('BlackIron','Fer noir')
, ('BlackSteel','Acier noir')
, ('CastIron','Fonte')
, ('Clay','Argile')
, ('CompositeConcrete','Béton composite')
, ('Concrete','Béton')
, ('CPVC','PVCC')
, ('FRP','Plastique renforcé de fibres (FRP)')
, ('GalvanizedSteel','Métal galvanisé')
, ('Masonry','Maçonnerie')
, ('Other','Autre')
, ('PB','Polybutylène (PB)')
, ('PE','Polyéthylène (PE)')
, ('PEX','Polyéthylène réticulé à haute densité (PEX)')
, ('PP','Polypropylène (PP)')
, ('PrestressedReinforcedConcrete','Béton armé précontraint')
, ('PVC','PVC')
, ('ReinforcedConcrete','Béton renforcé')
, ('RPMP','Mortier renforcé de polymères (RPMP)')
, ('Steel','Acier')
, ('Terracota','Terracotta')
, ('Wood','Bois')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('PipeMaterialTypeValue','attributes','PipeMaterialTypeValue'); --GPKG

select DropTable(NULL, 'ClassePrecisionReseauValue', TRUE);
CREATE TABLE ClassePrecisionReseauValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ClassePrecisionReseauValue VALUES
  ('A','Classe A')
, ('B','Classe B')
, ('C','Classe C')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ClassePrecisionReseauValue','attributes','ClassePrecisionReseauValue'); --GPKG

--XXX EP_Fourreau_Reco

select DropTable(NULL, 'EP_Fourreau_Reco', TRUE);
CREATE TABLE EP_Fourreau_Reco(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_Fourreau_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, DiametreDuFourreau NUMERIC NOT NULL
, DiametreDuFourreau_uom TEXT DEFAULT 'mm'
, CoupeType TEXT
, NombreOuvrages INTEGER --NOTE: Dans le cas ou plusieurs cables passent dans le même fourreau
, Geometrie LINESTRINGZ NOT NULL UNIQUE
, ProfondeurMinNonReg DOUBLE
, ProfondeurMinNonReg_uom TEXT DEFAULT 'm' -- REFERENCES ProfondeurMinNonReg_uomValue (valeurs)
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_Fourreau_Reco','features','EP_Fourreau_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_Fourreau_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_Fourreau_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_Fourreau_Reco', 'Geometrie');

select DropTable(NULL, 'EP_Fourreau_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_Fourreau_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_Fourreau_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Fourreau_Reco_reseau_reseau','attributes','EP_Fourreau_Reco_reseau_reseau'); --GPKG

--XXX EP_Galerie_Reco

select DropTable(NULL, 'EP_Galerie_Reco', TRUE);
CREATE TABLE EP_Galerie_Reco(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_Galerie_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, Hauteur NUMERIC
, Hauteur_uom TEXT DEFAULT 'm'
, Largeur NUMERIC
, Largeur_uom TEXT DEFAULT 'm'
, NombreOuvrages INTEGER
, Geometrie LINESTRINGZ NOT NULL UNIQUE
, ProfondeurMinNonReg DOUBLE
, ProfondeurMinNonReg_uom TEXT DEFAULT 'm' -- REFERENCES ProfondeurMinNonReg_uomValue (valeurs)
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_Galerie_Reco','features','EP_Galerie_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_Galerie_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_Galerie_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_Galerie_Reco', 'Geometrie');

select DropTable(NULL, 'EP_Galerie_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_Galerie_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_Galerie_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Galerie_Reco_reseau_reseau','attributes','EP_Galerie_Reco_reseau_reseau'); --GPKG

--XXX EP_PleineTerre_Reco

select DropTable(NULL, 'EP_PleineTerre_Reco_line', TRUE);
CREATE TABLE EP_PleineTerre_Reco_line(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_PleineTerre_Reco_line_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, CoupeType TEXT
, Geometrie LINESTRINGZ NOT NULL UNIQUE
, ProfondeurMinNonReg DOUBLE
, ProfondeurMinNonReg_uom TEXT DEFAULT 'm' -- REFERENCES ProfondeurMinNonReg_uomValue (valeurs)
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_PleineTerre_Reco_line','features','EP_PleineTerre_Reco_line',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_PleineTerre_Reco_line', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_PleineTerre_Reco_line', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_PleineTerre_Reco_line', 'Geometrie');

--XXX EP_ProtectionMecanique_Reco

select DropTable(NULL, 'EP_ProtectionMecanique_Reco', TRUE);
CREATE TABLE EP_ProtectionMecanique_Reco(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_ProtectionMecanique_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, Materiau TEXT REFERENCES PipeMaterialTypeValue (valeurs)
, CoupeType TEXT
, Geometrie LINESTRINGZ NOT NULL UNIQUE
, ProfondeurMinNonReg DOUBLE
, ProfondeurMinNonReg_uom TEXT DEFAULT 'm' -- REFERENCES ProfondeurMinNonReg_uomValue (valeurs)
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_ProtectionMecanique_Reco','features','EP_ProtectionMecanique_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_ProtectionMecanique_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_ProtectionMecanique_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_ProtectionMecanique_Reco', 'Geometrie');

select DropTable(NULL, 'EP_ProtectionMecanique_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_ProtectionMecanique_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_ProtectionMecanique_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_ProtectionMecanique_Reco_reseau_reseau','attributes','EP_ProtectionMecanique_Reco_reseau_reseau'); --GPKG

--XXX EP_Aerien_Reco

select DropTable(NULL, 'ModePoseValue', TRUE);
CREATE TABLE ModePoseValue  (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ModePoseValue VALUES
  ('EnFacade','En façade')
, ('Supporte','Supporté')
, ('SurLeSol','Sur le sol')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ModePoseValue','attributes','ModePoseValue'); --GPKG

select DropTable(NULL, 'EP_Aerien_Reco_line', TRUE);
CREATE TABLE EP_Aerien_Reco_line(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_Aerien_Reco_line_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, ModePose TEXT NOT NULL REFERENCES ModePoseValue (valeurs)
, Geometrie LINESTRINGZ NOT NULL UNIQUE
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_Aerien_Reco_line','features','EP_Aerien_Reco_line',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_Aerien_Reco_line', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_Aerien_Reco_line', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_Aerien_Reco_line', 'Geometrie');

-- XXX Vue Cheminement
-- vue technique hors recostar

select DropTable(NULL, 'Cheminement', TRUE);
CREATE VIEW Cheminement as
with all_conso as (
  SELECT ogr_pkid, id, cast('Fourreau' as text) type_cheminement, ProfondeurMinNonReg, PrecisionXY, PrecisionZ, Geometrie FROM EP_Fourreau_Reco
  UNION ALL
  SELECT ogr_pkid, id, cast('Galerie' as text) type_cheminement, ProfondeurMinNonReg, PrecisionXY, PrecisionZ, Geometrie FROM EP_Galerie_Reco
  UNION ALL
  SELECT ogr_pkid, id, cast('PleineTerre' as text) type_cheminement, ProfondeurMinNonReg, PrecisionXY, PrecisionZ, Geometrie FROM EP_PleineTerre_Reco_line
  UNION ALL
  SELECT ogr_pkid, id, cast('ProtectionMecanique' as text) type_cheminement, ProfondeurMinNonReg, PrecisionXY, PrecisionZ, Geometrie FROM EP_ProtectionMecanique_Reco
  UNION ALL
  SELECT ogr_pkid, id, cast('Aerien' as text) type_cheminement, cast(Null as double) ProfondeurMinNonReg, PrecisionXY, PrecisionZ, Geometrie FROM EP_Aerien_Reco_line
) select cast(ROW_NUMBER () OVER () as int) fid, * from all_conso;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('Cheminement','features','Cheminement',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('Cheminement', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG


--XXX > Cables

-- XXX EP_CableTelecommunication_Reco

select DropTable(NULL, 'ConditionOfFacilityValue', TRUE);
CREATE TABLE	ConditionOfFacilityValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ConditionOfFacilityValue VALUES
  ('Decommissioned', 'Hors service') -- QUESTION : différencier Hors Service de Abandonné
, ('Dismantled', 'Déposé')
, ('Functional', 'En service')
, ('UnderCommissionning', 'En attente de mise en service')
, ('Projected', 'Projeté') -- TODO: ajouter symboo dans QLR
, ('ToDecommission', 'A mettre hors service')-- TODO: ajouter symboo dans QLR
, ('ToDismantle', 'A déposer')-- TODO: ajouter symboo dans QLR
, ('UnderConstruction', 'En construction') -- TODO: ajouter symboo dans QLR
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ConditionOfFacilityValue','attributes','ConditionOfFacilityValue'); --GPKG

select DropTable(NULL, 'EP_CableTelecommunication_Reco', TRUE);
CREATE TABLE EP_CableTelecommunication_Reco(
    fid INTEGER PRIMARY KEY AUTOINCREMENT
  , ogr_pkid TEXT DEFAULT ('EP_CableTelecommunication_Reco_0')
  , id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
  , Commentaire TEXT -- QUESTION : NOT NULL dans la XSD ?
  , Statut TEXT NOT NULL REFERENCES ConditionOfFacilityValue (valeurs)
  -- Cheminement
  , TypePose TEXT NOT NULL REFERENCES TypePoseValue (valeurs) --NOTE : hors reco star => TypePose pour Cheminement Aérien ou PleineTerre (Enterre)
  , Geometrie LINESTRINGZ NOT NULL UNIQUE
  , PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
  , PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_CableTelecommunication_Reco','features','EP_CableTelecommunication_Reco'); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_CableTelecommunication_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

select DropTable(NULL, 'EP_CableTelecommunication_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_CableTelecommunication_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_CableTelecommunication_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_CableTelecommunication_Reco_reseau_reseau','attributes','EP_CableTelecommunication_Reco_reseau_reseau'); --GPKG


-- XXX EP_CableElectrique_Reco

select DropTable(NULL, 'DomaineTensionValue', TRUE);
CREATE TABLE DomaineTensionValue  (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO DomaineTensionValue VALUES
  ('BT','Basse Tension')
, ('HTA','Haute Tension A')
, ('HTB','Haute Tension B')
, ('Inconnu','Tension inconnue (ouvrages désaffectés par exemple)')
, ('TBT','Très Basse Tension')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('DomaineTensionValue','attributes','DomaineTensionValue'); --GPKG

select DropTable(NULL, 'ConceptionCableValue', TRUE);
CREATE TABLE	ConceptionCableValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ConceptionCableValue VALUES
  ('C33210', 'NF C 33-210 : Câbles isolés BT')
, ('C33226', 'NF C 33-226 ou 223 : Câbles isolés HTA')
, ('CDLEP', 'NF C 33-221 CDLEP')
, ('CuivreNu', 'NF C 34-110 Cuivre nu')
, ('H07RNF', 'EN 50525-2-31 H07 RNF')
, ('IEC60502', '2	IEC 60502-2')
, ('IEC61089', 'IEC 61089 Conducteurs pour lignes aériennes à brins circulaires, câbles en couches concentriques')
, ('U1000ARVFV', 'U1000 ARVFV')
, ('U1000R2V', 'U1000 R2V')
, ('U1000RVFV', 'NF C 32-320 et -321 U1000 RVFV')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ConceptionCableValue','attributes','ConceptionCableValue'); --GPKG

select DropTable(NULL, 'FonctionCableElectriqueValue', TRUE);
CREATE TABLE	FonctionCableElectriqueValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO FonctionCableElectriqueValue VALUES
  ('Autre','Autre')
, ('Communication','Communication')
, ('DistributionEnergie','Distribution d''énergie')
, ('MiseTerre','Mise à la terre')
, ('Equipotentialité','Equipotentialité')
, ('MaltEquipot','Mise à la terre & équipotentialité')
, ('ProtectionCathodique','Protection cathodique')
, ('TransportEnergie','Transport de l''énergie')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('FonctionCableElectriqueValue','attributes','FonctionCableElectriqueValue'); --GPKG

select DropTable(NULL, 'IsolantValue', TRUE);
CREATE TABLE	IsolantValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO IsolantValue VALUES
  ('Rubanee', 'isolation rubanée')
, ('PapierImpregne', 'isolation au papier imprégné')
, ('PapierPreImpregne', 'isolation (au papier) préimprégné')
, ('PapierImpregneAMasse', 'isolation (au papier) par imprégnation à masse')
, ('Stabilisee', 'isolation à matière stabilisée')
, ('Extrudee', 'isolation extrudée')
, ('Minerale', 'isolation minérale')
, ('Thermoplastique', 'isolation thermoplastique')
, ('Thermodurcissable', 'isolation thermodurcissable')
, ('Reticulee', 'isolation réticulée')
, ('Elastomere', 'isolation élastomère')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('IsolantValue','attributes','IsolantValue'); --GPKG

select DropTable(NULL, 'CableMaterialTypeValue', TRUE);
CREATE TABLE	CableMaterialTypeValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO CableMaterialTypeValue VALUES
  ('Alu', 'Aluminium')
, ('Cuivre', 'Cuivre')
, ('Almelec','Almélec (câble uniforme d''alliage d''aluminium AAAC)') -- QUESTION : Alm dans RPD ? > n'hexiste pas en EP (SLA) ?
, ('AluAcier','Alu-Acier (câble bimétallique ACSR)') -- QUESTION : n'hexiste pas en EP (SLA) ?
-- , ('AlmAcier','Alm-Acier (câble bimétallique AACSR)') --QUESTION : pas dans la xsd EP? > n'hexiste pas en EP (SLA) ?
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('CableMaterialTypeValue','attributes','CableMaterialTypeValue'); --GPKG

select DropTable(NULL, 'TypePoseValue', TRUE); --NOTE : hors reco star
CREATE TABLE TypePoseValue  (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO TypePoseValue VALUES
  ('EnFacade','En façade')
, ('Supporte','Supporté')
, ('SurLeSol','Sur le sol')
, ('Enterre','Enterré')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('TypePoseValue','attributes','TypePoseValue'); --GPKG

select DropTable(NULL, 'EP_CableElectrique_Reco', TRUE);
CREATE TABLE EP_CableElectrique_Reco(
    fid INTEGER PRIMARY KEY AUTOINCREMENT
  , ogr_pkid TEXT DEFAULT ('EP_CableElectrique_Reco_0')
  , id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
  , DomaineTension TEXT REFERENCES DomaineTensionValue (valeurs)
  , ConceptionCable_href TEXT REFERENCES ConceptionCableValue (valeurs)
  , FonctionCable_href TEXT REFERENCES FonctionCableElectriqueValue (valeurs)
  , NombreConducteurs INTEGER NOT NULL
  , Section NUMERIC NOT NULL
  , Section_uom TEXT DEFAULT 'mm-2'
  , SectionNeutre NUMERIC
  , SectionNeutre_uom TEXT DEFAULT 'mm-2'
  , NeutreCommun BOOLEAN
  , TensionNominale NUMERIC
  , TensionNominale_uom TEXT DEFAULT 'V'
  , Isolant TEXT REFERENCES IsolantValue (valeurs)
  , Materiau TEXT REFERENCES CableMaterialTypeValue (valeurs)
  , Commentaire TEXT
  , Statut TEXT NOT NULL REFERENCES ConditionOfFacilityValue (valeurs)
  -- Cheminement
  , TypePose TEXT NOT NULL REFERENCES TypePoseValue (valeurs) --NOTE : hors reco star => TypePose pour Cheminement Aérien ou PleineTerre (Enterre)
  , Geometrie LINESTRINGZ NOT NULL UNIQUE
  , PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
  , PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_CableElectrique_Reco','features','EP_CableElectrique_Reco'); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_CableElectrique_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

select DropTable(NULL, 'EP_CableElectrique_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_CableElectrique_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_CableElectrique_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_CableElectrique_Reco_reseau_reseau','attributes','EP_CableElectrique_Reco_reseau_reseau'); --GPKG

--XXX EP_CableTerre_Reco

select DropTable(NULL, 'ConducteurProtectionValue', TRUE);
CREATE TABLE	ConducteurProtectionValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ConducteurProtectionValue VALUES
  ('CuivreNu','Cuivre nu')
, ('CuivreIsol','Cuivre isolé')
, ('Sans','Sans')
, ('VertJaune','Vert-jaune')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ConducteurProtectionValue','attributes','ConducteurProtectionValue'); --GPKG

select DropTable(NULL, 'EP_CableTerre_Reco', TRUE);
CREATE TABLE EP_CableTerre_Reco(
    fid INTEGER PRIMARY KEY AUTOINCREMENT
  , ogr_pkid TEXT DEFAULT ('EP_CableTerre_Reco_0')
  , id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
  , FonctionCable_href TEXT NOT NULL REFERENCES FonctionCableElectriqueValue (valeurs)
  , NatureCableTerre_href TEXT NOT NULL REFERENCES ConducteurProtectionValue (valeurs)
  , Section NUMERIC NOT NULL
  , Section_uom TEXT DEFAULT 'mm-2'
  , Materiau TEXT NOT NULL REFERENCES CableMaterialTypeValue (valeurs)
  , Commentaire TEXT
  , Statut TEXT NOT NULL REFERENCES ConditionOfFacilityValue (valeurs)
  -- Cheminement
  , TypePose TEXT NOT NULL REFERENCES TypePoseValue (valeurs) --NOTE : hors reco star => TypePose pour aérien ou PleineTerre (Enterre)
  , Geometrie LINESTRINGZ NOT NULL UNIQUE
  , PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
  , PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
  , noeudreseau_href TEXT --QUESTION : UUID noeud arrivée terre?
);

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_CableTerre_Reco','features','EP_CableTerre_Reco'); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_CableTerre_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

select DropTable(NULL, 'EP_CableTerre_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_CableTerre_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_CableTerre_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_CableTerre_Reco_reseau_reseau','attributes','EP_CableTerre_Reco_reseau_reseau'); --GPKG

-- XXX Vue Cable
-- vue technique hors recostar

select DropTable(NULL, 'Cable', TRUE);
CREATE VIEW Cable as
with all_conso as (
  SELECT ogr_pkid, id, cast('CableElectrique' as text) type_cable, TypePose, FonctionCable_href FonctionCable, Section, Commentaire, Statut, PrecisionXY, PrecisionZ, Geometrie FROM EP_CableElectrique_Reco
  UNION ALL
  SELECT ogr_pkid, id, cast('CableTerre' as text) type_cable, TypePose, FonctionCable_href FonctionCable, Section, Commentaire, Statut, PrecisionXY, PrecisionZ, Geometrie FROM EP_CableTerre_Reco
  UNION ALL
  SELECT ogr_pkid, id, cast('CableTelecommunication' as text) type_cable, TypePose, 'Communication' FonctionCable, Null Section, Commentaire, Statut, PrecisionXY, PrecisionZ, Geometrie FROM EP_CableTelecommunication_Reco
) select cast(ROW_NUMBER () OVER () as int) fid, * from all_conso;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('Cable','features','Cable',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('Cable', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

--XXX Cheminements par defaut

select DropTable(NULL, 'EP_PleineTerre_Reco_virt', TRUE);
CREATE VIEW EP_PleineTerre_Reco_virt as
with difference as (
SELECT c.id, coalesce(ST_Difference(c."Geometrie", ST_Union(h."Geometrie")), c."Geometrie") Geometrie, c.PrecisionXY,c.PrecisionZ
FROM Cable c
LEFT JOIN Cheminement h ON ST_Within(h."Geometrie", c."Geometrie")
WHERE TypePose = 'Enterre'
group by c.id, c.PrecisionXY, c.PrecisionZ, c."Geometrie"
)
select cast(ROW_NUMBER () OVER () as int) fid, cast('EP_PleineTerre_Reco_virt_'||ROW_NUMBER () OVER () as text) ogr_pkid, cast(('id'||CreateUUID()) as text) id
, cast(Null as text) CoupeType, ST_GeometryN(c.Geometrie, s.value) Geometrie
, cast(Null as double) ProfondeurMinNonReg, cast('mm-2' as text) ProfondeurMinNonReg_uom, c.PrecisionXY, c.PrecisionZ
from difference c
, generate_series s ON s.value <= ST_NumGeometries(c.Geometrie)
;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_PleineTerre_Reco_virt','features','EP_PleineTerre_Reco_virt',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_PleineTerre_Reco_virt', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

select DropTable(NULL, 'EP_PleineTerre_Reco', TRUE);
CREATE TABLE EP_PleineTerre_Reco as --NOTE: VM à rafraichir avant chaque export
with uniiion as (
select * from EP_PleineTerre_Reco_line
union all
select * from EP_PleineTerre_Reco_virt
)
select cast(ROW_NUMBER () OVER () as int) fid, ogr_pkid, id, CoupeType, Geometrie, ProfondeurMinNonReg, ProfondeurMinNonReg_uom, PrecisionXY, PrecisionZ
from uniiion
;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_PleineTerre_Reco','features','EP_PleineTerre_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_PleineTerre_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

select DropTable(NULL, 'EP_PleineTerre_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_PleineTerre_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_PleineTerre_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_PleineTerre_Reco_reseau_reseau','attributes','EP_PleineTerre_Reco_reseau_reseau'); --GPKG

select DropTable(NULL, 'EP_Aerien_Reco_virt', TRUE);
CREATE VIEW EP_Aerien_Reco_virt as
with difference as (
SELECT c.id, coalesce(ST_Difference(c."Geometrie", ST_Union(h."Geometrie")), c."Geometrie") Geometrie, c.TypePose, c.PrecisionXY,c.PrecisionZ
FROM Cable c
LEFT JOIN Cheminement h ON ST_Within(h."Geometrie", c."Geometrie")
WHERE NOT TypePose = 'Enterre'
group by c.id, c.PrecisionXY, c.PrecisionZ, c."Geometrie"
)
select cast(ROW_NUMBER () OVER () as int) fid, cast('EP_Aerien_Reco_virt'||ROW_NUMBER () OVER () as text) ogr_pkid, cast(('id'||CreateUUID()) as text) id
, c.TypePose ModePose, ST_GeometryN(c.Geometrie, s.value) Geometrie
, c.PrecisionXY, c.PrecisionZ
from difference c
, generate_series s ON s.value <= ST_NumGeometries(c.Geometrie)
;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_Aerien_Reco_virt','features','EP_Aerien_Reco_virt',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_Aerien_Reco_virt', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

select DropTable(NULL, 'EP_Aerien_Reco', TRUE);
CREATE TABLE EP_Aerien_Reco as --NOTE: VM à rafraichir avant chaque export
with uniiion as (
select * from EP_Aerien_Reco_line
union all
select * from EP_Aerien_Reco_virt
)
select cast(ROW_NUMBER () OVER () as int) fid, ogr_pkid, id, ModePose, Geometrie, PrecisionXY, PrecisionZ
from uniiion
;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_Aerien_Reco','features','EP_Aerien_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_Aerien_Reco', 'Geometrie', 'LINESTRING', 2154, 1, 0); --GPKG

select DropTable(NULL, 'EP_Aerien_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_Aerien_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_Aerien_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Aerien_Reco_reseau_reseau','attributes','EP_Aerien_Reco_reseau_reseau'); --GPKG


--XXX Relation Cheminement_Cables
select DropTable(NULL, 'Cheminement_Cables', TRUE);
CREATE VIEW Cheminement_Cables AS
with uniiion as (
  select cast(c.id as text) cables_href
  , cast(h.id as text) cheminement_href
  FROM Cable c
  JOIN EP_Fourreau_Reco h ON ST_Within(h."Geometrie", c."Geometrie")
  union all
  select cast(c.id as text) cables_href
  , cast(h.id as text) cheminement_href
  FROM Cable c
  JOIN EP_ProtectionMecanique_Reco h ON ST_Within(h."Geometrie", c."Geometrie")
  union all
  select cast(c.id as text) cables_href
  , cast(h.id as text) cheminement_href
  FROM Cable c
  JOIN EP_Galerie_Reco h ON ST_Within(h."Geometrie", c."Geometrie")
  union all
  select cast(c.id as text) cables_href
  , cast(h.id as text) cheminement_href
  FROM Cable c
  JOIN EP_PleineTerre_Reco h ON ST_Within(h."Geometrie", c."Geometrie")
  union all
  select cast(c.id as text) cables_href
  , cast(h.id as text) cheminement_href
  FROM Cable c
  JOIN EP_Aerien_Reco h ON ST_Within(h."Geometrie", c."Geometrie")
)
select cast(ROW_NUMBER () OVER () as int) fid
, cast('Cheminement_Cables_'||ROW_NUMBER () OVER () as text) ogr_pkid
, ('id'||CreateUUID()) id
, *
, cast(null as text) cables_cables_EP_cableelectrique_reco_pkid
, cast(null as text) cables_cables_EP_cableterre_reco_pkid
, cast(null as text) cables_cables_EP_cabletelecommunication_reco_pkid
, cast(null as text) cheminement_cheminement_EP_aerien_reco_pkid
, cast(null as text) cheminement_cheminement_EP_fourreau_reco_pkid
, cast(null as text) cheminement_cheminement_EP_galerie_reco_pkid
, cast(null as text) cheminement_cheminement_EP_pleineterre_reco_pkid
, cast(null as text) cheminement_cheminement_EP_protectionmecanique_reco_pkid
from uniiion
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('Cheminement_Cables','attributes','Cheminement_Cables'); --GPKG
-- TODO : plusieurs cables peuvent emprunter le même cheminement (=> compléter NombreOuvrages dans les Fourreaux)
