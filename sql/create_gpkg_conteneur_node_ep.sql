-- select load_extension("/usr/local/lib/mod_spatialite.dylib");
-- SELECT EnableGpkgMode(); --GPKG

---XXX > Conteneurs

--XXX EP_BatimentTechnique_Reco

select DropTable(NULL, 'EP_BatimentTechnique_Reco_line', TRUE);
CREATE TABLE EP_BatimentTechnique_Reco_line (
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_BatimentTechnique_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, Materiau_href TEXT REFERENCES PipeMaterialTypeValue (valeurs)
, Commentaire TEXT
--NOTE: ATTRIBUTS EP_PosteElectrique_Reco :
, PosteElectrique BOOLEAN
, Categorie_href TEXT REFERENCES CategoriesPosteValue (valeurs)
, Fonction_href TEXT REFERENCES FonctionPosteValue (valeurs)
, TypePoste_href TEXT REFERENCES TypePosteValue (valeurs)
, TensionMax NUMERIC
, TensionMax_uom TEXT DEFAULT 'V'
, Commentaire_poste TEXT
--NOTE: ATTRIBUTS EP_CelluleHTA_Reco :
, NombreCelluleHTA INTEGER
, Fonction TEXT REFERENCES FonctionCelluleValue (valeurs) -- QUESTION : pas dans la XSD
, Nom TEXT
, CourantCourteDureeAdmissible NUMERIC
, CourantCourteDureeAdmissible_uom TEXT DEFAULT 'A'
, IntensiteAssignee NUMERIC
, IntensiteAssignee_uom TEXT DEFAULT 'A'
, TensionAssignee NUMERIC
, TensionAssignee_uom TEXT DEFAULT 'V'
, Commentaire_cellule TEXT
--NOTE: ATTRIBUTS EP_TableauHTA_Reco :
, NombreTableauHTA INTEGER
, NombreEmplacementsCellules INTEGER
, Commentaire_tableau TEXT
--NOTE: ATTRIBUTS EP_Transformateur_Reco :
, Transformateur BOOLEAN
, NatureEnroulement TEXT REFERENCES TypeTransfoValue (valeurs) -- QUESTION: cohérence nom attribut / liste
, Protection BOOLEAN
, Puissance NUMERIC
, Puissance_uom TEXT DEFAULT 'kVA' -- QUESTION : unité?
, ReglagePriseFixe_href TEXT REFERENCES ReglagePriseFixeValue (valeurs)
, Commentaire_transfo TEXT
--NOTE: ATTRIBUTS EP_Terre_Reco :
, Terre BOOLEAN
, NatureTerre_href TEXT REFERENCES NatureTerreValue (valeurs)
, Resistance NUMERIC
, Resistance_uom TEXT DEFAULT 'ohms'
, Commentaire_terre TEXT
-----------------------------------
, Statut TEXT NOT NULL REFERENCES ConditionOfFacilityValue (valeurs)
, Geometrie MULTILINESTRINGZ NOT NULL UNIQUE
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, geometriesupplementaire_href TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_BatimentTechnique_Reco_line','features','EP_BatimentTechnique_Reco_line',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_BatimentTechnique_Reco_line', 'Geometrie', 'MULTILINESTRING', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_BatimentTechnique_Reco_line', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_BatimentTechnique_Reco_line', 'Geometrie');

select DropTable(NULL, 'EP_BatimentTechnique_Reco', TRUE);
CREATE VIEW EP_BatimentTechnique_Reco as
select fid, ogr_pkid, id, ST_Centroid(Geometrie) Geometrie, PrecisionXY, PrecisionZ
from EP_BatimentTechnique_Reco_line;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_BatimentTechnique_Reco','features','EP_BatimentTechnique_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_BatimentTechnique_Reco', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG

select DropTable(NULL, 'EP_BatimentTechnique_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_BatimentTechnique_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_BatimentTechnique_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_BatimentTechnique_Reco_reseau_reseau','attributes','EP_BatimentTechnique_Reco_reseau_reseau'); --GPKG

--XXX EP_Coffret_Reco

select DropTable(NULL, 'TypeCoffretValue', TRUE);
CREATE TABLE TypeCoffretValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO TypeCoffretValue VALUES
  ('Coffret', 'Coffret')
, ('Armoire', 'Armoire')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('TypeCoffretValue','attributes','TypeCoffretValue'); --GPKG


select DropTable(NULL, 'ImplantationArmoireValue', TRUE);
CREATE TABLE ImplantationArmoireValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ImplantationArmoireValue VALUES
  ('Encastree', 'Encastrée')
, ('IntegreeDansLocal', 'Intégrée dans le local')
, ('Saillie', 'Saillie')
, ('SurSocleAluminium', 'Sur socle en aluminium')
, ('SurSocleBeton', 'Sur socle béton')
, ('SurSoclePolyester', 'Sur socle polyester')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ImplantationArmoireValue','attributes','ImplantationArmoireValue'); --GPKG

select DropTable(NULL, 'ImplantationCoffretValue', TRUE);
CREATE TABLE ImplantationCoffretValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ImplantationCoffretValue VALUES
  ('AppliqueEnFacade', 'Applique en façade')
, ('Deporte', 'Déporté')
, ('EnAppliqueSurMat', 'En applique sur mât')
, ('IncorporeDansLuminaire', 'Incorporé dans luminaire')
, ('PiedDeMat', 'Pied de mât')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ImplantationCoffretValue','attributes','ImplantationCoffretValue'); --GPKG


select DropTable(NULL, 'GeomCoffret', TRUE);
CREATE TABLE GeomCoffret (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  Geometrie LINESTRING
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('GeomCoffret','features','GeomCoffret',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('GeomCoffret', 'Geometrie', 'LINESTRING', 2154, 0, 0); --GPKG

-- Geom Coffret par Defaut 60x20cm -- QUESTION : quelles sont les dimensions pour l'EP?
INSERT INTO GeomCoffret VALUES
  ('Default',ST_AddPoint(ST_AddPoint(ST_AddPoint(ST_AddPoint(MakeLine(ST_Point(0,0),ST_Point(0.3,0)),ST_Point(0.3,0.2)),ST_Point(-0.3,0.2)),ST_Point(-0.3,0)),ST_Point(0,0))) --60x20cm
, ('Coffret',ST_AddPoint(ST_AddPoint(ST_AddPoint(ST_AddPoint(MakeLine(ST_Point(0,0),ST_Point(0.3,0)),ST_Point(0.3,0.2)),ST_Point(-0.3,0.2)),ST_Point(-0.3,0)),ST_Point(0,0))) --60x20cm
, ('Armoire',ST_AddPoint(ST_AddPoint(ST_AddPoint(ST_AddPoint(MakeLine(ST_Point(0,0),ST_Point(0.6,0)),ST_Point(0.6,0.3)),ST_Point(-0.6,0.3)),ST_Point(-0.6,0)),ST_Point(0,0))) --120x30cm
, ('Regard',ST_AddPoint(ST_AddPoint(ST_AddPoint(MakeLine(ST_Point(-0.15,-0.15),ST_Point(0.15,-0.15)),ST_Point(0.15,0.15)),ST_Point(-0.15,0.15)),ST_Point(-0.15,-0.15))) --30x30cm
;

select DropTable(NULL, 'EP_Coffret_Reco', TRUE);
CREATE TABLE EP_Coffret_Reco(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_Coffret_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, TypeCoffret TEXT NOT NULL REFERENCES TypeCoffretValue (valeurs) -- NOTE : dans QGIS adapter le formulaire en conséquence et les règles de remplissage
, ImplantationArmoire_href TEXT REFERENCES ImplantationArmoireValue (valeurs) --NOTE: SI ARMOIRE
, ImplantationCoffret_href TEXT REFERENCES ImplantationCoffretValue (valeurs) --NOTE: SI COFFRET
, Materiau_href TEXT REFERENCES PipeMaterialTypeValue (valeurs)
, TeinteCoffret TEXT -- NOTE : Couleur RAL
, Commentaire TEXT
--NOTE: SI ARMOIRE
--NOTE: ATTRIBUTS EP_PointDeComptage_Reco :
, PointDeComptage BOOLEAN --NOTE : hors reco star : permet d'améliorer l'interface
, NumeroPRM TEXT
, PuissanceSouscrite NUMERIC
, PuissanceSouscrite_uom TEXT DEFAULT 'kVA'
, Adresse TEXT
, Commentaire_compt TEXT
--NOTE: ATTRIBUTS EP_RaccordementDistributionEclairageExterieur_Reco :
, RaccordementDistributionEclairageExterieur BOOLEAN --NOTE : hors reco star : permet d'améliorer l'interface
, ReferenceAGCP TEXT
, Commentaire_raccext TEXT
--NOTE: ATTRIBUTS EP_TableauDeCommande_Reco :
, TableauDeCommande BOOLEAN --NOTE : hors reco star : permet d'améliorer l'interface
, NombreCablesSortie INTEGER
, NombreContacteurs INTEGER
, NombrePolesBornier INTEGER -- QUESTION : erreur dans la XSD c'est un TEXT avec réf
, PresenceBornierDALI BOOLEAN
, Commentaire_tableau TEXT
--NOTE: SI COFFRET
--NOTE: ATTRIBUTS EP_BoiteModulaire_Reco :
, BoiteModulaire BOOLEAN --NOTE : hors reco star : permet d'améliorer l'interface
, Commentaire_boite TEXT
--NOTE: ATTRIBUTS EP_BorneRepiquage_Reco :
, BorneRepiquage BOOLEAN --NOTE : hors reco star : permet d'améliorer l'interface
, NombreCablesSortie_borne INTEGER
, Commentaire_borne TEXT
--NOTE: ATTRIBUTS EP_Terre_Reco :
, Terre BOOLEAN
, NatureTerre_href TEXT REFERENCES NatureTerreValue (valeurs)
, Resistance NUMERIC
, Resistance_uom TEXT DEFAULT 'ohms'
, Commentaire_terre TEXT
-----------------------------------
, Statut TEXT NOT NULL REFERENCES ConditionOfFacilityValue (valeurs)
, Geometrie POINTZ NOT NULL UNIQUE
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, angle INTEGER --NOTE : hors reco star : permet de générer la géométrie supp orientée
, geometriesupplementaire_href TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_Coffret_Reco','features','EP_Coffret_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_Coffret_Reco', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_Coffret_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_Coffret_Reco', 'Geometrie');

select DropTable(NULL, 'EP_Coffret_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_Coffret_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_Coffret_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Coffret_Reco_reseau_reseau','attributes','EP_Coffret_Reco_reseau_reseau'); --GPKG

select DropTable(NULL, 'EP_Coffret_Reco_geometriesupplementaire', TRUE);
CREATE VIEW EP_Coffret_Reco_geometriesupplementaire as
select fid, c.ogr_pkid ogr_pkid, c.ogr_pkid parent_ogr_pkid
, geometriesupplementaire_href href
, cast(null as text) geometriesupplementair_EP_GeometrieSupplementaire_Reco_pkid
from EP_Coffret_Reco c ;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Coffret_Reco_geometriesupplementaire','attributes','EP_Coffret_Reco_geometriesupplementaire'); --GPKG


--XXX EP_Regard_Reco

select DropTable(NULL, 'EP_Regard_Reco', TRUE);
CREATE TABLE EP_Regard_Reco(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_Regard_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, Materiau_href TEXT REFERENCES PipeMaterialTypeValue (valeurs)
, Borgne BOOLEAN
, Classetampon TEXT
, Fonction TEXT
, GrilleProtection BOOLEAN NOT NULL -- FIXME : NULL dans la spec
, Visitable BOOLEAN NOT NULL -- FIXME : NULL dans la spec
, Commentaire TEXT
--NOTE: ATTRIBUTS EP_RaccordementLuminaire_Reco :
, RaccordementLuminaire BOOLEAN --NOTE : hors reco star : permet d'améliorer l'interface
, NombreCablesSortie INTEGER
, NombrePolesBornier INTEGER
, PresenceBornierDALI BOOLEAN
, ReferenceAGCP TEXT
, Commentaire_raccord TEXT
--NOTE: ATTRIBUTS EP_BoiteModulaire_Reco :
, BoiteModulaire BOOLEAN --NOTE : hors reco star : permet d'améliorer l'interface
, Commentaire_boite TEXT
--NOTE: ATTRIBUTS EP_BorneRepiquage_Reco :
, BorneRepiquage BOOLEAN --NOTE : hors reco star : permet d'améliorer l'interface
, NombreCablesSortie_borne INTEGER
, Commentaire_borne TEXT
-----------------------------------
, Statut TEXT NOT NULL REFERENCES ConditionOfFacilityValue (valeurs)
, Geometrie POINTZ NOT NULL UNIQUE
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, angle INTEGER --NOTE : hors reco star : permet de générer la géométrie supp orientée
, geometriesupplementaire_href TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_Regard_Reco','features','EP_Regard_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_Regard_Reco', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_Regard_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_Regard_Reco', 'Geometrie');

select DropTable(NULL, 'EP_Regard_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_Regard_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_Regard_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Regard_Reco_reseau_reseau','attributes','EP_Regard_Reco_reseau_reseau'); --GPKG

select DropTable(NULL, 'EP_Regard_Reco_geometriesupplementaire', TRUE);
CREATE VIEW EP_Regard_Reco_geometriesupplementaire as
select fid, c.ogr_pkid ogr_pkid, c.ogr_pkid parent_ogr_pkid
, geometriesupplementaire_href href
, cast(null as text) geometriesupplementair_EP_GeometrieSupplementaire_Reco_pkid
from EP_Regard_Reco c ;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Regard_Reco_geometriesupplementaire','attributes','EP_Regard_Reco_geometriesupplementaire'); --GPKG

--XXX EP_Support_Reco

select DropTable(NULL, 'ClasseSupportValue', TRUE);
CREATE TABLE ClasseSupportValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ClasseSupportValue VALUES -- QUESTION: IDEM RPD? > simplifier la liste (SLA)
  ('A',   'Ancien poteau béton simple')
, ('B',   'Ancien poteau béton simple')
, ('C',   'Ancien poteau béton simple')
, ('CFX', 'Contrefiché bois calé')
, ('CFY', 'Contrefiché bois')
, ('CFZ', 'Contrefiché bois')
, ('CH',  'Chevron bois')
, ('D',   'Béton simple rectangulaire')
, ('E',   'Béton simple carré')
, ('ER',  'Béton simple rond')
, ('HS',  'Haubanné bois')
, ('JA',  'Ancien poteau béton jumelé')
, ('JB',  'Ancien poteau béton jumelé')
, ('JC',  'Ancien poteau béton jumelé')
, ('JD',  'Béton rectangulaire jumelé')
, ('JE',  'Béton carré jumelé')
, ('JER', 'Béton rond jumelé')
, ('JS',  'Jumelé bois')
, ('M',   'Simple métallique')
, ('PA',  'Ancien portique béton')
, ('PB',  'Ancien portique béton')
, ('PC',  'Ancien portique béton')
, ('PCH', 'Portique chevron')
, ('PCHX','Portique chevron croisilloné')
, ('PD',  'Portique béton rectangulaire')
, ('PE',  'Portique béton carré')
, ('PER', 'Portique béton rond')
, ('PJA', 'Ancien portique jumelé béton')
, ('PJB', 'Ancien portique jumelé béton')
, ('PJC', 'Ancien portique jumelé béton')
, ('PJD', 'Portique jumelé béton rectang.')
, ('PJE', 'Portique jumelé béton carré.')
, ('PJER','Portique jumelé béton rond.')
, ('PJS', 'Portique jumelé bois')
, ('PJX', 'Portique bois jumelé croisillo')
, ('PM',  'Portique métallique')
, ('PS',  'Portique bois')
, ('PX',  'Portique bois croisilloné')
, ('S',   'Simple bois')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ClasseSupportValue','attributes','ClasseSupportValue'); --GPKG

select DropTable(NULL, 'FonctionSupportValue', TRUE);
CREATE TABLE FonctionSupportValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO FonctionSupportValue VALUES
  ('ARRDOUB', 'Arrêt double')
, ('ARRSIMP', 'Arrêt simple')
, ('DOUBANCR', 'Double ancrage')
, ('SEMIARR', 'Semi-arrête')
, ('SIMPFIX', 'Simple fixation')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('FonctionSupportValue','attributes','FonctionSupportValue'); --GPKG

select DropTable(NULL, 'FormeSupportValue', TRUE);
CREATE TABLE FormeSupportValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO FormeSupportValue VALUES
  ('Carre', 'Carré')
, ('CarreRetreint', 'Carré rétreint')
, ('Cintre', 'Cintré')
, ('Cylindrique', 'Cylindrique')
, ('CylindriqueRetreint', 'Cylindrique rétreint')
, ('CylindroConique', 'Cylindro-conique')
, ('Hexagonal', 'Hexagonal')
, ('OctoConique', 'Octo-conique')
, ('Style', 'Style')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('FormeSupportValue','attributes','FormeSupportValue'); --GPKG

select DropTable(NULL, 'SupportLuminaireValeur', TRUE);
CREATE TABLE SupportLuminaireValeur (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO SupportLuminaireValeur VALUES
  ('BUS', 'Abribus')
, ('CAT', 'Catenaire')
, ('ENS', 'Enseigne')
, ('MAT', 'Mât')
, ('MUR', 'Mur')
, ('OUV', 'Ouvrage')
, ('PAN', 'Panneau')
, ('POT', 'Poteau')
, ('SOL', 'Sol')
, ('AU', 'Autre')
, ('XX', 'Valeur inconnue')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('SupportLuminaireValeur','attributes','SupportLuminaireValeur'); --GPKG

select DropTable(NULL, 'MassifSupportValue', TRUE);
CREATE TABLE MassifSupportValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO MassifSupportValue VALUES
  ('Coule', 'Coulé')
, ('Prefabrique', 'Préfabriqué')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('MassifSupportValue','attributes','MassifSupportValue'); --GPKG

select DropTable(NULL, 'MatiereValue', TRUE);
CREATE TABLE MatiereValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO MatiereValue VALUES
  ('AcierGalvanise', 'Acier galvanisé')
, ('AcierGalvaniseThermolaque', 'Acier galvanisé thermolaqué')
, ('AcierThermolaque', 'Acier thermolaqué')
, ('AluminiumAnodise', 'Aluminium anodisé')
, ('AluminiumBrosse', 'Aluminium brossé')
, ('AluminiumThermolaque', 'Aluminium thermolaqué')
, ('Autre', 'Autre')
, ('Beton', 'Béton')
, ('Bois', 'Bois')
, ('BoisLamelleColle', 'Bois lamellé collé')
, ('Composite', 'Composite')
, ('FonteAcier', 'Fonte acier')
, ('FonteAluminium', 'Fonte aluminium')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('MatiereValue','attributes','MatiereValue'); --GPKG

select DropTable(NULL, 'EP_Support_Reco', TRUE);
CREATE TABLE EP_Support_Reco(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_Support_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, Materiau_href TEXT REFERENCES PipeMaterialTypeValue (valeurs)
, Classe_href TEXT REFERENCES ClasseSupportValue (valeurs)
, Effort NUMERIC
, Effort_uom TEXT DEFAULT 'kN'
, Enfouissement NUMERIC
, Enfouissement_uom TEXT DEFAULT 'm'
, FonctionSupport_href TEXT REFERENCES FonctionSupportValue (valeurs)
, FormeSupport_href TEXT REFERENCES FormeSupportValue (valeurs) -- FIXME : nom du champ = FormeSupportValue dans la XSD (= liste de valeur) ??? au lieu de FormeSupport ..
, HauteurPoteau NUMERIC
, HauteurPoteau_uom TEXT DEFAULT 'm'
, HauteurFixationConsole NUMERIC
, HauteurFixationConsole_uom TEXT DEFAULT 'm'
, SupportLuminaire TEXT REFERENCES SupportLuminaireValeur (valeurs) -- FIXME : SupportLuminaireValEUR ou ValUE??
, MassifSupport TEXT REFERENCES MassifSupportValue (valeurs)
, DimensionsMassifSupport NUMERIC
, DimensionsMassifSupport_uom TEXT DEFAULT 'm' -- QUESTION : unité? hauteurxlargeurxlongueur?
, MassifEntraxe NUMERIC
, MassifEntraxe_uom TEXT DEFAULT 'cm'
, Matiere_href TEXT REFERENCES MatiereValue (valeurs)
, TeinteSupport TEXT -- NOTE : Couleur RAL
, Commentaire TEXT
--NOTE: ATTRIBUTS EP_Armement_Reco :
, TypeArmement TEXT REFERENCES TypeArmementValue (valeurs)
, DecalageAccrochage NUMERIC
, DecalageAccrochage_uom TEXT DEFAULT 'm'
, Commentaire_armement TEXT
--NOTE: ATTRIBUTS EP_RaccordementLuminaire_Reco :
, RaccordementLuminaire BOOLEAN --NOTE : hors reco star : permet d'améliorer l'interface
, NombreCablesSortie INTEGER
, NombrePolesBornier INTEGER
, PresenceBornierDALI BOOLEAN
, ReferenceAGCP TEXT
, Commentaire_raccord TEXT
--NOTE: ATTRIBUTS EP_RemonteeAeroSouterraine_Reco :
, RemonteeAeroSouterraine BOOLEAN --NOTE : hors reco star : permet d'améliorer l'interface
, MateriauGoulotte_href TEXT REFERENCES MateriauGoulotteValue (valeurs)
, NombrePoles INTEGER
, PresenceGoulotte BOOLEAN
, TeinteGoulotte TEXT
, Commentaire_ras TEXT
-----------------------------------
, Orientation NUMERIC
, Orientation_uom TEXT DEFAULT '°'
, Statut TEXT NOT NULL REFERENCES ConditionOfFacilityValue (valeurs)
, Geometrie POINTZ NOT NULL UNIQUE
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, angle INTEGER --NOTE : hors reco star : permet d'améliorer le dessin
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_Support_Reco','features','EP_Support_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_Support_Reco', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_Support_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_Support_Reco', 'Geometrie');

select DropTable(NULL, 'EP_Support_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_Support_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_Support_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Support_Reco_reseau_reseau','attributes','EP_Support_Reco_reseau_reseau'); --GPKG


-- XXX Vue Conteneur
-- vue technique hors recostar

select DropTable(NULL, 'Conteneur', TRUE);
CREATE VIEW Conteneur as
with all_conso as (
  SELECT ogr_pkid, id, cast('BatimentTechnique' as text) type_conteneur, cast(null as text) Statut, PrecisionXY, PrecisionZ, Geometrie FROM EP_BatimentTechnique_Reco
  UNION ALL
  SELECT ogr_pkid, id, cast('Regard' as text) type_conteneur, Statut, PrecisionXY, PrecisionZ, Geometrie FROM EP_Regard_Reco
  UNION ALL
  SELECT ogr_pkid, id, cast('Coffret' as text) type_conteneur, Statut, PrecisionXY, PrecisionZ, Geometrie FROM EP_Coffret_Reco
  UNION ALL
  SELECT ogr_pkid, id, cast('Support' as text) type_conteneur, Statut, PrecisionXY, PrecisionZ, Geometrie FROM EP_Support_Reco
) select cast(ROW_NUMBER () OVER () as int) fid, * from all_conso;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('Conteneur','features','Conteneur',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('Conteneur', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG

--XXX EP_GeometrieSupplementaire_Reco

select DropTable(NULL, 'EP_GeometrieSupplementaire_Reco', TRUE);
CREATE VIEW EP_GeometrieSupplementaire_Reco as
with all_conso as (
  SELECT cast('EP_BatimentTechnique_Reco_geomsupp_'||fid as text) ogr_pkid, geometriesupplementaire_href id, id conteneur_id, cast('BatimentTechnique' as text) type_conteneur, Statut, PrecisionXY, PrecisionZ
  , Geometrie "Ligne2.5D"
  , CASE  WHEN ST_IsClosed(Geometrie) THEN ST_MakePolygon(Geometrie)
          WHEN ST_NumPoints(Geometrie) > 3 THEN ST_MakePolygon(ST_AddPoint(Geometrie, ST_StartPoint(Geometrie)))
          ELSE NULL END "Surface2.5D"
  FROM EP_BatimentTechnique_Reco_line
  UNION ALL
  SELECT cast('EP_Coffret_Reco_geomsupp_'||fid as text) ogr_pkid, geometriesupplementaire_href id, id conteneur_id, cast('Coffret' as text) type_conteneur, Statut, PrecisionXY, PrecisionZ
  , ST_Translate(RotateCoordinates(CastToXYZ(g.Geometrie),coalesce(angle,0)), ST_X(c.Geometrie), ST_Y(c.Geometrie), ST_Z(c.Geometrie)) "Ligne2.5D"
  , ST_Translate(RotateCoordinates(CastToXYZ(ST_MakePolygon(g.Geometrie)),coalesce(angle,0)), ST_X(c.Geometrie), ST_Y(c.Geometrie), ST_Z(c.Geometrie)) "Surface2.5D"
  FROM EP_Coffret_Reco c
  JOIN GeomCoffret g ON g.valeurs = c.TypeCoffret
  UNION ALL
  SELECT cast('EP_Regard_Reco_geomsupp_'||fid as text) ogr_pkid, geometriesupplementaire_href id, id conteneur_id, cast('Regard' as text) type_conteneur, Statut, PrecisionXY, PrecisionZ
  , ST_Translate(RotateCoordinates(CastToXYZ(g.Geometrie),coalesce(angle,0)), ST_X(c.Geometrie), ST_Y(c.Geometrie), ST_Z(c.Geometrie)) "Ligne2.5D"
  , ST_Translate(RotateCoordinates(CastToXYZ(ST_MakePolygon(g.Geometrie)),coalesce(angle,0)), ST_X(c.Geometrie), ST_Y(c.Geometrie), ST_Z(c.Geometrie)) "Surface2.5D"
  FROM EP_Regard_Reco c
  JOIN GeomCoffret g ON g.valeurs = 'Regard'
) select cast(ROW_NUMBER () OVER () as int) fid, * from all_conso;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_GeometrieSupplementaire_Reco','features','EP_GeometrieSupplementaire_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_GeometrieSupplementaire_Reco', 'Surface2.5D', 'MULTIPOLYGON', 2154, 1, 0); --GPKG


--XXX > Noeuds

--XXX EP_RaccordementDistributionEclairageExterieur_Reco

select DropTable(NULL, 'EP_RaccordementDistributionEclairageExterieur_Reco', TRUE);
CREATE TABLE EP_RaccordementDistributionEclairageExterieur_Reco as --NOTE: VM à rafraichir avant chaque export
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_RaccordementDistributionEclairageExterieur_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, cast(('id'||CreateUUID()) as text) id
, ReferenceAGCP
, Commentaire_raccext Commentaire
, Statut
, id conteneur_href
from EP_Coffret_Reco
where RaccordementDistributionEclairageExterieur = True;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_RaccordementDistributionEclairageExterieur_Reco','attributes','EP_RaccordementDistributionEclairageExterieur_Reco');

select DropTable(NULL, 'EP_RaccordementDistributionEclairageExterieur_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_RaccordementDistributionEclairageExterieur_Reco_reseau_reseau as -- QUESTION : Absent dans la XSD
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_RaccordementDistributionEclairageExterieur_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_RaccordementDistributionEclairageExterieur_Reco_reseau_reseau','attributes','EP_RaccordementDistributionEclairageExterieur_Reco_reseau_reseau'); --GPKG


--XXX EP_RaccordementLuminaire_Reco

select DropTable(NULL, 'EP_RaccordementLuminaire_Reco', TRUE);
CREATE TABLE EP_RaccordementLuminaire_Reco as --NOTE: VM à rafraichir avant chaque export
with uniiion as (
select
  cast(('id'||CreateUUID()) as text) id
, NombreCablesSortie
, NombrePolesBornier
, PresenceBornierDALI
, ReferenceAGCP
, Commentaire_raccord Commentaire
, Statut
, id conteneur_href
from EP_Support_Reco
where RaccordementLuminaire = True
UNION ALL
select
  cast(('id'||CreateUUID()) as text) id
, NombreCablesSortie
, NombrePolesBornier
, PresenceBornierDALI
, ReferenceAGCP
, Commentaire_raccord Commentaire
, Statut
, id conteneur_href
from EP_Regard_Reco
where RaccordementLuminaire = True
)
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_RaccordementLuminaire_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, * from uniiion;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_RaccordementLuminaire_Reco','attributes','EP_RaccordementLuminaire_Reco'); --GPKG

select DropTable(NULL, 'EP_RaccordementLuminaire_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_RaccordementLuminaire_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_RaccordementLuminaire_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_RaccordementLuminaire_Reco_reseau_reseau','attributes','EP_RaccordementLuminaire_Reco_reseau_reseau'); --GPKG


--XXX EP_BoiteModulaire_Reco

select DropTable(NULL, 'EP_BoiteModulaire_Reco', TRUE);
CREATE TABLE EP_BoiteModulaire_Reco as --NOTE: VM à rafraichir avant chaque export
with uniiion as (
select
  cast(('id'||CreateUUID()) as text) id
, Commentaire_boite Commentaire
, Statut
, id conteneur_href
from EP_Regard_Reco
where BoiteModulaire = True
UNION ALL
select
  cast(('id'||CreateUUID()) as text) id
, Commentaire_boite Commentaire
, Statut
, id conteneur_href
from EP_Coffret_Reco
where BoiteModulaire = True
)
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_BoiteModulaire_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, * from uniiion;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_BoiteModulaire_Reco','attributes','EP_BoiteModulaire_Reco'); --GPKG

select DropTable(NULL, 'EP_BoiteModulaire_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_BoiteModulaire_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_BoiteModulaire_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_BoiteModulaire_Reco_reseau_reseau','attributes','EP_BoiteModulaire_Reco_reseau_reseau'); --GPKG


--XXX EP_BorneRepiquage_Reco

select DropTable(NULL, 'EP_BorneRepiquage_Reco', TRUE);
CREATE TABLE EP_BorneRepiquage_Reco as --NOTE: VM à rafraichir avant chaque export
with uniiion as (
select
  cast(('id'||CreateUUID()) as text) id
, NombreCablesSortie_borne NombreCablesSortie
, Commentaire_borne Commentaire
, Statut
, Geometrie
, PrecisionXY
, PrecisionZ
, id conteneur_href
from EP_Regard_Reco
where BorneRepiquage = True
UNION ALL
select
  cast(('id'||CreateUUID()) as text) id
, NombreCablesSortie_borne NombreCablesSortie
, Commentaire_borne Commentaire
, Statut
, Geometrie
, PrecisionXY
, PrecisionZ
, id conteneur_href
from EP_Coffret_Reco
where BorneRepiquage = True
)
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_BorneRepiquage_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, * from uniiion;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_BorneRepiquage_Reco','features','EP_BorneRepiquage_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_BorneRepiquage_Reco', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG

select DropTable(NULL, 'EP_BorneRepiquage_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_BorneRepiquage_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_BorneRepiquage_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_BorneRepiquage_Reco_reseau_reseau','attributes','EP_BorneRepiquage_Reco_reseau_reseau'); --GPKG


--XXX EP_CelluleHTA_Reco

select DropTable(NULL, 'FonctionCelluleValue', TRUE);
CREATE TABLE FonctionCelluleValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO FonctionCelluleValue VALUES
  ('ArriveeHTA', 'Arrivée HTA')
, ('ArriveeTransformateur', 'Arrivée transformateur')
, ('Couplage', 'Couplage')
, ('DepartClientHTA', 'Départ client HTA')
, ('DepartCondensateurs', 'Départ condensateurs')
, ('DepartHTA', 'Départ HTA')
, ('LiaisonTransformateur', 'Liaison transformateur')
, ('Mesure', 'Mesure')
, ('PontBarres', 'Pont de barres')
, ('ProtectionTransformateur', 'Protection transformateur')
, ('Reserve', 'Réserve')
, ('ServiceAuxiliaires', 'Service des auxiliaires')
, ('Shunt', 'Shunt')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('FonctionCelluleValue','attributes','FonctionCelluleValue'); --GPKG

select DropTable(NULL, 'EP_CelluleHTA_Reco', TRUE);
CREATE TABLE EP_CelluleHTA_Reco as --NOTE: VM à rafraichir avant chaque export
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_CelluleHTA_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, cast(('id'||CreateUUID()) as text) id
, Fonction
, Nom
, CourantCourteDureeAdmissible
, CourantCourteDureeAdmissible_uom
, IntensiteAssignee
, IntensiteAssignee_uom
, TensionAssignee
, TensionAssignee_uom
, Commentaire_cellule Commentaire
, Statut
, id conteneur_href
from EP_BatimentTechnique_Reco_line
, generate_series s ON s.value <= NombreCelluleHTA
where NombreCelluleHTA > 0;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_CelluleHTA_Reco','attributes','EP_CelluleHTA_Reco');

select DropTable(NULL, 'EP_CelluleHTA_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_CelluleHTA_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_CelluleHTA_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_CelluleHTA_Reco_reseau_reseau','attributes','EP_CelluleHTA_Reco_reseau_reseau'); --GPKG


--XXX EP_CircuitEclairageExterieur_Reco

select DropTable(NULL, 'FonctionDepartValue', TRUE);
CREATE TABLE FonctionDepartValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO FonctionDepartValue VALUES
  ('Valeurs', 'Alias')
, ('Autre', 'Autre')
, ('Permanent', 'Permanent')
, ('Regulation', 'Régulation')
, ('SemiPermanent', 'Semi-permanent')
, ('Telegere', 'Télégéré')
, ('ToujoursSousTension', 'Toujours sous tension')
, ('Variation', 'Variation')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('FonctionDepartValue','attributes','FonctionDepartValue'); --GPKG

select DropTable(NULL, 'EP_CircuitEclairageExterieur_Reco', TRUE);
CREATE TABLE EP_CircuitEclairageExterieur_Reco(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_CircuitEclairageExterieur_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, DomaineTension TEXT REFERENCES DomaineTensionValue (valeurs) -- QUESTION : nécessaire? > NON (SLA)?
, Fonctionnement_href TEXT REFERENCES FonctionDepartValue (valeurs)
, LongueurElectriqueDepart NUMERIC
, LongueurElectriqueDepart_uom TEXT DEFAULT 'm'
, NombrePointsLumineuxRaccordes INTEGER
, NombreHeuresFonctionnementAnnuel INTEGER
, NomNumeroCircuit TEXT
, PuissanceActiveInstallee NUMERIC
, PuissanceActiveInstallee_uom TEXT DEFAULT 'W'
, IntensiteParPhase NUMERIC
, IntensiteParPhase_uom TEXT DEFAULT 'A'
, Cosphi NUMERIC
, Cosphi_uom TEXT DEFAULT ''
, Commentaire TEXT
, Statut TEXT NOT NULL REFERENCES ConditionOfFacilityValue (valeurs)
, Geometrie POINTZ NOT NULL UNIQUE --NOTE : hors reco star : permet de gérer les relations et la topologie
, conteneur_href TEXT -- REFERENCES Conteneur (id)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_CircuitEclairageExterieur_Reco','features','EP_CircuitEclairageExterieur_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_CircuitEclairageExterieur_Reco', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_CircuitEclairageExterieur_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_CircuitEclairageExterieur_Reco', 'Geometrie');

select DropTable(NULL, 'EP_CircuitEclairageExterieur_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_CircuitEclairageExterieur_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_CircuitEclairageExterieur_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_CircuitEclairageExterieur_Reco_reseau_reseau','attributes','EP_CircuitEclairageExterieur_Reco_reseau_reseau'); --GPKG


--XXX EP_Armement_Reco

select DropTable(NULL, 'TypeArmementValue', TRUE); -- QUESTION : Absent dans la XSD
CREATE TABLE TypeArmementValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);
-- QUESTION : Absent dans la XSD ?!
INSERT INTO TypeArmementValue VALUES
('NC', 'NonConcerné')
-- ...
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('TypeArmementValue','attributes','TypeArmementValue'); --GPKG

select DropTable(NULL, 'EP_Armement_Reco', TRUE);
CREATE TABLE EP_Armement_Reco as --NOTE: VM à rafraichir avant chaque export
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_Armement_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, cast(('id'||CreateUUID()) as text) id
, TypeArmement
, DecalageAccrochage
, DecalageAccrochage_uom
, Orientation
, Orientation_uom
, Commentaire_armement Commentaire
, Statut
, id conteneur_href
from EP_Support_Reco
where TypeArmement <> 'NC';

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Armement_Reco','attributes','EP_Armement_Reco'); --GPKG

select DropTable(NULL, 'EP_Armement_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_Armement_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_Armement_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Armement_Reco_reseau_reseau','attributes','EP_Armement_Reco_reseau_reseau'); --GPKG


--XXX EP_Jonction_Reco

select DropTable(NULL, 'TypeJonctionValue', TRUE);
CREATE TABLE TypeJonctionValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO TypeJonctionValue VALUES
  ('Derivation','Dérivation')
, ('ExtremiteReseau','Extrémité du réseau')
, ('Jonction','Jonction')
, ('RemonteeAeroSouterraine','Remontée aéro-souterraine')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('TypeJonctionValue','attributes','TypeJonctionValue'); --GPKG

select DropTable(NULL, 'EP_Jonction_Reco', TRUE);
CREATE TABLE EP_Jonction_Reco(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_Jonction_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, DomaineTension TEXT REFERENCES DomaineTensionValue (valeurs)
, TypeJonction TEXT REFERENCES TypeJonctionValue (valeurs)
, Commentaire TEXT
, Statut TEXT NOT NULL REFERENCES ConditionOfFacilityValue (valeurs)
, Geometrie POINTZ NOT NULL UNIQUE
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, angle INTEGER --NOTE : hors reco star : permet d'améliorer le dessin
, conteneur_href TEXT -- REFERENCES Conteneur (id)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_Jonction_Reco','features','EP_Jonction_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_Jonction_Reco', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_Jonction_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_Jonction_Reco', 'Geometrie');

select DropTable(NULL, 'EP_Jonction_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_Jonction_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_Jonction_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Jonction_Reco_reseau_reseau','attributes','EP_Jonction_Reco_reseau_reseau'); --GPKG


--XXX EP_Luminaire_Reco

select DropTable(NULL, 'TypeInstallationValue', TRUE);
CREATE TABLE TypeInstallationValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO TypeInstallationValue VALUES
  ('A', 'A')
, ('Autre', 'Autre')
, ('B1', 'B1')
, ('B2', 'B2')
, ('B3', 'B3')
, ('C', 'C')
, ('D', 'D')
, ('E', 'E')
, ('F', 'F')
, ('G', 'G')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('TypeInstallationValue','attributes','TypeInstallationValue'); --GPKG

select DropTable(NULL, 'TypeLuminaireValue', TRUE);
CREATE TABLE TypeLuminaireValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO TypeLuminaireValue VALUES
  ('Applique', 'Applique')
, ('Autre', 'Autre')
, ('Borne', 'Borne')
, ('BouleLumineuse', 'Boule lumineuse')
, ('Colonne', 'Colonne')
, ('EncastreMural', 'Encastré mural')
, ('EncastreSol', 'Encastré de sol')
, ('LanterneStyle', 'Lanterne de style')
, ('LuminaireAmbiance', 'Luminaire d''ambiance')
, ('LuminaireFonctionnel', 'Luminaire fonctionnel')
, ('Projecteur', 'Projecteur')
, ('Reglette', 'Réglette')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('TypeLuminaireValue','attributes','TypeLuminaireValue'); --GPKG

select DropTable(NULL, 'EP_Luminaire_Reco_pt', TRUE);
CREATE TABLE EP_Luminaire_Reco_pt(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_Luminaire_Reco_pt_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, NombreLuminaires INTEGER DEFAULT 1
, TypeInstallation_href TEXT REFERENCES TypeInstallationValue (valeurs)
, TypeLuminaire_href TEXT REFERENCES TypeLuminaireValue (valeurs)
, ClasseIsolement INTEGER
, HauteurFeu NUMERIC
, HauteurFeu_uom TEXT DEFAULT 'm'
, TeinteLuminaire TEXT
, ULRNominal NUMERIC
, ULRNominal_uom TEXT DEFAULT '%'
, Commentaire TEXT
, Statut TEXT NOT NULL REFERENCES ConditionOfFacilityValue (valeurs)
, Geometrie POINTZ NOT NULL UNIQUE
, PrecisionXY TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, PrecisionZ TEXT NOT NULL REFERENCES ClassePrecisionReseauValue (valeurs)
, angle INTEGER --NOTE : hors reco star : permet d'améliorer le dessin
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_Luminaire_Reco_pt','features','EP_Luminaire_Reco_pt',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_Luminaire_Reco_pt', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_Luminaire_Reco_pt', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_Luminaire_Reco_pt', 'Geometrie');

select DropTable(NULL, 'EP_Luminaire_Reco', TRUE);
CREATE TABLE EP_Luminaire_Reco as --NOTE: VM à rafraichir avant chaque export
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_Luminaire_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, cast(('id'||CreateUUID()) as text) id
, l.TypeInstallation_href
, l.TypeLuminaire_href
, l.ClasseIsolement
, l.HauteurFeu
, l.HauteurFeu_uom
, l.TeinteLuminaire
, l.ULRNominal
, l.ULRNominal_uom
, l.Commentaire
, l.Statut
, l.Geometrie
, l.PrecisionXY
, l.PrecisionZ
, l.angle
, coalesce(g.conteneur_id, c.id) conteneur_href
from EP_Luminaire_Reco_pt l
, generate_series s ON s.value <= NombreLuminaires
LEFT JOIN EP_GeometrieSupplementaire_Reco g ON PtDistWithin(g."Ligne2.5D", l."Geometrie", 0.002) OR PtDistWithin(g."Surface2.5D", l."Geometrie", 0.002)
LEFT JOIN Conteneur c ON c.type_conteneur = 'Support' AND PtDistWithin(c."Geometrie", l."Geometrie", 0.002)
where NombreLuminaires > 0
;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_Luminaire_Reco','features','EP_Luminaire_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_Luminaire_Reco', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG

select DropTable(NULL, 'EP_Luminaire_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_Luminaire_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_Luminaire_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Luminaire_Reco_reseau_reseau','attributes','EP_Luminaire_Reco_reseau_reseau'); --GPKG


--XXX EP_PointDeComptage_Reco

select DropTable(NULL, 'EP_PointDeComptage_Reco', TRUE);
CREATE TABLE EP_PointDeComptage_Reco as --NOTE: VM à rafraichir avant chaque export
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_PointDeComptage_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, cast(('id'||CreateUUID()) as text) id
, NumeroPRM
, PuissanceSouscrite
, PuissanceSouscrite_uom
, Adresse
, Commentaire_compt Commentaire
, Statut
, id conteneur_href
from EP_Coffret_Reco
where PointDeComptage = True;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_PointDeComptage_Reco','attributes','EP_PointDeComptage_Reco');

select DropTable(NULL, 'EP_PointDeComptage_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_PointDeComptage_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_PointDeComptage_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_PointDeComptage_Reco_reseau_reseau','attributes','EP_PointDeComptage_Reco_reseau_reseau'); --GPKG


--XXX EP_PosteElectrique_Reco

select DropTable(NULL, 'CategoriesPosteValue', TRUE);
CREATE TABLE CategoriesPosteValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO CategoriesPosteValue VALUES
  ('Distribution','Poste de distribution')
, ('Manoeuvre','Poste de manœuvre')
, ('PosteSource','Poste source')
, ('RepartitionHTA','Poste de répartition HTA')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('CategoriesPosteValue','attributes','CategoriesPosteValue'); --GPKG

select DropTable(NULL, 'FonctionPosteValue', TRUE);
CREATE TABLE FonctionPosteValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO FonctionPosteValue VALUES
  ('InjectionHTA', 'Injection HTA')
, ('RepartitionBT', 'Répartition BT')
, ('RepartitionHTAavecProtection', 'Répartition HTA avec protection')
, ('RepartitionHTAsansProtection', 'Répartition HTA sans protection')
, ('SoutirageHTA', 'Soutirage HTA')
, ('TransformationHTA-BT', 'Transformation HTA/BT')
, ('TransformationHTA-HTA', 'Transformation HTA/HTA')
, ('TransformationHTB-HTA', 'Transformation HTB/HTA')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('FonctionPosteValue','attributes','FonctionPosteValue'); --GPKG

select DropTable(NULL, 'TypePosteValue', TRUE);
CREATE TABLE TypePosteValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text,
  categorie text
);

INSERT INTO TypePosteValue VALUES
  ('CB','Cabine Basse', 'Distribution')
, ('CC','Cabine de chantier', 'Distribution')
, ('CH','Cabine haute', 'Distribution')
, ('IM','En Immeuble', 'Distribution')
, ('EN','En Terre', 'Distribution')
, ('PSSA','Poste au Sol Simplifié de Type A', 'Distribution')
, ('PSSB','Poste au Sol Simplifié de Type B', 'Distribution')
, ('PRCS','Poste Rural Compact Socle', 'Distribution')
, ('PUIE','Poste Urbain Intégré à son Environnement', 'Distribution')
, ('H6','Poteau H61', 'Distribution')
, ('PO','Poteau non H61', 'Distribution')
, ('RC','Rural Compact', 'Distribution')
, ('RS','Rural Socle', 'Distribution')
, ('UC','Urbain Compact', 'Distribution')
, ('UP','Urbain Portable (PAC)', 'Distribution')
, ('HTEP','Poste Haute tension - Eclairage Public', 'Distribution')
, ('GRSC','Poste Source Groupe SC Classification)', 'PosteSource')
, ('GR1','Poste Source Groupe 1', 'PosteSource')
, ('GR2A','Poste Source Groupe 2A', 'PosteSource')
, ('GR2B','Poste Source Groupe 2B', 'PosteSource')
, ('GR2C','Poste Source Groupe 2C', 'PosteSource')
, ('GR2D','Poste Source Groupe 2D', 'PosteSource')
, ('GR2E','Poste Source Groupe 2E', 'PosteSource')
, ('GR2F','Poste Source Groupe 2F', 'PosteSource')
, ('GR3','Poste Source Groupe 3', 'PosteSource')
, ('GHTA','Poste de répartition HTA', 'RepartitionHTA')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('TypePosteValue','attributes','TypePosteValue'); --GPKG

select DropTable(NULL, 'EP_PosteElectrique_Reco_virt', TRUE);
CREATE VIEW EP_PosteElectrique_Reco_virt as --NOTE: Vue pour la visualisation des postes dans QGIS
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_PosteElectrique_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, cast(('id'||CreateUUID()) as text) id
, Categorie_href
, Fonction_href
, TypePoste_href
, TensionMax
, TensionMax_uom
, Commentaire_poste Commentaire
, Statut
, id conteneur_href
--NOTE: ATTRIBUTS EP_CelluleHTA_Reco :
, NombreCelluleHTA
, Fonction
, Nom
, CourantCourteDureeAdmissible
, CourantCourteDureeAdmissible_uom
, IntensiteAssignee
, IntensiteAssignee_uom
, TensionAssignee
, TensionAssignee_uom
, Commentaire_cellule
--NOTE: ATTRIBUTS EP_TableauHTA_Reco :
, NombreTableauHTA
, NombreEmplacementsCellules
, Commentaire_tableau
--NOTE: ATTRIBUTS EP_Transformateur_Reco :
, Transformateur
, NatureEnroulement
, Protection
, Puissance
, Puissance_uom
, ReglagePriseFixe_href
, Commentaire_transfo
, ST_Centroid(Geometrie) Geometrie
from EP_BatimentTechnique_Reco_line;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_PosteElectrique_Reco_virt','features','EP_PosteElectrique_Reco_virt',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_PosteElectrique_Reco_virt', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG

select DropTable(NULL, 'EP_PosteElectrique_Reco', TRUE);
CREATE TABLE EP_PosteElectrique_Reco as --NOTE: VM à rafraichir avant chaque export
select fid
, ogr_pkid
, id
, Categorie_href
, Fonction_href
, TypePoste_href
, TensionMax
, TensionMax_uom
, Commentaire
, Statut
, conteneur_href
from EP_PosteElectrique_Reco_virt;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_PosteElectrique_Reco','attributes','EP_PosteElectrique_Reco');

select DropTable(NULL, 'EP_PosteElectrique_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_PosteElectrique_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_PosteElectrique_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_PosteElectrique_Reco_reseau_reseau','attributes','EP_PosteElectrique_Reco_reseau_reseau'); --GPKG


--XXX EP_RemonteeAeroSouterraine_Reco

select DropTable(NULL, 'MateriauGoulotteValue', TRUE);
CREATE TABLE MateriauGoulotteValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO MateriauGoulotteValue VALUES
  ('Acier', 'Acier')
, ('Alu', 'Alu')
, ('Plastique', 'Plastique')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('MateriauGoulotteValue','attributes','MateriauGoulotteValue'); --GPKG

select DropTable(NULL, 'EP_RemonteeAeroSouterraine_Reco', TRUE);
CREATE TABLE EP_RemonteeAeroSouterraine_Reco as --NOTE: VM à rafraichir avant chaque export
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_RemonteeAeroSouterraine_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, cast(('id'||CreateUUID()) as text) id
, MateriauGoulotte_href
, NombrePoles
, PresenceGoulotte
, TeinteGoulotte
, Commentaire_ras Commentaire
, Statut
, Geometrie
, PrecisionXY
, PrecisionZ
, id conteneur_href
from EP_Support_Reco
where RemonteeAeroSouterraine = True;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_RemonteeAeroSouterraine_Reco','features','EP_RemonteeAeroSouterraine_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_RemonteeAeroSouterraine_Reco', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG

select DropTable(NULL, 'EP_RemonteeAeroSouterraine_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_RemonteeAeroSouterraine_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_RemonteeAeroSouterraine_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_RemonteeAeroSouterraine_Reco_reseau_reseau','attributes','EP_RemonteeAeroSouterraine_Reco_reseau_reseau'); --GPKG

--XXX EP_TableauBT_Reco
--NOTE : présent dans la XSD mais inutile

--XXX EP_TableauDeCommande_Reco

select DropTable(NULL, 'EP_TableauDeCommande_Reco', TRUE);
CREATE TABLE EP_TableauDeCommande_Reco as --NOTE: VM à rafraichir avant chaque export
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_TableauDeCommande_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, cast(('id'||CreateUUID()) as text) id
, NombreCablesSortie
, NombreContacteurs
, NombrePolesBornier
, PresenceBornierDALI
, Commentaire_tableau Commentaire
, Statut
, id conteneur_href
from EP_Coffret_Reco
where TableauDeCommande = True;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_TableauDeCommande_Reco','attributes','EP_TableauDeCommande_Reco');

select DropTable(NULL, 'EP_TableauDeCommande_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_TableauDeCommande_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_TableauDeCommande_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_TableauDeCommande_Reco_reseau_reseau','attributes','EP_TableauDeCommande_Reco_reseau_reseau'); --GPKG


--XXX EP_TableauHTA_Reco

select DropTable(NULL, 'EP_TableauHTA_Reco', TRUE);
CREATE TABLE EP_TableauHTA_Reco as --NOTE: VM à rafraichir avant chaque export
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_TableauHTA_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, cast(('id'||CreateUUID()) as text) id
, NombreEmplacementsCellules
, Commentaire_tableau Commentaire
, Statut
, id conteneur_href
from EP_BatimentTechnique_Reco_line
, generate_series s ON s.value <= NombreTableauHTA
where NombreTableauHTA > 0;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_TableauHTA_Reco','attributes','EP_TableauHTA_Reco');

select DropTable(NULL, 'EP_TableauHTA_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_TableauHTA_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_TableauHTA_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_TableauHTA_Reco_reseau_reseau','attributes','EP_TableauHTA_Reco_reseau_reseau'); --GPKG


--XXX EP_Terre_Reco

select DropTable(NULL, 'NatureTerreValue', TRUE);
CREATE TABLE NatureTerreValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO NatureTerreValue VALUES
  ('TerreMasses','Terre des masses métalliques')
, ('TerreNeutre','Terre du neutre de la distribution')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('NatureTerreValue','attributes','NatureTerreValue'); --GPKG

select DropTable(NULL, 'EP_Terre_Reco', TRUE);
CREATE TABLE EP_Terre_Reco as --NOTE: VM à rafraichir avant chaque export
with uniiion as (
select
  cast(('id'||CreateUUID()) as text) id
, NatureTerre_href
, Resistance
, Resistance_uom
, Commentaire_terre Commentaire
, Statut
, id conteneur_href
from EP_Coffret_Reco
where Terre = True
UNION ALL
select
  cast(('id'||CreateUUID()) as text) id
, NatureTerre_href
, Resistance
, Resistance_uom
, Commentaire_terre Commentaire
, Statut
, id conteneur_href
from EP_BatimentTechnique_Reco_line
where Terre = True
)
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_Terre_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, * from uniiion;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Terre_Reco','attributes','EP_Terre_Reco'); --GPKG

select DropTable(NULL, 'EP_Terre_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_Terre_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_Terre_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Terre_Reco_reseau_reseau','attributes','EP_Terre_Reco_reseau_reseau'); --GPKG


--XXX EP_Transformateur_Reco

select DropTable(NULL, 'TypeTransfoValue', TRUE);
CREATE TABLE TypeTransfoValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO TypeTransfoValue VALUES
  ('AutoTransformateur', 'Autotransformateur')
, ('EnroulementsSepares', 'Enroulements séparés')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('TypeTransfoValue','attributes','TypeTransfoValue'); --GPKG

select DropTable(NULL, 'ReglagePriseFixeValue', TRUE);
CREATE TABLE ReglagePriseFixeValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO ReglagePriseFixeValue VALUES
  ('0', '0%')
, ('2,5', '2,5%')
, ('5', '5%')
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ReglagePriseFixeValue','attributes','ReglagePriseFixeValue'); --GPKG

select DropTable(NULL, 'EP_Transformateur_Reco', TRUE);
CREATE TABLE EP_Transformateur_Reco as --NOTE: VM à rafraichir avant chaque export
select cast(ROW_NUMBER () OVER () as int) fid
, cast('EP_Transformateur_Reco_'||ROW_NUMBER () OVER () as text) ogr_pkid
, cast(('id'||CreateUUID()) as text) id
, NatureEnroulement
, Protection
, Puissance
, Puissance_uom
, ReglagePriseFixe_href
, Commentaire_transfo Commentaire
, Statut
, id conteneur_href
from EP_BatimentTechnique_Reco_line
where Transformateur = True;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Transformateur_Reco','attributes','EP_Transformateur_Reco');

select DropTable(NULL, 'EP_Transformateur_Reco_reseau_reseau', TRUE);
CREATE VIEW EP_Transformateur_Reco_reseau_reseau as
select cast(ROW_NUMBER () OVER () as int) fid, c.ogr_pkid parent_pkid, r.ogr_pkid child_pkid from EP_Transformateur_Reco c, Reseau r;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('EP_Transformateur_Reco_reseau_reseau','attributes','EP_Transformateur_Reco_reseau_reseau'); --GPKG


-- XXX Vue Noeud
-- vue technique hors recostar

select DropTable(NULL, 'Noeud', TRUE);
CREATE VIEW Noeud as
with all_conso as (
  SELECT ogr_pkid, id, cast('Luminaire' as text) type_noeud, Statut, 3 Ordre, PrecisionXY, PrecisionZ, Geometrie, conteneur_href FROM EP_Luminaire_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('Armement' as text) type_noeud, Statut, NULL Ordre, NULL, NULL, NULL, conteneur_href FROM EP_Armement_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('RemonteeAeroSouterraine' as text) type_noeud, Statut, 2 Ordre, PrecisionXY, PrecisionZ, Geometrie, conteneur_href FROM EP_RemonteeAeroSouterraine_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('RaccordementLuminaire' as text) type_noeud, Statut, 1 Ordre, NULL, NULL, NULL, conteneur_href FROM EP_RaccordementLuminaire_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('BorneRepiquage' as text) type_noeud, Statut, 1 Ordre, PrecisionXY, PrecisionZ, Geometrie, conteneur_href FROM EP_BorneRepiquage_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('BoiteModulaire' as text) type_noeud, Statut, 1 Ordre, NULL, NULL, NULL, conteneur_href FROM EP_BoiteModulaire_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('Transformateur' as text) type_noeud, Statut, 1 Ordre, NULL, NULL, NULL, conteneur_href FROM EP_Transformateur_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('Jonction' as text) type_noeud, Statut, 1 Ordre, PrecisionXY, PrecisionZ, Geometrie, conteneur_href FROM EP_Jonction_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('PointDeComptage' as text) type_noeud, Statut, 4 Ordre, NULL, NULL, NULL, conteneur_href FROM EP_PointDeComptage_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('RaccordementDistributionEclairageExterieur' as text) type_noeud, Statut, 3 Ordre, NULL, NULL, NULL, conteneur_href FROM EP_RaccordementDistributionEclairageExterieur_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('TableauDeCommande' as text) type_noeud, Statut, 2 Ordre, NULL, NULL, NULL, conteneur_href FROM EP_TableauDeCommande_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('CircuitEclairageExterieur' as text) type_noeud, Statut, 1 Ordre, NULL, NULL, Geometrie, conteneur_href FROM EP_CircuitEclairageExterieur_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('Terre' as text) type_noeud, Statut, NULL Ordre, NULL, NULL, NULL, conteneur_href FROM EP_Terre_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('CelluleHTA' as text) type_noeud, Statut, NULL Ordre, NULL, NULL, NULL, conteneur_href FROM EP_CelluleHTA_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('TableauHTA' as text) type_noeud, Statut, NULL Ordre, NULL, NULL, NULL, conteneur_href FROM EP_TableauHTA_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('PosteElectrique' as text) type_noeud, Statut, NULL Ordre, NULL, NULL, Geometrie, conteneur_href FROM EP_PosteElectrique_Reco
) select cast(ROW_NUMBER () OVER () as int) fid, * from all_conso;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('Noeud','features','Noeud',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('Noeud', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG

--XXX Vue Conteneur_Noeud
-- vue technique hors recostar
-- permet de renseigner conteneur_href sur les tables noeuds

select DropTable(NULL, 'Conteneur_Noeud', TRUE);
CREATE VIEW Conteneur_Noeud as
with all_conso as (
  SELECT n.id noeud_id, type_noeud, c.conteneur_id conteneur_id, type_conteneur
  FROM Noeud n
  JOIN EP_GeometrieSupplementaire_Reco c ON PtDistWithin(c."Ligne2.5D", n."Geometrie", 0.002) OR PtDistWithin(c."Surface2.5D", n."Geometrie", 0.002)
  where conteneur_href is null
  UNION ALL
  SELECT n.id noeud_id, type_noeud, c.id conteneur_id, type_conteneur
  FROM Noeud n
  JOIN Conteneur c ON c.type_conteneur = 'Support' AND PtDistWithin(c."Geometrie", n."Geometrie", 0.002)
  where conteneur_href is null
  UNION ALL
  SELECT n.id noeud_id, type_noeud, c.id conteneur_id, type_conteneur
  FROM Noeud n
  JOIN Conteneur c ON c.id = n.conteneur_href
  where conteneur_href is not null
) select cast(ROW_NUMBER () OVER () as int) fid, * from all_conso;
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('Conteneur_Noeud','attributes','Conteneur_Noeud'); --GPKG

--XXX Vue Conteneur_Cable
-- vue technique hors recostar`

select DropTable(NULL, 'Conteneur_Cable', TRUE);
CREATE VIEW Conteneur_Cable as
with all_conso as (
  SELECT n.id cable_id, type_cable, cast('StartPoint' as text) connpt, c.conteneur_id conteneur_id, type_conteneur
  FROM Cable n
  JOIN EP_GeometrieSupplementaire_Reco c ON PtDistWithin(c."Ligne2.5D", ST_StartPoint(n."Geometrie"), 0.002) OR PtDistWithin(c."Surface2.5D", ST_StartPoint(n."Geometrie"), 0.002)
  UNION ALL
  SELECT n.id cable_id, type_cable, cast('StartPoint' as text) connpt, c.id conteneur_id, type_conteneur
  FROM Cable n
  JOIN Conteneur c ON c.type_conteneur = 'Support' AND PtDistWithin(c."Geometrie", ST_StartPoint(n."Geometrie"), 0.002)
  UNION ALL
  SELECT n.id cable_id, type_cable, cast('EndPoint' as text) connpt, c.conteneur_id conteneur_id, type_conteneur
  FROM Cable n
  JOIN EP_GeometrieSupplementaire_Reco c ON PtDistWithin(c."Ligne2.5D", ST_EndPoint(n."Geometrie"), 0.002) OR PtDistWithin(c."Surface2.5D", ST_EndPoint(n."Geometrie"), 0.002)
  UNION ALL
  SELECT n.id cable_id, type_cable, cast('EndPoint' as text) connpt, c.id conteneur_id, type_conteneur
  FROM Cable n
  JOIN Conteneur c ON c.type_conteneur = 'Support' AND PtDistWithin(c."Geometrie", ST_EndPoint(n."Geometrie"), 0.002)
) select cast(ROW_NUMBER () OVER () as int) fid, * from all_conso;
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('Conteneur_Cable','attributes','Conteneur_Cable'); --GPKG

--XXX Vue Noeud_Cable
-- vue technique hors recostar`

select DropTable(NULL, 'Noeud_Cable', TRUE);
CREATE VIEW Noeud_Cable as
with all_conso as (
  SELECT c.id cable_id, type_cable, cast('StartPoint' as text) connpt, n.id noeud_id, type_noeud, ordre
  FROM Noeud n
  JOIN Cable c ON PtDistWithin(n."Geometrie", ST_StartPoint(c."Geometrie"), 0.002)
  UNION ALL
  SELECT c.id cable_id, type_cable, cast('EndPoint' as text) connpt, n.id noeud_id, type_noeud, ordre
  FROM Noeud n
  JOIN Cable c ON PtDistWithin(n."Geometrie", ST_EndPoint(c."Geometrie"), 0.002)
) select cast(ROW_NUMBER () OVER () as int) fid, * from all_conso;
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('Noeud_Cable','attributes','Noeud_Cable'); --GPKG

--XXX Relation CableElectrique_NoeudReseau
-- FIXME  à vérifier dans la XSD

select DropTable(NULL, 'CableElectrique_NoeudReseau', TRUE);
CREATE VIEW CableElectrique_NoeudReseau AS
with uniiion as (
  select cast(c.id as text) cableelectrique_href
  , cast(n.noeud_id as text) noeudreseau_href
  , connpt
  , -1 dist
  , n.ordre
  FROM EP_CableElectrique_Reco c
  JOIN Noeud_Cable n ON n.cable_id=c.id
  union all
  select cast(c.id as text) cableelectrique_href
  , cast(n.noeud_id as text) noeudreseau_href
  , connpt
  , ST_Distance(c.Geometrie, m.Geometrie)
  , m.ordre
  FROM EP_CableElectrique_Reco c
  JOIN Conteneur_Cable h ON h.cable_id=c.id
  JOIN Conteneur_Noeud n ON n.conteneur_id=h.conteneur_id
  JOIN Noeud m on m.id = n.noeud_id
)
, diiistinct as (
select cableelectrique_href, noeudreseau_href, connpt--, dist, ordre
from uniiion a
WHERE NOT EXISTS (SELECT 1 FROM uniiion b WHERE b.cableelectrique_href=a.cableelectrique_href
                  and b.connpt=a.connpt and ((b.dist <= a.dist and b.ordre < a.ordre) or (b.dist < a.dist)))
)
SELECT
  cast(ROW_NUMBER () OVER () as int) fid
, cast('CableElectrique_NoeudReseau_'||ROW_NUMBER () OVER () as text) ogr_pkid
, ('id'||CreateUUID()) id
, *
from diiistinct
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('CableElectrique_NoeudReseau','attributes','CableElectrique_NoeudReseau'); --GPKG

--XXX > Equipements
--XXX EP_SourceLumineuse_Reco
--TODO: à faire

--XXX EP_Materiel_Reco ????
-- QUESTION: quels sont ls ouvrages qui nécessitent cette info?

--XXX EP_PointLeveOuvrageReseau_Reco

select DropTable(NULL, 'LeveTypeValue', TRUE);
CREATE TABLE LeveTypeValue (
  valeurs text NOT NULL UNIQUE PRIMARY KEY,
  alias text
);

INSERT INTO LeveTypeValue VALUES
  ('AltitudeGeneratrice','Altitude à la génératrice') -- z gs
, ('ChargeGeneratrice','Charge à la génératrice') -- profondeur gs
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('LeveTypeValue','attributes','LeveTypeValue'); --GPKG

select DropTable(NULL, 'EP_PointLeveOuvrageReseau_Reco', TRUE);
CREATE TABLE EP_PointLeveOuvrageReseau_Reco(
  fid INTEGER PRIMARY KEY AUTOINCREMENT
, ogr_pkid TEXT DEFAULT ('EP_PointLeveOuvrageReseau_Reco_0')
, id TEXT NOT NULL UNIQUE DEFAULT ('id'||CreateUUID())
, NumeroPoint TEXT NOT NULL
, CodeOuvrage TEXT --NOTE : hors reco star => permet de tracer les lignes en auto
, Leve NUMERIC NOT NULL -- ZGS ou Profondeur
, Leve_uom TEXT DEFAULT 'm'
, TypeLeve TEXT NOT NULL REFERENCES LeveTypeValue (valeurs)
, Producteur TEXT NOT NULL
--, Horodatage DATE --FIXME : NOT NULL (mais probleme qgis plugin import) >>> créer attribut date dans formulaire
, Geometrie POINTZ NOT NULL --UNIQUE
, PrecisionXYnum INTEGER NOT NULL
, PrecisionZnum INTEGER NOT NULL
, UNIQUE (Geometrie, CodeOuvrage)
);

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('EP_PointLeveOuvrageReseau_Reco','features','EP_PointLeveOuvrageReseau_Reco',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('EP_PointLeveOuvrageReseau_Reco', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG
SELECT gpkgAddSpatialIndex('EP_PointLeveOuvrageReseau_Reco', 'Geometrie' );
select gpkgAddGeometryTriggers('EP_PointLeveOuvrageReseau_Reco', 'Geometrie');
