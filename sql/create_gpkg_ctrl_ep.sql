--CONTROLES TOPO

-- XXX Vue Noeud
-- vue technique hors recostar

select DropTable(NULL, 'Noeud_virt', TRUE);
CREATE VIEW Noeud_virt as
with all_conso as (
  SELECT ogr_pkid, id, cast('Luminaire' as text) type_noeud, Statut, 3 Ordre, PrecisionXY, PrecisionZ, Geometrie, NULL conteneur_href FROM EP_Luminaire_Reco_pt
    UNION ALL
  SELECT ogr_pkid, id, cast('Armement' as text) type_noeud, Statut, NULL Ordre, NULL, NULL, NULL, id conteneur_href FROM EP_Support_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('RemonteeAeroSouterraine' as text) type_noeud, Statut, 2 Ordre, PrecisionXY, PrecisionZ, Geometrie, id conteneur_href FROM EP_Support_Reco where RemonteeAeroSouterraine = True
    UNION ALL
  SELECT ogr_pkid, id, cast('RaccordementLuminaire' as text) type_noeud, Statut, 1 Ordre, NULL, NULL, NULL, id conteneur_href FROM EP_Support_Reco where RaccordementLuminaire = True
    UNION ALL
  SELECT ogr_pkid, id, cast('BorneRepiquage' as text) type_noeud, Statut, 1 Ordre, PrecisionXY, PrecisionZ, Geometrie, id conteneur_href FROM EP_Regard_Reco where BorneRepiquage = True
    UNION ALL
  SELECT ogr_pkid, id, cast('BorneRepiquage' as text) type_noeud, Statut, 1 Ordre, PrecisionXY, PrecisionZ, Geometrie, id conteneur_href FROM EP_Coffret_Reco where BorneRepiquage = True
    UNION ALL
  SELECT ogr_pkid, id, cast('BoiteModulaire' as text) type_noeud, Statut, 1 Ordre, NULL, NULL, NULL, id conteneur_href FROM EP_Regard_Reco where BoiteModulaire = True
    UNION ALL
  SELECT ogr_pkid, id, cast('BoiteModulaire' as text) type_noeud, Statut, 1 Ordre, NULL, NULL, NULL, id conteneur_href FROM EP_Coffret_Reco where BoiteModulaire = True
    UNION ALL
  SELECT ogr_pkid, id, cast('Transformateur' as text) type_noeud, Statut, 1 Ordre, NULL, NULL, NULL, id conteneur_href FROM EP_BatimentTechnique_Reco_line where Transformateur = True
    UNION ALL
  SELECT ogr_pkid, id, cast('Jonction' as text) type_noeud, Statut, 1 Ordre, PrecisionXY, PrecisionZ, Geometrie, NULL conteneur_href FROM EP_Jonction_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('PointDeComptage' as text) type_noeud, Statut, 4 Ordre, NULL, NULL, NULL, id conteneur_href FROM EP_Coffret_Reco where PointDeComptage = True
    UNION ALL
  SELECT ogr_pkid, id, cast('RaccordementDistributionEclairageExterieur' as text) type_noeud, Statut, 3 Ordre, NULL, NULL, NULL, id conteneur_href FROM EP_Coffret_Reco where RaccordementDistributionEclairageExterieur = True
    UNION ALL
  SELECT ogr_pkid, id, cast('TableauDeCommande' as text) type_noeud, Statut, 2 Ordre, NULL, NULL, NULL, id conteneur_href FROM EP_Coffret_Reco where TableauDeCommande = True
    UNION ALL
  SELECT ogr_pkid, id, cast('CircuitEclairageExterieur' as text) type_noeud, Statut, 1 Ordre, NULL, NULL, Geometrie, NULL conteneur_href FROM EP_CircuitEclairageExterieur_Reco
    UNION ALL
  SELECT ogr_pkid, id, cast('Terre' as text) type_noeud, Statut, NULL Ordre, NULL, NULL, NULL, id conteneur_href FROM EP_Coffret_Reco where Terre = True
    UNION ALL
  SELECT ogr_pkid, id, cast('Terre' as text) type_noeud, Statut, NULL Ordre, NULL, NULL, NULL, id conteneur_href FROM EP_BatimentTechnique_Reco_line where Terre = True
    UNION ALL
  SELECT ogr_pkid, id, cast('CelluleHTA' as text) type_noeud, Statut, NULL Ordre, NULL, NULL, NULL, id conteneur_href FROM EP_BatimentTechnique_Reco_line where NombreCelluleHTA > 0
    UNION ALL
  SELECT ogr_pkid, id, cast('TableauHTA' as text) type_noeud, Statut, NULL Ordre, NULL, NULL, NULL, id conteneur_href FROM EP_BatimentTechnique_Reco_line where NombreTableauHTA > 0
    UNION ALL
  SELECT ogr_pkid, id, cast('PosteElectrique' as text) type_noeud, Statut, NULL Ordre, NULL, NULL, Geometrie, id conteneur_href FROM EP_BatimentTechnique_Reco_line where PosteElectrique = True
) select cast(ROW_NUMBER () OVER () as int) fid
, case when conteneur_href is not null then ogr_pkid||'_'||type_noeud else ogr_pkid end ogr_pkid
, case when conteneur_href is not null then id||'_'||type_noeud else id end id
, type_noeud, Statut, Ordre, PrecisionXY, PrecisionZ, Geometrie, conteneur_href from all_conso;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('Noeud_virt','features','Noeud_virt',2154); --GPKG
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('Noeud_virt', 'Geometrie', 'POINT', 2154, 1, 0); --GPKG

--XXX Vue Noeud_Cable
-- vue technique hors recostar`

select DropTable(NULL, 'Noeud_Cable_virt', TRUE);
CREATE VIEW Noeud_Cable_virt as
with all_conso as (
  SELECT c.id cable_id, type_cable, cast('StartPoint' as text) connpt, n.id noeud_id, type_noeud, ordre
  FROM Noeud_virt n
  JOIN Cable c ON PtDistWithin(n."Geometrie", ST_StartPoint(c."Geometrie"), 0.002)
  WHERE n.Geometrie is not null
  UNION ALL
  SELECT c.id cable_id, type_cable, cast('EndPoint' as text) connpt, n.id noeud_id, type_noeud, ordre
  FROM Noeud_virt n
  JOIN Cable c ON PtDistWithin(n."Geometrie", ST_EndPoint(c."Geometrie"), 0.002)
  WHERE n.Geometrie is not null
) select cast(ROW_NUMBER () OVER () as int) fid, * from all_conso;
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('Noeud_Cable_virt','attributes','Noeud_Cable_virt'); --GPKG

--XXX Vue Conteneur_Noeud
-- vue technique hors recostar
-- permet de renseigner conteneur_href sur les tables noeuds

select DropTable(NULL, 'Conteneur_Noeud_virt', TRUE);
CREATE VIEW Conteneur_Noeud_virt as
with all_conso as (
  SELECT n.id noeud_id, type_noeud, c.conteneur_id conteneur_id, type_conteneur
  FROM Noeud_virt n
  JOIN EP_GeometrieSupplementaire_Reco c ON PtDistWithin(c."Ligne2.5D", n."Geometrie", 0.002) OR PtDistWithin(c."Surface2.5D", n."Geometrie", 0.002)
  where conteneur_href is null
  UNION ALL
  SELECT n.id noeud_id, type_noeud, c.id conteneur_id, type_conteneur
  FROM Noeud_virt n
  JOIN Conteneur c ON c.type_conteneur = 'Support' AND PtDistWithin(c."Geometrie", n."Geometrie", 0.002)
  where conteneur_href is null
  UNION ALL
  SELECT n.id noeud_id, type_noeud, c.id conteneur_id, type_conteneur
  FROM Noeud_virt n
  JOIN Conteneur c ON c.id = n.conteneur_href
  where conteneur_href is not null
) select cast(ROW_NUMBER () OVER () as int) fid, * from all_conso;
;

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('Conteneur_Noeud_virt','attributes','Conteneur_Noeud_virt'); --GPKG


--XXX NOEUD / CABLE
select DropTable(NULL, 'ctrl_Noeud_Cable', TRUE);
CREATE VIEW ctrl_Noeud_Cable AS
with all_ctrl as(

SELECT c.id, 'il manque un Noeud à l''extrémité de ce Cable Electrique' ctrl, ST_StartPoint(CastToXY("Geometrie")) Geometrie
FROM EP_CableElectrique_Reco c
WHERE Statut = 'UnderCommissionning'
AND NOT EXISTS (SELECT 1 FROM Noeud_Cable_virt cn WHERE cn.cable_id=c.id and cn.connpt = 'StartPoint' and cn.type_noeud not in ('Armement') )
AND NOT EXISTS (SELECT 1 FROM Conteneur_Cable cc
                          JOIN Conteneur_Noeud_virt cn ON cc.conteneur_id=cn.conteneur_id
                          WHERE cc.cable_id=c.id AND cc.connpt = 'StartPoint' and cn.type_noeud not in ('Armement') )
UNION ALL
SELECT c.id, 'il manque un Noeud à l''extrémité de ce Cable Electrique' ctrl, ST_EndPoint(CastToXY("Geometrie")) Geometrie
FROM EP_CableElectrique_Reco c
WHERE Statut = 'UnderCommissionning'
AND NOT EXISTS (SELECT 1 FROM Noeud_Cable_virt cn WHERE cn.cable_id=c.id and cn.connpt = 'EndPoint' and cn.type_noeud not in ('Armement') )
AND NOT EXISTS (SELECT 1 FROM Conteneur_Cable cc
                          JOIN Conteneur_Noeud_virt cn ON cc.conteneur_id=cn.conteneur_id
                          WHERE cc.cable_id=c.id AND cc.connpt = 'EndPoint' and cn.type_noeud not in ('Armement') )
-- UNION ALL
-- SELECT id, 'il manque un Noeud Terre à l''extrémité de ce Cable de Terre' ctrl, ST_EndPoint(CastToXY("Geometrie")) Geometrie
-- FROM EP_CableTerre_Reco c
-- WHERE Statut = 'UnderCommissionning'
-- AND NOT EXISTS (SELECT 1 FROM Noeud_Cable_virt cn WHERE cn.cable_id=c.id and cn.type_noeud = 'Terre' and cn.connpt='EndPoint' )

UNION ALL
SELECT noeud_id id, 'ce type de noeud '||n.type_noeud||' n''est pas autorisé à l''extrémité d''un Cable Electrique' ctrl, CastToXY(n."Geometrie") Geometrie
FROM Noeud_Cable_virt cn
JOIN Noeud_virt n ON n.id=cn.noeud_id
WHERE type_cable = 'CableElectrique' and cn.type_noeud not in ('RaccordementDistributionEclairageExterieur', 'RaccordementLuminaire', 'BoiteModulaire', 'BorneRepiquage', 'CelluleHTA', 'CircuitEclairageExterieur', 'Jonction', 'Luminaire', 'PointDeComptage', 'PosteElectrique', 'RemonteeAeroSouterraine', 'TableauDeCommande', 'TableauHTA', 'Terre', 'Armement', 'Transformateur')
AND Statut = 'UnderCommissionning'

UNION ALL
SELECT n.id, 'le Domaine Tension de la Jonction n''est pas cohérent avec celui du Cable Electrique' ctrl, CastToXY(n."Geometrie") Geometrie
FROM EP_Jonction_Reco n
JOIN Noeud_Cable_virt cn on cn.noeud_id=n.id
JOIN EP_CableElectrique_Reco c on c.id=cn.cable_id
WHERE n.Statut = 'UnderCommissionning' AND n.DomaineTension <> c.DomaineTension
--QUESTION: regle qui s'applique également sur les : BoiteModulaire,CelluleHTA,PosteElectrique,RemonteeAeroSouterraine,TableauHTA,TransformateurDePuissance. Cependant ces objets n'ont pas de Domaine Tension

UNION ALL
SELECT n.id, 'ce noeud '||n.type_noeud||' ne coupe pas le Cable' ctrl,CastToXY(n.Geometrie) Geometrie
FROM Noeud_virt n
JOIN Cable c ON PtDistWithin(n."Geometrie",c."Geometrie", 0.002)
WHERE n.Statut = 'UnderCommissionning' AND c.Statut = 'UnderCommissionning'
AND NOT EXISTS (SELECT 1 FROM Noeud_Cable_virt cn WHERE cn.cable_id=c.id and cn.noeud_id=n.id )

) select cast(ROW_NUMBER () OVER () as int) fid, * from all_ctrl
;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('ctrl_Noeud_Cable','features','ctrl_Noeud_Cable',2154);
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('ctrl_Noeud_Cable', 'Geometrie', 'POINT', 2154, 0, 0);

--XXX CHEMINEMENT / CABLE
select DropTable(NULL, 'ctrl_Cheminement_Cable', TRUE);
CREATE VIEW ctrl_Cheminement_Cable AS -- QUESTION : peut arriver dans quels cas?
SELECT id, 'aucun Cable ne passe dans ce '||type_cheminement ctrl, CastToXY(h.Geometrie) Geometrie
FROM Cheminement h
WHERE NOT EXISTS (SELECT 1 FROM Cable c where ST_Within(h."Geometrie", c."Geometrie"));

INSERT INTO gpkg_contents (table_name, data_type, identifier) values ('ctrl_Cheminement_Cable','features','ctrl_Cheminement_Cable');
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('ctrl_Cheminement_Cable', 'Geometrie', 'LINESTRING', 2154, 0, 0);

--XXX NOEUD / CONTENEUR
select DropTable(NULL, 'ctrl_Noeud_Conteneur', TRUE);
CREATE VIEW ctrl_Noeud_Conteneur AS
with all_ctrl as(

SELECT n.id, 'ce noeud '||type_noeud||' n''est pas contenu dans un Coffret' ctrl, CastToXY(n.Geometrie) Geometrie
FROM Noeud_virt n
WHERE type_noeud in ('CircuitEclairageExterieur')
AND Statut = 'UnderCommissionning'
AND NOT EXISTS (SELECT 1 FROM Conteneur_Noeud_virt cn WHERE cn.noeud_id=n.id AND cn.type_conteneur = 'Coffret') -- QUESTION : Vérifier si coffret de type Armoire?

UNION ALL
SELECT n.id, 'ce noeud '||type_noeud||' n''est pas contenu dans un Coffret ou un Regard' ctrl, CastToXY(n.Geometrie) Geometrie
FROM Noeud_virt n
WHERE type_noeud in ('BorneRepiquage')
AND Statut = 'UnderCommissionning'
AND NOT EXISTS (SELECT 1 FROM Conteneur_Noeud_virt cn WHERE cn.noeud_id=n.id AND cn.type_conteneur in ('Coffret', 'Regard'))

UNION ALL
SELECT n.id, 'cette '||TypeJonction||' n''est pas placé sur un Support' ctrl, CastToXY(n.Geometrie) Geometrie
FROM EP_Jonction_Reco n
WHERE TypeJonction in ('RemonteeAeroSouterraine')
AND Statut = 'UnderCommissionning'
AND NOT EXISTS (SELECT 1 FROM Conteneur_Noeud_virt cn WHERE cn.noeud_id=n.id AND cn.type_conteneur = 'Support')

UNION ALL
SELECT n.id, 'cette '||type_noeud||' n''est pas placé sur un Support' ctrl, CastToXY(n.Geometrie) Geometrie
FROM Noeud_virt n
WHERE type_noeud in ('RemonteeAeroSouterraine')
AND Statut = 'UnderCommissionning'
AND NOT EXISTS (SELECT 1 FROM Conteneur_Noeud_virt cn WHERE cn.noeud_id=n.id AND cn.type_conteneur = 'Support')

UNION ALL
SELECT n.id, 'ce noeud '||type_noeud||' n''est pas placé sur un Support ou dans un Regard' ctrl, CastToXY(n.Geometrie) Geometrie
FROM Noeud_virt n
WHERE type_noeud in ('Luminaire')
AND Statut = 'UnderCommissionning'
AND NOT EXISTS (SELECT 1 FROM Conteneur_Noeud_virt cn WHERE cn.noeud_id=n.id AND cn.type_conteneur in ('Support', 'Regard'))

UNION ALL
SELECT n.id, 'la précison XY / Z de ce noeud '||type_noeud||' n''est pas renseignée' ctrl, CastToXY(n.Geometrie) Geometrie
FROM Noeud_virt n
WHERE (PrecisionXY is Null OR PrecisionZ is Null) AND Statut = 'UnderCommissionning'
AND NOT EXISTS (SELECT 1 FROM Conteneur_Noeud_virt cn WHERE cn.noeud_id=n.id)

) select cast(ROW_NUMBER () OVER () as int) fid, * from all_ctrl
;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('ctrl_Noeud_Conteneur','features','ctrl_Noeud_Conteneur',2154);
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('ctrl_Noeud_Conteneur', 'Geometrie', 'POINT', 2154, 0, 0);

-- XXX PTRL
select DropTable(NULL, 'ctrl_PLOR_Ouvrage', TRUE);
CREATE VIEW ctrl_PLOR_Ouvrage AS
with all_ctrl as(

select p.id, 'ce Point Levé n''est pas placé sur le tracé d''un Ouvrage' ctrl, CastToXY(p.Geometrie) Geometrie
from EP_PointLeveOuvrageReseau_Reco p
WHERE NOT EXISTS (select 1 from Cable c where PtDistWithin(c.Geometrie, p.Geometrie, 0.002))
AND NOT EXISTS (select 1 from Cheminement c where PtDistWithin(c.Geometrie, p.Geometrie, 0.002))
AND NOT EXISTS (select 1 from Noeud_virt c where PtDistWithin(c.Geometrie, p.Geometrie, 0.002))
AND NOT EXISTS (select 1 from Conteneur c where PtDistWithin(c.Geometrie, p.Geometrie, 0.002) and type_conteneur in ('Support', 'Regard', 'Coffret'))
AND NOT EXISTS (select 1 from EP_GeometrieSupplementaire_Reco c where PtDistWithin(c."Ligne2.5D", p."Geometrie", 0.002))

UNION ALL
SELECT c.id
,CASE WHEN NOT EXISTS (select 1 from EP_PointLeveOuvrageReseau_Reco p where PtDistWithin(ST_PointN(c.Geometrie, s.value), p.Geometrie, 0.002))
      AND NOT EXISTS (select 1 from EP_GeometrieSupplementaire_Reco g where PtDistWithin(ST_PointN(c.Geometrie, s.value), g."Ligne2.5D", 0.002))
          THEN 'le sommet de ce Cable n''est pas placé sur un Point Levé'
          --NOTE: ok si il arrive sur l'emprise d'un conteneur
      WHEN (select 1 from EP_PointLeveOuvrageReseau_Reco p where PtDistWithin(ST_PointN(c.Geometrie, s.value), p.Geometrie, 0.002)
                                                            AND NOT ST_Z(ST_PointN(c.Geometrie, s.value)) = st_Z(p.Geometrie))
          THEN 'le Z du sommet de ce Cable n''est pas cohérent avec le Point Levé'
end ctrl
,ST_PointN(c.Geometrie, s.value) Geometrie
FROM cable c
, generate_series s ON s.value <= ST_NumPoints(c.Geometrie)
WHERE Statut = 'UnderCommissionning' AND PrecisionXY = 'A' and PrecisionZ = 'A' and ctrl is not null

UNION ALL
SELECT c.id
,CASE WHEN NOT EXISTS (select 1 from EP_PointLeveOuvrageReseau_Reco p where PtDistWithin(ST_PointN(c.Geometrie, s.value), p.Geometrie, 0.002))
      AND NOT EXISTS (select 1 from EP_GeometrieSupplementaire_Reco g where PtDistWithin(ST_PointN(c.Geometrie, s.value), g."Ligne2.5D", 0.002))
          THEN 'le sommet de ce Cheminement n''est pas placé sur un Point Levé'
      WHEN (select 1 from EP_PointLeveOuvrageReseau_Reco p where PtDistWithin(ST_PointN(c.Geometrie, s.value), p.Geometrie, 0.002)
                                                            AND NOT ST_Z(ST_PointN(c.Geometrie, s.value)) = st_Z(p.Geometrie))
          THEN 'le Z du sommet de ce Cheminement n''est pas cohérent avec le Point Levé'
end ctrl
,ST_PointN(c.Geometrie, s.value) Geometrie
FROM Cheminement c
, generate_series s ON s.value <= ST_NumPoints(c.Geometrie)
WHERE PrecisionXY = 'A' and PrecisionZ = 'A' and ctrl is not null --QUESTION : pas de statut pour gérer les conditions liées à la classe A et aux PLOR

UNION ALL
SELECT c.id
,CASE WHEN NOT EXISTS (select 1 from EP_PointLeveOuvrageReseau_Reco p where PtDistWithin(ST_PointN(c."Ligne2.5D", s.value), p.Geometrie, 0.002))
          THEN 'le sommet de ce '||type_conteneur||' n''est pas placé sur un Point Levé'
      WHEN (select 1 from EP_PointLeveOuvrageReseau_Reco p where PtDistWithin(ST_PointN(c."Ligne2.5D", s.value), p.Geometrie, 0.002)
                                                            AND NOT ST_Z(ST_PointN(c."Ligne2.5D", s.value)) = st_Z(p.Geometrie))
          THEN 'le Z du sommet de ce '||type_conteneur||' n''est pas cohérent avec le Point Levé'
end ctrl
,ST_PointN(c."Ligne2.5D", s.value) Geometrie
FROM EP_GeometrieSupplementaire_Reco c
, generate_series s ON s.value <= ST_NumPoints(c."Ligne2.5D")
WHERE Statut = 'UnderCommissionning' AND PrecisionXY = 'A' and PrecisionZ = 'A' and type_conteneur in ('BatimentTechnique') and ctrl is not null

UNION ALL
SELECT c.id
,CASE WHEN NOT EXISTS (select 1 from EP_PointLeveOuvrageReseau_Reco p where PtDistWithin(c.Geometrie, p.Geometrie, 0.002))
          THEN 'ce '||type_conteneur||' n''est pas placé sur un Point Levé'
      WHEN (select 1 from EP_PointLeveOuvrageReseau_Reco p where PtDistWithin(c.Geometrie, p.Geometrie, 0.002)
                                                            AND NOT ST_Z(c.Geometrie) = st_Z(p.Geometrie))
          THEN 'le Z du '||type_conteneur||' n''est pas cohérent avec le Point Levé'
end ctrl
,c.Geometrie
FROM Conteneur c
WHERE Statut = 'UnderCommissionning' AND PrecisionXY = 'A' and PrecisionZ = 'A' and type_conteneur in ('Support', 'Coffret', 'Regard') and ctrl is not null
-- QUESTION: valider que la gestion des regards se fait comme celle des coffrets ou des batiments?
UNION ALL
SELECT c.id
,CASE WHEN NOT EXISTS (select 1 from EP_PointLeveOuvrageReseau_Reco p where PtDistWithin(c.Geometrie, p.Geometrie, 0.002))
          THEN 'ce noeud '||type_noeud||' n''est pas placé sur un Point Levé'
      WHEN (select 1 from EP_PointLeveOuvrageReseau_Reco p where PtDistWithin(c.Geometrie, p.Geometrie, 0.002)
                                                            AND NOT ST_Z(c.Geometrie) = st_Z(p.Geometrie))
          THEN 'le Z du noeud '||type_noeud||' n''est pas cohérent avec le Point Levé'
end ctrl
,c.Geometrie
FROM Noeud_virt c
WHERE Statut = 'UnderCommissionning' AND PrecisionXY = 'A' and PrecisionZ = 'A' and ctrl is not null
AND NOT EXISTS (SELECT 1 FROM Conteneur_Noeud_virt cn WHERE cn.noeud_id=c.id)

) select cast(ROW_NUMBER () OVER () as int) fid, * from all_ctrl
;

INSERT INTO gpkg_contents (table_name, data_type, identifier, srs_id) values ('ctrl_PLOR_Ouvrage','features','ctrl_PLOR_Ouvrage',2154);
INSERT INTO gpkg_geometry_columns (table_name, column_name, geometry_type_name, srs_id, z, m) values ('ctrl_PLOR_Ouvrage', 'Geometrie', 'POINT', 2154, 0, 0);

-- TODO :
-- voir regles PGOC UN PTRL tous les 15m et suffisament dans les courbes
-- CONTROLE CHEMINEMENT AERIEN / SUPPORT

-- TODO Métier :
---- ordonner les listes
---- voir les valeurs par defaut
---- symbologie des réseaux
